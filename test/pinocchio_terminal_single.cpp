#include <random>
#include <cmath>
#include <functional>
#include <limits>
#include <map>
#include <type_traits>

#include "pinocchio/parsers/urdf.hpp"

#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/aba-derivatives.hpp"
#include "pinocchio/algorithm/aba.hpp"

#include "boost/range/adaptor/indexed.hpp"

#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"

#include "boost/preprocessor/tuple/elem.hpp"
#include "boost/preprocessor/comparison/equal.hpp"
#include "boost/preprocessor/facilities/empty.hpp"
#include "boost/preprocessor/punctuation/comma.hpp"

#include "boost/multiprecision/cpp_bin_float.hpp"
#include "general_utilities.hpp"
#include "model.hpp"
#include "eigen/random.hpp"
#include "indexers.hpp"
#include "ddp.hpp"

#include "fmt/format.h"
#include "fmt/ostream.h"
#include "pinocchio_dynamics.hpp"

#define AS_MEM(a1, a2, Elem)                                                   \
  BOOST_PP_TUPLE_ELEM(0, Elem) BOOST_PP_TUPLE_ELEM(1, Elem);

#define AS_OPTIONAL_MEM(a1, a2, Elem)                                          \
  boost::optional<BOOST_PP_TUPLE_ELEM(0, Elem)> BOOST_PP_TUPLE_ELEM(1, Elem) = \
      boost::none;

#define AND_HAS_VALUE(a1, a2, i, Elem)                                         \
  BOOST_PP_IF(BOOST_PP_EQUAL(i, 0), BOOST_PP_EMPTY(), and)                     \
  args.BOOST_PP_TUPLE_ELEM(1, Elem).has_value()

#define DEREF(a1, a2, i, Elem)                                                 \
  BOOST_PP_IF(BOOST_PP_EQUAL(i, 0), BOOST_PP_EMPTY, BOOST_PP_COMMA)            \
  () * args.BOOST_PP_TUPLE_ELEM(1, Elem)

using big_float = boost::multiprecision::number<
    boost::multiprecision::backends::cpp_bin_float<200> >;

using scalar_t = big_float;

#define ARG_TUPLE                                                              \
  ((scalar_t, dt),                                                             \
   (gu::isize, horizon),                                                       \
   (scalar_t, reference_traj_norm),                                            \
   (scalar_t, mu),                                                             \
   (scalar_t, threshold),                                                      \
   (gu::usize, success_strategy_idx),                                          \
   (gu::usize, failure_strategy_idx),                                          \
   (scalar_t, w_success_update_pow),                                           \
   (scalar_t, n_success_update_pow),                                           \
   (scalar_t, w_failure_update_pow),                                           \
   (scalar_t, n_failure_update_pow),                                           \
   (scalar_t, mu_update_factor),                                               \
   (bool, eval_hessians),                                                      \
   (gu::isize, n_iter),                                                        \
   (boost::filesystem::path, pinocchio_path))

#define ARG_SEQ BOOST_PP_TUPLE_TO_SEQ(ARG_TUPLE)
struct parsed_arg_type {
  BOOST_PP_SEQ_FOR_EACH(AS_OPTIONAL_MEM, _, ARG_SEQ)
};
struct arg_type {
  BOOST_PP_SEQ_FOR_EACH(AS_MEM, _, ARG_SEQ)
};

static auto from_parsed(parsed_arg_type&& args) -> boost::optional<arg_type> {
  if (BOOST_PP_SEQ_FOR_EACH_I(AND_HAS_VALUE, _, ARG_SEQ)) {
    return arg_type{BOOST_PP_SEQ_FOR_EACH_I(DEREF, _, ARG_SEQ)};
  }
  return boost::none;
}

constexpr int default_n_digits = 3;

template <typename T>
auto to_str(
    boost::multiprecision::number<T> const& x, int n_digits = default_n_digits)
    -> std::string {
  return x.str(n_digits, std::ios_base::scientific);
}

template <typename T, REQUIRES(std::is_floating_point<T>{})>
auto to_str(T const& x, int n_digits = default_n_digits) -> std::string {
  std::array<char, 32> buffer{};
  fmt::format_to(buffer.begin(), "{}:.{}e{}", "{", n_digits, "}");
  return fmt::format(buffer.data(), x);
}

auto parse_args(gsl::span<gsl::zstring> args) -> boost::optional<arg_type> {
  using boost::lexical_cast;
  using boost::string_view;
  using boost::filesystem::path;
  using gsl::czstring;
  using gu::isize;
  using gu::usize;

  parsed_arg_type result;

  auto arg_z = [&](gsl::index i) -> czstring {
    return args.at(gsl::narrow_cast<std::size_t>(i));
  };
  auto arg = [&](gsl::index i) -> gsl::cstring_span { return {arg_z(i)}; };

  std::map<gsl::cstring_span, std::function<void(string_view)> > actions;

  actions["-seed"] = [](string_view current_arg) {
    std::srand(lexical_cast<unsigned int>(current_arg));
  };
  actions["-pinocchio"] = [&result](string_view current_arg) {
    if (current_arg.starts_with("~/")) {
      string_view home = std::getenv("HOME");
      string_view path_tail = current_arg.substr(2);
      result.pinocchio_path = path(home.begin(), home.end());
      result.pinocchio_path->append(path_tail.begin(), path_tail.end());
    } else {
      result.pinocchio_path = path(current_arg.begin(), current_arg.end());
    }
  };

  // System parameters
  actions["-t"] = [&result](string_view current_arg) {
    result.horizon = lexical_cast<isize>(current_arg);
  };
  actions["-dt"] = [&result](string_view current_arg) {
    result.dt = lexical_cast<scalar_t>(current_arg);
  };
  actions["-norm"] = [&result](string_view current_arg) {
    result.reference_traj_norm = lexical_cast<scalar_t>(current_arg);
  };

  // Solver parameters
  actions["-mu"] = [&result](string_view current_arg) {
    result.mu = lexical_cast<scalar_t>(current_arg);
  };
  actions["-err"] = [&result](string_view current_arg) {
    result.threshold = lexical_cast<scalar_t>(current_arg);
  };
  actions["-niter"] = [&result](string_view current_arg) {
    result.n_iter = lexical_cast<isize>(current_arg);
  };

  actions["-success-strat"] = [&result](string_view current_arg) {
    result.success_strategy_idx = lexical_cast<usize>(current_arg);
  };
  actions["-failure-strat"] = [&result](string_view current_arg) {
    result.failure_strategy_idx = lexical_cast<usize>(current_arg);
  };

  actions["-n-success-pow"] = [&result](string_view current_arg) {
    result.n_success_update_pow = lexical_cast<scalar_t>(current_arg);
  };
  actions["-w-success-pow"] = [&result](string_view current_arg) {
    result.w_success_update_pow = lexical_cast<scalar_t>(current_arg);
  };
  actions["-n-failure-pow"] = [&result](string_view current_arg) {
    result.n_failure_update_pow = lexical_cast<scalar_t>(current_arg);
  };
  actions["-w-failure-pow"] = [&result](string_view current_arg) {
    result.w_failure_update_pow = lexical_cast<scalar_t>(current_arg);
  };
  actions["-w-failure-pow"] = [&result](string_view current_arg) {
    result.w_failure_update_pow = lexical_cast<scalar_t>(current_arg);
  };
  actions["-hessians"] = [&result](string_view current_arg) {
    result.eval_hessians = lexical_cast<bool>(current_arg);
  };
  actions["-mu-factor"] = [&result](string_view current_arg) {
    result.mu_update_factor = lexical_cast<scalar_t>(current_arg);
  };
  actions["-factor"] = [](string_view) {};

  for (gsl::index i = 1; i < args.ssize(); i += 2) {
    actions.at(arg(i))(arg_z(i + 1));
  }

  return from_parsed(std::move(result));
}

//------------------------------------------------------------------------------

struct custom_indexer_t {
  using isize = gu::isize;

  isize n_steps;

  auto total_size() const -> isize { return 6; }

  using rows_t = gu::dyn;
  using cols_t = gu::fix<1>;

  auto rows(isize t) const -> rows_t {
    if (t == n_steps - 2) {
      return gu::dyn{6};
    }
    return gu::dyn{0};
  }
  auto cols(isize) const -> gu::fix<1> { return {}; }

  auto stride(isize t) const -> gu::dyn { return rows(t); }

  using iterator = ddp::detail::indexer_iter_t<custom_indexer_t>;
  using const_iterator = iterator;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() const -> iterator { return {this, 0, 0}; }
  auto end() const -> iterator { return {this, n_steps, total_size()}; }
  auto rbegin() const -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() const -> reverse_iterator { return reverse_iterator{begin()}; }
};

template <typename F>
struct constraint_t { // NOLINT(cppcoreguidelines-pro-type-member-init)

  using isize = gu::isize;

  template <isize N>
  using dyn_or_fixed_t = gu::conditional_t<N == -1, gu::dyn, gu::fix<N> >;

  template <isize indiml, isize indimr, isize outdim>
  using ten_t = np::tensor<       //
      scalar_t,                   //
      np::tensor_shape_t<         //
          dyn_or_fixed_t<indiml>, //
          dyn_or_fixed_t<indimr>, //
          dyn_or_fixed_t<outdim>  //
          >,                      //
      np::tags::detail::on_heap_t //
      >;

  template <isize rows, isize cols> using mat_t = np::mat<scalar_t, rows, cols>;
  template <isize Size> using vec_t = np::mat<scalar_t, Size, 1>;

  isize m_horizon;
  F const& m_dynamics;
  std::function<vec_t<-1>(isize t)> m_rhs;

  auto out_dim(isize t) const noexcept -> gu::dyn {
    if (t == m_horizon - 2) {
      return gu::dyn{6};
    }
    return gu::dyn{0};
  }

  auto x_dim(isize t) const noexcept -> decltype(m_dynamics(0).x_dim()) {
    return m_dynamics(t).x_dim();
  }

  auto u_dim(isize t) const noexcept -> decltype(m_dynamics(0).u_dim()) {
    return m_dynamics(t).u_dim();
  }

  struct derivs_2nd_t {
    vec_t<-1> val;
    mat_t<-1, 12> x;
    mat_t<-1, 6> u;
    ten_t<12, 12, -1> xx;
    ten_t<6, 12, -1> ux;
    ten_t<6, 6, -1> uu;
  };

  template <typename InX, typename InU>
  auto operator()(isize t, InX const& x, InU const& u) const -> vec_t<-1> {
    auto f1 = m_dynamics.f(x, u);
    auto f2 = m_dynamics.f(f1, u);
    return np::eval(np::top_rows(f2, out_dim(t)) - m_rhs(t + 2));
  }

  template <typename InX, typename InU>
  auto derivs_2(isize t, InX const& x, InU const& u) const -> derivs_2nd_t {
    using np::clone;
    using np::eval;
    using uninit = np::uninitialized<scalar_t>;

    auto val = uninit::col(out_dim(t));
    auto jx = uninit::mat(out_dim(t), gu::fix<12>{});
    auto ju = uninit::mat(out_dim(t), gu::fix<6>{});

    auto hxx = uninit::ten(
        gu::fix<12>{}, gu::fix<12>{}, out_dim(t), np::tags::on_heap);
    auto hux =
        uninit::ten(gu::fix<6>{}, gu::fix<12>{}, out_dim(t), np::tags::on_heap);
    auto huu =
        uninit::ten(gu::fix<6>{}, gu::fix<6>{}, out_dim(t), np::tags::on_heap);

    if (out_dim(t).as_isize() != 0) {

      auto f1 = m_dynamics.derivs_2(x, u);
      auto f2 = m_dynamics.derivs_2(f1.val, uninit::col(gu::fix<6>{}));

      val = np::top_rows(f2.val, out_dim(t)) - m_rhs(t + 2);
      jx = np::top_rows(f2.x * f1.x, out_dim(t));
      ju = np::top_rows(f2.x * f1.u, out_dim(t));

      np::contraction_outer_axis(hxx, f1.xx, np::top_rows(f2.x, out_dim(t)));
      np::contraction_outer_axis(hux, f1.ux, np::top_rows(f2.x, out_dim(t)));
      np::contraction_outer_axis(huu, f1.uu, np::top_rows(f2.x, out_dim(t)));

      {
        auto tmp1 = uninit::ten(
            f2.xx.indiml(), f1.x.cols(), f2.xx.outdim(), np::tags::on_heap);
        auto tmp2 = uninit::ten(
            f1.x.cols(), f1.x.cols(), f2.xx.outdim(), np::tags::on_heap);

        np::contraction_right_axis(tmp1, f2.xx, f1.x);
        np::contraction_left_axis(tmp2, tmp1, f1.x.T());

        np::no_alias(hxx.as_matrix_view()) +=
            np::left_cols(tmp2.as_matrix_view(), out_dim(t));
      }
      {
        auto tmp1 = uninit::ten(
            f2.xx.indiml(), f1.x.cols(), f2.xx.outdim(), np::tags::on_heap);
        auto tmp2 = uninit::ten(
            f1.u.cols(), f1.x.cols(), f2.xx.outdim(), np::tags::on_heap);

        np::contraction_right_axis(tmp1, f2.xx, f1.x);
        np::contraction_left_axis(tmp2, tmp1, f1.u.T());

        np::no_alias(hux.as_matrix_view()) +=
            np::left_cols(tmp2.as_matrix_view(), out_dim(t));
      }
      {
        auto tmp1 = uninit::ten(
            f2.xx.indiml(), f1.u.cols(), f2.xx.outdim(), np::tags::on_heap);
        auto tmp2 = uninit::ten(
            f1.u.cols(), f1.u.cols(), f2.xx.outdim(), np::tags::on_heap);

        np::contraction_right_axis(tmp1, f2.xx, f1.u);
        np::contraction_left_axis(tmp2, tmp1, f1.u.T());

        np::no_alias(huu.as_matrix_view()) +=
            np::left_cols(tmp2.as_matrix_view(), out_dim(t));
      }
    }

    return {std::move(val),
            std::move(jx),
            std::move(ju),
            std::move(hxx),
            std::move(hux),
            std::move(huu)};
  }

  struct functor_t {
    gu::cref_t<constraint_t> parent;
    isize t;

    auto x_dim() const RETURNS_AUTO(parent.x_dim(t))
    auto u_dim() const RETURNS_AUTO(parent.u_dim(t))
    auto out_dim() const RETURNS_AUTO(parent.out_dim(t))

    template <typename InX, typename InU>
    auto operator()(InX const& x, InU const& u) const -> vec_t<-1> {
      return parent(t, x, u);
    }

    template <typename InX, typename InU>
    auto derivs_2(InX const& x, InU const& u) const -> derivs_2nd_t {
      return parent.derivs_2(t, x, u);
    }
  };
  auto operator()(isize t) const -> functor_t { return {*this, t}; }
};

struct iterate_logger_t {

  std::vector<ddp::iterate_t<scalar_t> > m_iterates;
  gu::usize m_best_index{};
  gu::dyn m_traj_size;
  gu::dyn m_eq_mult_size;

  static constexpr auto size_sentinel = std::numeric_limits<gu::usize>::max();

  template <typename T>
  explicit iterate_logger_t(T const& prob)
      : m_best_index{size_sentinel},
        m_traj_size{prob.x_idx.total_size() + prob.u_idx.total_size()},
        m_eq_mult_size{prob.eq_idx.total_size()} {}

  void header() const {
    fmt::print(
        "{:<5} {:>15} {:>15} {:>15} {:>15} {:>15} {:>15}\n",
        "iter",
        "aug. lag. obj.",
        "constraint",
        "w",
        "n",
        "mu",
        "reg.");
  }

  auto best() const -> ddp::solution_candidate_t<scalar_t> const& {
    return m_iterates.at(m_best_index).sol;
  }

  template <typename Iterate> void operator()(Iterate const& iterate) {
    fmt::print(
        "{:<5} {:>15} {:>15} {:>15} {:>15} {:>15} {:>15}\n",
        iterate.iteration_num,
        to_str(iterate.opt_obj),
        to_str(iterate.opt_constr),
        to_str(iterate.old_parameters.w),
        to_str(iterate.old_parameters.n),
        to_str(iterate.old_parameters.mu),
        to_str(iterate.old_parameters.reg));

    auto traj_concat = np::uninitialized<scalar_t>::col(m_traj_size);
    auto eq_mult_concat = np::uninitialized<scalar_t>::col(m_eq_mult_size);

    gu::dyn eq_acc{0};
    gu::dyn traj_acc{0};
    for (auto elems : gu::ranges::zip(iterate.traj, iterate.mults.eq)) {
      BIND(auto&&, (xu, eq_fn), elems);
      auto x = xu.x();
      auto u = xu.u();
      auto eq = eq_fn(x);

      // clang-format off
      np::mid_rows(eq_mult_concat , eq_acc             , eq.rows())  = eq;
      np::mid_rows(traj_concat    , traj_acc           , x .rows() ) = x;
      np::mid_rows(traj_concat    , traj_acc + x.rows(), u .rows() ) = u;
      // clang-format on

      eq_acc = eq_acc + eq.rows();
      traj_acc = traj_acc + x.rows() + u.rows();
    }
    auto x = iterate.traj.x_f();
    np::bot_rows(traj_concat, x.rows()) = x;

    m_iterates.push_back(
        {iterate.iteration_num,
         {std::move(traj_concat), std::move(eq_mult_concat), iterate.opt_score},
         iterate.old_parameters.reg,
         iterate.new_reg,
         iterate.step,
         iterate.opt_constr,
         iterate.opt_lag});

    if (m_best_index == size_sentinel or
        iterate.opt_score < best().optimality_score) {
      m_best_index = m_iterates.size() - 1;
    }
  }
};

auto main(int argc, char** argv) -> int {
  using gu::dyn;
  using gu::fix;
  using gu::isize;
  namespace pin = pinocchio;

  spdlog::set_level(spdlog::level::debug);

  auto const maybe_args = parse_args({argv, gsl::narrow<std::size_t>(argc)});
  if (not maybe_args.has_value()) {
    return 1;
  }

  auto const& args = *maybe_args;

  // Load the URDF model
  auto const model = [&]() -> pin::ModelTpl<scalar_t> {
    pin::Model m;
    pin::urdf::buildModel(
        boost::filesystem::absolute(
            args.pinocchio_path /
            "/models/others/robots/ur_description/urdf/ur5_gripper.urdf")
            .string(),
        m);
    return m.cast<scalar_t>();
  }();
  pin::DataTpl<scalar_t> data(model);
  ddp::pinocchio_dynamics_t<
      pin::ModelTpl<scalar_t>,
      pin::DataTpl<scalar_t>,
      fix<6>,
      fix<6> >
      f{model, data, args.dt, args.eval_hessians};

  {
    using namespace ddp;
    using uninit = np::uninitialized<scalar_t>;
    using Dyn_vec = np::mat<scalar_t, dyn::value, 1>;
    using std::move;

    scalar_t const mu_default = args.mu;
    scalar_t const err_threshold = args.threshold;
    isize const n_iter = args.n_iter;

    auto const nx = fix<12>{};
    auto const nu = fix<6>{};

    gu::assert_eq_runtime(nx.as_isize(), model.nq + model.nv);
    gu::assert_eq_runtime(nu.as_isize(), model.nv);

    auto T = args.horizon;
    auto x_idx = ddp::vec_regular_indexer(T + 1, nx);
    auto u_idx = ddp::vec_regular_indexer(T, nu);

    auto x_init = uninit::col(nx);

    auto Q = uninit::mat(nu, nu);
    auto q = uninit::row(nu);

    auto R = uninit::mat(nx, nx);
    auto r = uninit::row(nx);
    auto R_f = uninit::mat(nx, nx);
    auto r_f = uninit::row(nx);

    {
      top_rows(x_init, fix<6>{}).eigen() = pinocchio::neutral(model);
      fill_constant(bot_rows(x_init, fix<6>{}), 0);

      Q.eigen().setIdentity();
      fill_constant(q, 0);

      fill_constant(R, 0);
      fill_constant(R_f, 0);
      fill_constant(r, 0);
      fill_constant(r_f, 0);
    }

    auto l = [&] /* NOLINT(modernize-use-trailing-return-type) */ (isize) {
      return ddp::make_quadratic_model(np::zero_v, r, q, R, np::zero_v, Q);
    };
    auto l_f = ddp::make_univar_quadratic_model(np::zero_v, r_f, R_f);

    ddp::trajectory_t<scalar_t, decltype(x_idx), decltype(u_idx)> traj(
        x_idx, u_idx, false);

    auto update_reg = []           //
        (scalar_t reg,             //
         scalar_t const& stepsize) //
        -> boost::optional<scalar_t> {
      if (stepsize >= .49) {
        reg /= 2;
        if (reg < 1e-6) {
          reg = 0;
        }
      } else if (stepsize <= 1e-2) {
        reg *= 2;
        if (reg < 1e-2) {
          reg = 1e-2;
        }
      }
      if (reg > 1e20) {
        spdlog::info("regularization too high. stopping");
        return boost::none;
      }
      return reg;
    };

    auto const ni = fix<0>{};
    auto e_idx = custom_indexer_t{T};
    auto i_idx = ddp::vec_regular_indexer(T, ni);

    // unused
    auto X_ineq = uninit::mat(ni, nx);
    auto U_ineq = uninit::mat(ni, nu);
    auto C_ineq = uninit::col(ni);
    // end unused

    std::array<scalar_t, 6> freqs{};
    {
      std::mt19937 rng{0}; // NOLINT(cert-msc32-c,cert-msc51-cpp)
      std::uniform_real_distribution<double> dist{1, 3};
      for (auto& e : freqs) {
        e = static_cast<scalar_t>(dist(rng));
      }
    }

    auto c_eq = constraint_t<gu::remove_cvref_t<decltype(f)> >{
        T, f, [T, &x_init, &args, &freqs](isize t) -> np::dyn_vec_t<scalar_t> {
          if (t != T) {
            return uninit::col(dyn{0});
          }
          auto retval = eval(top_rows(x_init, dyn{6}));
          for (isize i = 0; i < retval.rows().as_isize(); ++i) {
            retval.eigen()(i) += args.reference_traj_norm * //
                                 sin(static_cast<scalar_t>(t) * args.dt *
                                     freqs.at(static_cast<gu::usize>(i)));
          }
          return retval;
        }};

    auto c_ineq = [&] /* NOLINT(modernize-use-trailing-return-type) */ (isize) {
      return make_linear_model(C_ineq, X_ineq, U_ineq);
    };

    auto update_on_success = gu::to_array( //
        gu::dyn_function(                  //
            [&args]                        //
            (scalar_t & mu, scalar_t & n, scalar_t & w) {
              n /= pow(mu, args.n_success_update_pow);
              w /= pow(mu, args.w_success_update_pow);
            }),
        gu::dyn_function(        //
            [&args, &mu_default] //
            (scalar_t & mu, scalar_t & n, scalar_t & w) {
              gu::unused(mu);
              n /= pow(mu, args.n_success_update_pow);
              w /= pow(mu_default, args.w_success_update_pow);
            }),
        gu::dyn_function(        //
            [&args, &mu_default] //
            (scalar_t & mu, scalar_t & n, scalar_t & w) {
              gu::unused(mu);
              n /= pow(mu_default, args.n_success_update_pow);
              w /= pow(mu_default, args.w_success_update_pow);
            }) //
    );

    auto update_on_failure = gu::to_array( //
        gu::dyn_function(                  //
            [&args]                        //
            (scalar_t & mu, scalar_t & n, scalar_t & w) {
              gu::unused(n, w);
              mu *= args.mu_update_factor;
            }),           //
        gu::dyn_function( //
            [&args]       //
            (scalar_t & mu, scalar_t & n, scalar_t & w) {
              mu *= args.mu_update_factor;
              n = 1 / pow(mu, args.n_failure_update_pow);
              w = 1 / pow(mu, args.w_failure_update_pow);
            }),                  //
        gu::dyn_function(        //
            [&args, &mu_default] //
            (scalar_t & mu, scalar_t & n, scalar_t & w) {
              scalar_t n0 = (1 / mu_default);
              scalar_t w0 = (1 / pow(mu_default, 0.1));
              mu *= args.mu_update_factor;
              n = n0 / pow(mu, args.n_failure_update_pow);
              w = w0 / pow(mu, args.w_failure_update_pow);
            }) //
    );

    auto prob = make_ddp_solver<scalar_t>(
        x_init, x_idx, u_idx, e_idx, i_idx, f, l, l_f, c_eq, c_ineq);

    auto initial_traj = prob.make_trajectory(
        [&](isize t,
            np::mat_view<
                np::mat<big_float, 12, 1, 0>,
                np::tags::detail::mem_align_t::unaligned> const& x) {
          auto u = np::uninitialized<scalar_t>::col(prob.u_idx.rows(t));
          np::fill_constant(u, 0);
          return u;
        });

    struct {
      decltype(update_reg) reg;
      decltype(update_on_success)::value_type on_success;
      decltype(update_on_failure)::value_type on_failure;
    } update_strategy = {update_reg,
                         update_on_success.at(args.success_strategy_idx),
                         update_on_failure.at(args.failure_strategy_idx)};

    auto solver_params =
        solver_parameters_t<scalar_t>{n_iter,
                                      err_threshold,
                                      mu_default,
                                      0,
                                      1 / mu_default,
                                      1 / pow(mu_default, 0.1)};

    auto logger = iterate_logger_t{prob};
    prob.solve<ddp::method::primal>(
        logger, update_strategy, solver_params, initial_traj.clone());
    prob.solve<ddp::method::primal_dual_constant_multipliers>(
        logger, update_strategy, solver_params, initial_traj.clone());
    prob.solve<ddp::method::primal_dual_affine_multipliers>(
        logger, update_strategy, solver_params, initial_traj.clone());

    {
      gsl::czstring row_fmt = "{:<5} {:>15} {:>15} {:>15} {:>15} {:>15}\n";
      fmt::print(
          row_fmt,
          "iter",
          "primal",
          "dual",
          "reg after bkw",
          "reg after fwd",
          "step size");
      auto diff = [](Dyn_vec const& u,
                     Dyn_vec const& v,
                     bool accept_different_sizes = false) -> std::string {
        if (not accept_different_sizes or
            u.rows().as_isize() == v.rows().as_isize()) {
          return to_str(stable_norm(u - v));
        }
        return "Invalid";
      };
      auto const& iterates = logger.m_iterates;
      auto const& best = logger.best();
      for (auto const& iterate : iterates) {
        fmt::print(
            stderr,
            row_fmt,
            iterate.idx,
            diff(iterate.sol.primal, best.primal),
            diff(iterate.sol.dual, best.dual, true),
            to_str(iterate.reg_after_bkw),
            to_str(iterate.reg_after_fwd),
            to_str(iterate.step));
      }

      std::ofstream out_file(fmt::format("/tmp/terminal_ss_{}.dat", args.mu));
      for (auto const& iterate : iterates) {
        fmt::print(
            out_file,
            "{} {} {} {}\n",
            iterate.idx,
            diff(iterate.sol.primal, best.primal),
            iterate.primal_optimality,
            iterate.dual_optimality);
      }
    }
  }
}

template <typename T>
using vec_cref_base = Eigen::MatrixBase<
    Eigen::Ref<const Eigen::Matrix<T, Eigen::Dynamic, 1> > > const&;

template <typename T>
using mat_ref_base = Eigen::MatrixBase<
    Eigen::Ref<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> > > const&;

extern template void pinocchio::computeABADerivatives(
    ModelTpl<big_float> const&,
    DataTpl<big_float>&,
    vec_cref_base<big_float>,
    vec_cref_base<big_float>,
    vec_cref_base<big_float>,
    mat_ref_base<big_float>,
    mat_ref_base<big_float>,
    mat_ref_base<big_float>);

extern template const typename pinocchio::DataTpl<big_float>::TangentVectorType&
pinocchio::aba(
    ModelTpl<big_float> const&,
    DataTpl<big_float>&,
    vec_cref_base<big_float>,
    vec_cref_base<big_float>,
    vec_cref_base<big_float>);

template <typename T>
using mat_cref =
    Eigen::Ref<const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >;

extern template const Eigen::LLT<
    Eigen::MatrixBase<mat_cref<big_float> >::PlainObject>
Eigen::MatrixBase<mat_cref<big_float> >::llt() const;
