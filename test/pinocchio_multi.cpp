#include "pinocchio/parsers/urdf.hpp"

#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/aba-derivatives.hpp"
#include "pinocchio/algorithm/aba.hpp"

#include "boost/range/adaptor/indexed.hpp"

#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"

#include "boost/preprocessor/tuple/elem.hpp"
#include "boost/preprocessor/comparison/equal.hpp"
#include "boost/preprocessor/facilities/empty.hpp"
#include "boost/preprocessor/punctuation/comma.hpp"

#include <cmath>
#include <functional>
#include <ios>
#include <iostream>
#include <limits>
#include <map>
#include <type_traits>
#include "boost/multiprecision/cpp_bin_float.hpp"
#include "general_utilities.hpp"
#include "model.hpp"
#include "eigen/random.hpp"
#include "indexers.hpp"
#include "ddp.hpp"

#include "fmt/format.h"
#include "fmt/ostream.h"
#include "pinocchio_dynamics_multi_shooting.hpp"

#define AS_MEM(a1, a2, Elem)                                                   \
  BOOST_PP_TUPLE_ELEM(0, Elem) BOOST_PP_TUPLE_ELEM(1, Elem);

#define AS_OPTIONAL_MEM(a1, a2, Elem)                                          \
  boost::optional<BOOST_PP_TUPLE_ELEM(0, Elem)> BOOST_PP_TUPLE_ELEM(1, Elem) = \
      boost::none;

#define AND_HAS_VALUE(a1, a2, i, Elem)                                         \
  BOOST_PP_IF(BOOST_PP_EQUAL(i, 0), BOOST_PP_EMPTY(), and)                     \
  args.BOOST_PP_TUPLE_ELEM(1, Elem).has_value()

#define DEREF(a1, a2, i, Elem)                                                 \
  BOOST_PP_IF(BOOST_PP_EQUAL(i, 0), BOOST_PP_EMPTY, BOOST_PP_COMMA)            \
  () * args.BOOST_PP_TUPLE_ELEM(1, Elem)

using Big_float = boost::multiprecision::number<
    boost::multiprecision::backends::cpp_bin_float<200> >;

using Scalar = Big_float;

#define ARG_TUPLE                                                              \
  ((Scalar, dt),                                                               \
   (gu::isize, horizon),                                                       \
   (Scalar, reference_traj_norm),                                              \
   (Scalar, ms_factor),                                                        \
   (Scalar, mu),                                                               \
   (Scalar, threshold),                                                        \
   (gu::usize, success_strategy_idx),                                          \
   (gu::usize, failure_strategy_idx),                                          \
   (Scalar, w_success_update_pow),                                             \
   (Scalar, n_success_update_pow),                                             \
   (Scalar, w_failure_update_pow),                                             \
   (Scalar, n_failure_update_pow),                                             \
   (gu::isize, n_iter),                                                        \
   (boost::filesystem::path, pinocchio_path))

#define ARG_SEQ BOOST_PP_TUPLE_TO_SEQ(ARG_TUPLE)
struct parsed_arg_type {
  BOOST_PP_SEQ_FOR_EACH(AS_OPTIONAL_MEM, _, ARG_SEQ)
};
struct arg_type {
  BOOST_PP_SEQ_FOR_EACH(AS_MEM, _, ARG_SEQ)
};

static auto from_parsed(parsed_arg_type&& args) -> boost::optional<arg_type> {
  if (BOOST_PP_SEQ_FOR_EACH_I(AND_HAS_VALUE, _, ARG_SEQ)) {
    return arg_type{BOOST_PP_SEQ_FOR_EACH_I(DEREF, _, ARG_SEQ)};
  }
  return boost::none;
}

constexpr int default_n_digits = 3;

template <typename T>
std::string to_str(
    boost::multiprecision::number<T> const& x,
    int n_digits = default_n_digits) {
  return x.str(n_digits, std::ios_base::scientific);
}

template <typename T, REQUIRES(std::is_floating_point<T>{})>
std::string to_str(T const& x, int n_digits = default_n_digits) {
  std::array<char, 32> buffer{};
  fmt::format_to(buffer.begin(), "{}:.{}e{}", "{", n_digits, "}");
  return fmt::format(buffer.data(), x);
}

auto parse_args(gsl::span<gsl::zstring> args) -> boost::optional<arg_type> {
  using boost::lexical_cast;
  using boost::string_view;
  using boost::filesystem::path;
  using gsl::czstring;
  using gu::isize;
  using gu::usize;

  parsed_arg_type result;

  auto arg_z = [&](gsl::index i) {
    return args.at(gsl::narrow_cast<std::size_t>(i));
  };
  auto arg = [&](gsl::index i) { return gsl::cstring_span{arg_z(i)}; };

  std::map<gsl::cstring_span, std::function<void(string_view)> > actions;

  actions["-seed"] = [](string_view current_arg) {
    std::srand(lexical_cast<unsigned int>(current_arg));
  };
  actions["-pinocchio"] = [&result](string_view current_arg) {
    if (current_arg.starts_with("~/")) {
      string_view home = std::getenv("HOME");
      string_view path_tail = current_arg.substr(2);
      result.pinocchio_path = path(home.begin(), home.end());
      result.pinocchio_path->append(path_tail.begin(), path_tail.end());
    } else {
      result.pinocchio_path = path(current_arg.begin(), current_arg.end());
    }
  };

  // System parameters
  actions["-t"] = [&result](string_view current_arg) {
    result.horizon = lexical_cast<isize>(current_arg);
  };
  actions["-dt"] = [&result](string_view current_arg) {
    result.dt = lexical_cast<Scalar>(current_arg);
  };
  actions["-norm"] = [&result](string_view current_arg) {
    result.reference_traj_norm = lexical_cast<Scalar>(current_arg);
  };
  actions["-factor"] = [&result](string_view current_arg) {
    result.ms_factor = lexical_cast<Scalar>(current_arg);
  };

  // Solver parameters
  actions["-mu"] = [&result](string_view current_arg) {
    result.mu = lexical_cast<Scalar>(current_arg);
  };
  actions["-err"] = [&result](string_view current_arg) {
    result.threshold = lexical_cast<Scalar>(current_arg);
  };
  actions["-niter"] = [&result](string_view current_arg) {
    result.n_iter = lexical_cast<isize>(current_arg);
  };

  actions["-success-strat"] = [&result](string_view current_arg) {
    result.success_strategy_idx = lexical_cast<usize>(current_arg);
  };
  actions["-failure-strat"] = [&result](string_view current_arg) {
    result.failure_strategy_idx = lexical_cast<usize>(current_arg);
  };

  actions["-n-success-pow"] = [&result](string_view current_arg) {
    result.n_success_update_pow = lexical_cast<Scalar>(current_arg);
  };
  actions["-w-success-pow"] = [&result](string_view current_arg) {
    result.w_success_update_pow = lexical_cast<Scalar>(current_arg);
  };
  actions["-n-failure-pow"] = [&result](string_view current_arg) {
    result.n_failure_update_pow = lexical_cast<Scalar>(current_arg);
  };
  actions["-w-failure-pow"] = [&result](string_view current_arg) {
    result.w_failure_update_pow = lexical_cast<Scalar>(current_arg);
  };

  for (gsl::index i = 1; i < args.ssize(); i += 2) {
    actions.at(arg(i))(arg_z(i + 1));
  }

  return from_parsed(std::move(result));
}

//------------------------------------------------------------------------------

template <typename T>
np::Mat<Scalar, np::Dyn::value, 1> //
to_vec(T const& mats) {
  return eval(mats.data.as_vector());
}

constexpr gu::isize Nq = 6;
constexpr gu::isize Nv = 6;

constexpr gu::isize Nx = Nq + Nv;
constexpr gu::isize Nu = Nv + Nx;

constexpr gu::isize Dof = 0;

constexpr gu::isize Ne = Nu - Dof;
constexpr gu::isize Ni = 0;

template <typename F> struct Constraint {
  using isize = gu::isize;
  template <isize Rows, isize Cols> using Mat = np::Mat<Scalar, Rows, Cols>;
  template <isize Size> using Vec = np::Mat<Scalar, Size, 1>;

  auto nc() const -> gu::Fix<Ne> { return {}; }
  auto nq() const -> gu::Fix<Nq> { return {}; }
  auto nv() const -> gu::Fix<Nv> { return {}; }
  auto nu() const -> gu::Fix<Nu> { return {}; }

  struct Derivs_2 {
    Vec<Ne> val;
    Mat<Ne, Nx> x;
    Mat<Ne, Nu> u;
    np::Zero_Scalar xx;
    np::Zero_Scalar ux;
    np::Zero_Scalar uu;
  };

  F const& dynamics;
  Scalar factor;
  std::function<Vec<Ne - Nq - Nv>(isize t)> rhs;

  template <typename InX, typename InU>
  auto operator()(isize t, InX const& x, InU const& u) const -> Vec<Ne> {
    using uninit = np::uninitialized<Scalar>;

    auto ctrl = np::top_rows(u, nv());
    auto drift = np::bot_rows(u, nv() + nv());

    auto f1 = dynamics.old_f(x, ctrl);
    auto f2 = dynamics.old_f(f1, uninit::col(ctrl.rows()));
    auto retval = uninit::col(nc());

    auto rhs_size = nc() - nq() - nv();
    np::top_rows(retval, rhs_size) = np::top_rows(f2, rhs_size) - rhs(t + 2);
    np::bot_rows(retval, nv() + nv()) = factor * drift;

    return retval;
  }

  template <typename InX, typename InU>
  auto derivs_2(isize t, InX const& x, InU const& u) const -> Derivs_2 {
    using np::clone;
    using np::eval;
    using uninit = np::uninitialized<Scalar>;

    auto rhs_size = nc() - nq() - nv();

    auto ctrl = np::top_rows(u, nv());
    auto drift = np::bot_rows(u, nv() + nv());

    auto f1 = dynamics.old_derivs_2(x, ctrl);
    auto f2 = dynamics.old_derivs_2(f1.val, uninit::col(ctrl.rows()));

    auto val = uninit::col(nc());
    auto jx = uninit::mat(nc(), nv() + nv());
    auto ju = uninit::mat(nc(), nu());

    np::top_rows(val, rhs_size) = np::top_rows(f2.val, rhs_size) - rhs(t + 2);
    np::bot_rows(val, nv() + nv()) = factor * drift;

    np::top_rows(jx, rhs_size) = np::top_rows(f2.x * f1.x, rhs_size);
    np::fill_constant(np::bot_rows(jx, nv() + nv()), 0);

    // top left
    np::top_left_block(ju, rhs_size, nv()) =
        np::top_rows(f2.x * f1.u, rhs_size);
    // top right
    np::fill_constant(np::top_right_block(ju, rhs_size, nv() + nv()), 0);
    // bot left
    np::fill_constant(np::bot_left_block(ju, nv() + nv(), nv()), 0);
    // bot right
    np::bot_right_block(ju, nv() + nv(), nv() + nv()).eigen().setIdentity();
    np::bot_right_block(ju, nv() + nv(), nv() + nv()) *= factor;

    auto hxx = np::zero_v;
    auto hux = np::zero_v;
    auto huu = np::zero_v;

    using std::move;
    return {move(val), move(jx), move(ju), move(hxx), move(hux), move(huu)};
  }

  struct Functor {
    gu::cref_t<Constraint> parent;
    isize t;
    template <typename InX, typename InU>
    auto operator()(InX const& x, InU const& u) const -> Vec<Ne> {
      return parent(t, x, u);
    }

    template <typename InX, typename InU>
    auto derivs_2(InX const& x, InU const& u) const -> Derivs_2 {
      return parent.derivs_2(t, x, u);
    }
  };
  auto operator()(isize t) const -> Functor { return {*this, t}; }
};

template <typename Prob, typename Traj, typename Mults, typename Derivs>
auto optimality(
    Prob const& p,
    Traj const& traj,
    Mults const& mults,
    Scalar const& mu,
    Derivs const& derivs) -> std::tuple<Scalar, Scalar, Scalar> {
  return std::make_tuple(
      p.optimality_constr(derivs),
      p.optimality_obj(traj, mults, mu, derivs),
      p.optimality_lag(traj, mults, derivs));
}

int main(int argc, char** argv) {
  using gu::Dyn;
  using gu::Fix;
  using gu::isize;
  namespace pin = pinocchio;

  spdlog::set_level(spdlog::level::debug);

  auto const maybe_args = parse_args({argv, gsl::narrow<std::size_t>(argc)});
  if (not maybe_args.has_value()) {
    return 1;
  }

  auto const& args = *maybe_args;

  // Load the URDF model
  pin::ModelTpl<Scalar> const model = [&]() //
  {
    pin::Model m;
    pin::urdf::buildModel(
        boost::filesystem::absolute(
            args.pinocchio_path /
            "/models/others/robots/ur_description/urdf/ur5_gripper.urdf")
            .string(),
        m);
    return m.cast<Scalar>();
  }();
  pin::DataTpl<Scalar> data(model);
  ddp::Pinocchio_dynamics_ms<
      pin::ModelTpl<Scalar>,
      pin::DataTpl<Scalar>,
      Fix<Nq>,
      Fix<Nv> >
      f{model, data, args.dt, false};

  {
    using namespace ddp;
    using uninit = np::uninitialized<Scalar>;
    using Dyn_vec = np::Mat<Scalar, Dyn::value, 1>;
    using std::move;

    Scalar const mu_default = args.mu;
    Scalar const err_threshold = args.threshold;
    isize const n_iter = args.n_iter;

    auto const nx = Fix<Nx>{};
    auto const nv = Fix<Nv>{};
    auto const nu = Fix<Nu>{};

    gu::assert_eq_runtime(nx.inner(), model.nq + model.nv);
    gu::assert_eq_runtime(nu.inner(), nx.inner() + model.nv);

    auto T = args.horizon;
    auto x_idx = ddp::packed_vec_regular_indexer(T + 1, nx);
    auto u_idx = ddp::packed_vec_regular_indexer(T, nu);

    auto x_init = uninit::col(nx);

    auto Q = uninit::mat(nu, nu);
    auto q = uninit::row(nu);

    auto R = uninit::mat(nx, nx);
    auto r = uninit::row(nx);
    auto R_f = uninit::mat(nx, nx);
    auto r_f = uninit::row(nx);

    {
      top_rows(x_init, Fix<Nq>{}).eigen() = pinocchio::neutral(model);
      fill_constant(bot_rows(x_init, Fix<Nq>{}), 0);

      fill_constant(Q, 0);
      fill_constant(q, 0);
      np::top_left_block(Q, nv, nv).eigen().setIdentity();

      fill_constant(R, 0);
      fill_constant(R_f, 0);
      fill_constant(r, 0);
      fill_constant(r_f, 0);
    }

    auto l = [&](isize) {
      return ddp::make_quadratic_model(np::zero_v, r, q, R, np::zero_v, Q);
    };
    auto l_f = ddp::make_univar_quadratic_model(np::zero_v, r_f, R_f);

    ddp::Trajectory<Scalar, decltype(x_idx), decltype(u_idx)> traj(
        x_idx, u_idx, false);

    auto end_log = [&] {
      spdlog::debug("final trajectory (origin shifted to target)");
      for (auto const& iterate : traj) {
        spdlog::debug(
            "t = {}\n{}\n{}\n",
            iterate.time_idx(),
            (iterate.x() - x_init).T(),
            iterate.u().T());
      }
      spdlog::debug("final\n{}", (traj.x_f() - x_init).T());
    };

    auto update_reg = []         //
        (Scalar reg,             //
         Scalar const& stepsize) //
        -> boost::optional<Scalar> {
      if (stepsize >= .49) {
        reg /= 2;
        if (reg < 1e-6) {
          reg = 0;
        }
      } else if (stepsize <= 1e-2) {
        reg *= 2;
        if (reg < 1e-2) {
          reg = 1e-2;
        }
      }
      if (reg > 1e20) {
        spdlog::info("regularization too high. stopping");
        return boost::none;
      }
      return reg;
    };

    struct Iterate {
      isize idx;
      Dyn_vec primal;
      Dyn_vec dual;
      Dyn_vec feedback;
      Scalar reg_after_bkw;
      Scalar reg_after_fwd;
      Scalar step;
    };
    std::vector<Iterate> iterates;
    boost::optional<Dyn_vec> best_primal = boost::none;
    boost::optional<Dyn_vec> best_dual = boost::none;

    {
      auto const ne = Fix<Ne>{};
      auto const ni = Fix<Ni>{};
      auto e_idx = ddp::packed_vec_regular_indexer(T, ne);
      auto i_idx = ddp::packed_vec_regular_indexer(T, ni);

      // unused
      auto X_ineq = uninit::mat(ni, nx);
      auto U_ineq = uninit::mat(ni, nu);
      auto C_ineq = uninit::col(ni);
      // end unused

      std::array<Scalar, ne.inner()> freqs{};
      for (auto& e : freqs) {
        Scalar r_max{RAND_MAX};
        e = (std::rand() / r_max) * 2 + 1;
      }

      auto c_eq = Constraint<gu::remove_cvref_t<decltype(f)> >{
          f, args.ms_factor, [&x_init, ne, nx, &args, &freqs](isize t) {
            auto retval = eval(top_rows(x_init, ne - nx));
            for (isize i = 0; i < retval.rows().inner(); ++i) {
              retval.eigen()(i) +=
                  args.reference_traj_norm *
                  sin(t * args.dt * freqs.at(static_cast<gu::usize>(i)));
            }
            return retval;
          }};

      auto c_ineq = [&](isize) {
        return make_linear_model(C_ineq, X_ineq, U_ineq);
      };

      auto prob = make_ddp_solver<Scalar>(
          x_init, x_idx, u_idx, e_idx, i_idx, f, l, l_f, c_eq, c_ineq);

      auto derivs = prob.uninit_derivative_storage(traj);

      auto reset_traj = [&] {
        traj = prob.make_trajectory([nu, nv, &f, &x_init](isize) {
          auto u = uninit::col(nu);

          fill_constant(np::top_rows(u, nv), 0);
          np::bot_rows(u, nv + nv) = -f.old_f(x_init, np::top_rows(u, nv));

          return u;
        });
      };

      auto log_optimality = [](isize iter,
                               Scalar const& opt_cstr,
                               Scalar const& opt_obj,
                               Scalar const& mu,
                               Scalar const& reg,
                               Scalar const& w,
                               Scalar const& n) {
        fmt::print(
            "{:<5} {:>13} {:>13} {:>13} {:>13} {:>13} {:>13}\n",
            iter,
            to_str(opt_cstr),
            to_str(opt_obj),
            to_str(mu),
            to_str(reg),
            to_str(n),
            to_str(w));
      };

      auto update_on_success = gu::make_array( //
          gu::make_function(                   //
              [&args]                          //
              (Scalar & mu, Scalar & n, Scalar & w) {
                n /= pow(mu, args.n_success_update_pow);
                w /= pow(mu, args.w_success_update_pow);
              }),
          gu::make_function(       //
              [&args, &mu_default] //
              (Scalar & mu, Scalar & n, Scalar & w) {
                gu::unused(mu);
                n /= pow(mu, args.n_success_update_pow);
                w /= pow(mu_default, args.w_success_update_pow);
              }),
          gu::make_function(       //
              [&args, &mu_default] //
              (Scalar & mu, Scalar & n, Scalar & w) {
                gu::unused(mu);
                n /= pow(mu_default, args.n_success_update_pow);
                w /= pow(mu_default, args.w_success_update_pow);
              }) //
      );

      auto update_on_failure = gu::make_array( //
          gu::make_function(                   //
              []                               //
              (Scalar & mu, Scalar & n, Scalar & w) {
                gu::unused(n, w);
                mu *= 100;
              }),            //
          gu::make_function( //
              [&args]        //
              (Scalar & mu, Scalar & n, Scalar & w) {
                mu *= 100;
                n = 1 / pow(mu, args.n_failure_update_pow);
                w = 1 / pow(mu, args.w_failure_update_pow);
              }),                  //
          gu::make_function(       //
              [&args, &mu_default] //
              (Scalar & mu, Scalar & n, Scalar & w) {
                Scalar n0 = (1 / mu_default);
                Scalar w0 = (1 / pow(mu_default, 0.1));
                mu *= 100;
                n = n0 / pow(mu, args.n_failure_update_pow);
                w = w0 / pow(mu, args.w_failure_update_pow);
              }) //
      );

      {
        reset_traj();
        fmt::print("Initial trajectory\n");
        for (auto xu : traj) {
          fmt::print(" > {}\n", xu.x().T());
        }
        Scalar lowest_optimality = std::numeric_limits<Scalar>::infinity();
        auto mults = prob.make_multipliers_primal_zero();

        best_primal.emplace(to_vec(traj));
        best_dual.emplace(to_vec(mults.eq));

        Scalar reg = 0.0;

        Scalar mu = mu_default;
        Scalar w = 1 / mu;
        Scalar n = 1 / pow(mu, 0.1);

        for (isize i = 0; i < n_iter; ++i) {
          prob.compute_derivatives(traj, derivs);
          BIND(                                         //
              auto,                                     //
              (opt_cstr, opt_obj, opt_lag),             //
              optimality(prob, traj, mults, mu, derivs) //
          );

          log_optimality(i, opt_cstr, opt_obj, mu, reg, w, n);
          {
            auto const& opt = std::max(opt_lag, opt_cstr);
            if (opt < lowest_optimality) {
              lowest_optimality = opt;
              best_primal = to_vec(traj);
              best_dual = to_vec(mults.eq);
            }

            if (opt < err_threshold) {
              break;
            }
          }

          auto b_res = prob.backward_pass_primal(traj, mults, reg, mu, derivs);
          auto f_res = prob.forward_pass_primal(traj, mults, b_res, true);

          {
            auto new_reg = update_reg(b_res.regularization, f_res.step);
            if (new_reg.has_value()) {
              reg = *new_reg;
            } else {
              break;
            }
          }

          iterates.push_back(
              {i,
               to_vec(traj),
               to_vec(mults.eq),
               to_vec(b_res.feedback),
               b_res.regularization,
               reg,
               f_res.step});

          if (opt_obj < w) {
            if (opt_cstr < n) {
              mults = move(f_res.mults);
              update_on_success.at(args.success_strategy_idx)(mu, n, w);
            } else {
              update_on_failure.at(args.failure_strategy_idx)(mu, n, w);
            }
          }

          traj = move(f_res.new_traj);
        }

        end_log();
        iterates.push_back(
            {iterates.back().idx + 1,
             to_vec(traj),
             to_vec(mults.eq),
             uninit::col(Dyn{0}),
             reg,
             reg,
             0.0});
      }
      {
        reset_traj();
        Scalar lowest_optimality = std::numeric_limits<Scalar>::infinity();
        auto mults = prob.make_multipliers_primal_zero();
        Scalar reg = 0.0;

        Scalar mu = mu_default;
        Scalar w = 1 / mu;
        Scalar n = 1 / pow(mu, 0.1);

        for (isize i = 0; i < n_iter; ++i) {
          prob.compute_derivatives(traj, derivs);
          BIND(
              auto,
              (opt_cstr, opt_obj, opt_lag),
              optimality(prob, traj, mults, mu, derivs));

          log_optimality(i, opt_cstr, opt_obj, mu, reg, w, n);
          {
            auto const& opt = std::max(opt_lag, opt_cstr);
            if (opt < lowest_optimality) {
              lowest_optimality = opt;
              best_primal = to_vec(traj);
              best_dual = to_vec(mults.eq);
            }
            if (opt < err_threshold) {
              break;
            }
          }

          auto b_res =
              prob.backward_pass_primal_dual(traj, mults, reg, mu, derivs);
          auto f_res = prob.forward_pass_primal_dual(traj, mults, b_res, true);

          {
            auto new_reg = update_reg(b_res.regularization, f_res.step);
            if (new_reg.has_value()) {
              reg = *new_reg;
            } else {
              break;
            }
          }
          iterates.push_back(
              {i,
               to_vec(traj),
               to_vec(mults.eq),
               to_vec(b_res.feedback),
               b_res.regularization,
               reg,
               f_res.step});

          if (opt_obj < w) {
            if (opt_cstr < n) {
              mults = move(f_res.mults);
              update_on_success.at(args.success_strategy_idx)(mu, n, w);
            } else {
              update_on_failure.at(args.failure_strategy_idx)(mu, n, w);
            }
          }

          traj = move(f_res.new_traj);
        }
        end_log();
        iterates.push_back(
            {iterates.back().idx + 1,
             to_vec(traj),
             to_vec(mults.eq),
             uninit::col(Dyn{0}),
             reg,
             reg,
             0.0});
      }
      {
        reset_traj();
        Scalar lowest_optimality = std::numeric_limits<Scalar>::infinity();
        auto mults = prob.make_multipliers_zero();
        auto mults_tmp = prob.make_multipliers_primal_zero();
        Scalar reg = 0.0;

        Scalar mu = mu_default;
        Scalar w = 1 / mu;
        Scalar n = 1 / pow(mu, 0.1);

        for (isize i = 0; i < n_iter; ++i) {
          prob.compute_derivatives(traj, derivs);
          BIND(
              auto,
              (opt_cstr, opt_obj, opt_lag),
              optimality(prob, traj, mults, mu, derivs));

          log_optimality(i, opt_cstr, opt_obj, mu, reg, w, n);
          {
            for (auto cur : gu::ranges::zip(traj, mults.eq, mults_tmp.eq)) {
              BIND(auto, (xu, m_eq, m_eq_out), cur);
              m_eq_out.val() = m_eq(xu.x());
            }
            auto const& opt = std::max(opt_lag, opt_cstr);
            if (opt < lowest_optimality) {
              lowest_optimality = opt;
              best_primal = to_vec(traj);
              best_dual = to_vec(mults_tmp.eq);
            }
            if (opt < err_threshold) {
              break;
            }
          }

          auto b_res =
              prob.backward_pass_primal_dual_fn(traj, mults, reg, mu, derivs);
          auto f_res =
              prob.forward_pass_primal_dual_fn(traj, mults, b_res, true);

          {
            auto new_reg = update_reg(b_res.regularization, f_res.step);
            if (new_reg.has_value()) {
              reg = *new_reg;
            } else {
              break;
            }
          }

          {
            iterates.push_back(
                {i,
                 to_vec(traj),
                 to_vec(mults_tmp.eq),
                 to_vec(b_res.feedback),
                 b_res.regularization,
                 reg,
                 f_res.step});
          }

          if (opt_obj < w) {
            if (opt_cstr < n) {
              mults = move(f_res.mults);
              update_on_success.at(args.success_strategy_idx)(mu, n, w);
            } else {
              update_on_failure.at(args.failure_strategy_idx)(mu, n, w);
            }
          }

          traj = move(f_res.new_traj);
        }
        end_log();

        for (auto cur : gu::ranges::zip(traj, mults.eq, mults_tmp.eq)) {
          BIND(auto, (xu, m_eq, m_eq_out), cur);
          m_eq_out.val() = m_eq(xu.x());
        }
        iterates.push_back(
            {iterates.back().idx + 1,
             to_vec(traj),
             to_vec(mults_tmp.eq),
             uninit::col(Dyn{0}),
             reg,
             reg,
             0.0});
      }
    }
    {
      gsl::czstring row_fmt = "{:<5} {:>13} {:>13} {:>13} {:>13} {:>13}\n";
      fmt::print(
          row_fmt,
          "iter",
          "primal",
          "dual",
          "reg after bkw",
          "reg after fwd",
          "step size");
      auto diff = [](Dyn_vec const& u,
                     Dyn_vec const& v,
                     bool accept_different_sizes = false) -> std::string {
        if (not accept_different_sizes or
            u.rows().inner() == v.rows().inner()) {
          return to_str(stable_norm(u - v));
        }
        return "Invalid";
      };

      for (auto const& iterate : iterates) {
        fmt::print(
            stderr,
            row_fmt,
            iterate.idx,
            diff(iterate.primal, *best_primal),
            diff(iterate.dual, *best_dual, true),
            to_str(iterate.reg_after_bkw),
            to_str(iterate.reg_after_fwd),
            to_str(iterate.step));
      }

      std::ofstream out_file(
          fmt::format("/tmp/ms_{}_{}.dat", args.mu, args.ms_factor));
      for (auto const& iterate : iterates) {
        fmt::print(
            out_file,
            "{}  {} \n",
            iterate.idx,
            diff(iterate.primal, *best_primal));
      }
    }
  }
}
