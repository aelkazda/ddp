#include "eigen/tensor.hpp"
#include "eigen/random.hpp"
#include "fmt/format.h"
#include "fmt/ostream.h"

int main() {
  using Scalar = double;
  using gu::Fix;
  constexpr Fix<5> in_l;
  constexpr Fix<6> in_r;
  constexpr Fix<4> out;

  auto x = np::uninitialized<Scalar>::ten(in_l, in_r, out);
  x.as_matrix().eigen().setRandom();

  using co = fmt::color;
  auto pass = []() {
    fmt::print(
        fmt::fg(co::white)                  //
            | fmt::bg(co::cornflower_blue), //
        " pass ");
    fmt::print("\n");
  };
  auto fail = []() {
    fmt::print(
        fmt::fg(co::white)           //
            | fmt::bg(co::dark_red), //
        " fail ");
    fmt::print("\n");
  };
  auto pass_if = [&](bool cond) {
    if (cond) {
      pass();
    } else {
      fail();
    }
  };

  {
    auto v = np::Random<Scalar>::col(out);

    fmt::print("prod\n{}\n", (x % v).eigen());
    auto tmp = np::uninitialized<Scalar>::mat(in_l, in_r);
    tmp.eigen().setZero();

    for (int i = 0; i < out.inner(); ++i) {
      auto view =
          Eigen::Map<Eigen::Matrix<Scalar, in_l.inner(), in_r.inner()> >{
              x.data() + i * (in_l * in_r).inner()};

      tmp.eigen() += view * v.eigen()[i];
    }
    fmt::print("prod manual\n{}\n", tmp.eigen());
    Scalar diff = np::norm(tmp - x % v);
    fmt::print("Difference = {:60}", fmt::format("{}", diff));
    pass_if(diff < 1e-8);
  }
  {
    auto v = np::Random<Scalar>::row(in_l);
    fmt::print("prod\n{}\n", np::trans(v * x).eigen());

    auto tmp = np::uninitialized<Scalar>::mat(in_r, out);
    tmp.eigen().setZero();
    for (int i = 0; i < out.inner(); ++i) {
      auto view =
          Eigen::Map<Eigen::Matrix<Scalar, in_l.inner(), in_r.inner()> >{
              x.data() + i * (in_l * in_r).inner()};

      tmp.eigen().col(i) += v.eigen() * view;
    }
    fmt::print("prod manual\n{}\n", tmp.eigen());
    Scalar diff = np::norm(tmp - np::trans(v * x));
    fmt::print("Difference = {:60}", fmt::format("{}", diff));
    pass_if(diff < 1e-8);
  }
  {
    auto v = np::Random<Scalar>::col(in_r);
    fmt::print("{}\n", np::trans(v));
    v.eigen().setOnes();
    fmt::print("prod\n{}\n", np::trans(x * v).eigen());
    auto tmp = np::uninitialized<Scalar>::mat(in_l, out);
    tmp.eigen().setZero();
    for (int i = 0; i < out.inner(); ++i) {
      auto view =
          Eigen::Map<Eigen::Matrix<Scalar, in_l.inner(), in_r.inner()> >{
              x.data() + i * (in_l * in_r).inner()};
      tmp.eigen().col(i) += view * v.eigen();
    }
    fmt::print("prod manual\n{}\n", tmp);
    Scalar diff = np::norm(tmp - np::trans(x * v));
    fmt::print("Difference = {:60}", fmt::format("{}", diff));
    pass_if(diff < 1e-8);
  }
  {
    auto v1 = np::Random<Scalar>::col(in_r);
    auto v2 = np::Random<Scalar>::row(in_l);
    fmt::print("l then r\n{}\n", np::trans((v2 * x) * v1));
    fmt::print("r then l\n{}\n", v2 * np::trans(x * v1));
    Scalar diff = np::norm(np::trans((v2 * x) * v1) - v2 * np::trans(x * v1));
    fmt::print("Difference = {:60}", fmt::format("{}", diff));
    pass_if(diff < 1e-8);
  }
}
