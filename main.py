import matplotlib.pyplot as plt
from itertools import islice

from sys import argv
from typing import List

argv: List[str]
if len(argv) < 2:
    exit()
if len(argv) == 4:
    min_n_iter = int(argv[2])
    max_n_iter = int(argv[3])
else:
    min_n_iter = 0
    max_n_iter = 10000

plt.style.use("ggplot")
with open(argv[1]) as f:
    L = f.readlines()

dists = []
primals = []
duals = []
n_iters = []

for l in L:
    l: str
    idx, dist, primal, dual = l.split()
    idx = int(idx)
    dist = float(dist)
    primal = float(primal)
    dual = float(dual)

    if idx == 0:
        dists.append([])
        primals.append([])
        duals.append([])
        n_iters.append(0)
        idx += 1
    dists[-1].append(dist)
    primals[-1].append(primal)
    duals[-1].append(dual)
    n_iters[-1] += 1

trim = lambda r: list(islice(r, min_n_iter, max_n_iter))

fig, ax = plt.subplots(1, 3, figsize=[4.8*3, 4.8], sharey=True)
plt.tight_layout()

titles = ["primal", "primal dual", "primal dual fn(x)"]
for i in range(3):
    ax[i].set_title(titles[i])
    x = trim(range(n_iters[i]))
    ax[i].semilogy(x, trim(dists[i]))
    ax[i].semilogy(x, trim(primals[i]))
    ax[i].semilogy(x, trim(duals[i]))
    ax[i].legend(["Distance to x*", "Primal optimality", "Dual optimality"])

print(f"{argv[1][:-4]}.pdf")
fig.savefig(f"{argv[1][:-4]}.pdf")
