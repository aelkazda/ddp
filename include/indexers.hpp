#ifndef INCLUDE_INDEXERS_HPP_HIXE7FRK
#define INCLUDE_INDEXERS_HPP_HIXE7FRK

#include <cstddef>
#include <iterator>
#include <tuple>
#include <type_traits>
#include "eigen/forward_declarations.hpp"
#include "eigen/matrix.hpp"
#include "general_utilities.hpp"

#define DDP_DEFINE_POSTFIX(Class_Name)                                         \
  auto operator++(int)->Class_Name {                                           \
    Class_Name cur = *this;                                                    \
    ++(*this);                                                                 \
    return cur;                                                                \
  }                                                                            \
  auto operator--(int)->Class_Name {                                           \
    Class_Name cur = *this;                                                    \
    --(*this);                                                                 \
    return cur;                                                                \
  }                                                                            \
  static char _dummy_##__LINE__

namespace ddp {
using gu::isize;
namespace detail {

template <typename Indexer> struct indexer_iter_t;

template <typename Indexer> struct indexer_proxy_t {
  Indexer const& m_indexer;
  isize m_steps_so_far;
  isize m_memory_offset;

  using cols_t = typename Indexer::cols_t;
  using rows_t = typename Indexer::rows_t;

  auto rows() const -> rows_t { return m_indexer.rows(m_steps_so_far); }
  auto cols() const -> cols_t { return m_indexer.cols(m_steps_so_far); }

  auto time_idx() const -> isize { return m_steps_so_far; }
  auto to_forward_iterator() const -> indexer_iter_t<Indexer>;
};

template <typename Indexer> struct indexer_iter_t {
  using proxy = indexer_proxy_t<Indexer>;

  using difference_type = std::ptrdiff_t;
  using value_type = void;
  using pointer = proxy*;
  using reference = proxy;
  using iterator_category = std::bidirectional_iterator_tag;

  Indexer const* m_indexer_ptr;
  isize m_steps_so_far;
  isize m_memory_offset;

  auto operator==(indexer_iter_t other) const -> bool {
    return m_steps_so_far == other.m_steps_so_far;
  }
  auto operator!=(indexer_iter_t other) const -> bool {
    return not(*this == other);
  }
  auto operator++() -> indexer_iter_t& {
    if (m_indexer_ptr == nullptr) {
      std::terminate();
    }
    m_memory_offset += m_indexer_ptr->stride(m_steps_so_far).as_isize();
    ++m_steps_so_far;
    return *this;
  }
  auto operator--() -> indexer_iter_t& {
    if (m_indexer_ptr == nullptr) {
      std::terminate();
    }
    --m_steps_so_far;
    m_memory_offset -= m_indexer_ptr->stride(m_steps_so_far).as_isize();
    return *this;
  }
  DDP_DEFINE_POSTFIX(indexer_iter_t);

  auto operator*() const -> proxy {
    return {*m_indexer_ptr, m_steps_so_far, m_memory_offset};
  }
};

template <typename Indexer>
auto indexer_proxy_t<Indexer>::to_forward_iterator() const
    -> indexer_iter_t<Indexer> {
  return {&m_indexer, m_steps_so_far, m_memory_offset};
}

template <typename IdxL, typename IdxR> struct row_concat_indexer_t {

  using rows_t = gu::add_t<typename IdxL::rows_t, typename IdxR::rows_t>;
  using cols_t = typename IdxR::cols_t;

  isize n_steps;
  isize m_total_size;
  IdxL idx_left;
  IdxR idx_right;

  row_concat_indexer_t(IdxL l, IdxR r)
      : n_steps{l.n_steps},
        m_total_size{l.total_size() + r.total_size()},
        idx_left{std::move(l)},
        idx_right{std::move(r)} {

    gu::assert_eq_runtime(idx_left.n_steps, idx_right.n_steps);
    for (isize t = 0; t < n_steps; ++t) {
      gu::assert_eq(idx_left.cols(t), idx_right.cols(t));
    }
  }

  auto total_size() const -> isize { return m_total_size; }
  auto stride(isize t) const -> gu::mul_t<rows_t, cols_t> {
    return idx_left.stride(t) + idx_right.stride(t);
  }

  auto rows(isize t) const -> rows_t {
    return idx_left.rows(t) + idx_right.rows(t);
  }
  auto cols(isize t) const -> cols_t { return idx_left.cols(t); }

  using iterator = indexer_iter_t<row_concat_indexer_t>;
  using reverse_iterator = std::reverse_iterator<iterator>;

  auto begin() const -> iterator { return {this, 0, 0}; }
  auto end() const -> iterator { return {this, n_steps, total_size()}; }
  auto rbegin() const -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() const -> reverse_iterator { return reverse_iterator{begin()}; }
};

template <typename IdxL, typename IdxR> struct outer_prod_indexer_t {

  static_assert(IdxL::cols_t::value == 1, "");
  static_assert(IdxR::cols_t::value == 1, "");

  using rows_t = typename IdxL::rows_t;
  using cols_t = typename IdxR::rows_t;

  isize n_steps;
  isize m_total_size;
  IdxL idx_left;
  IdxR idx_right;

  outer_prod_indexer_t(IdxL l, IdxR r)
      : n_steps{l.n_steps},
        m_total_size{0},
        idx_left{std::move(l)},
        idx_right{std::move(r)} {

    gu::assert_eq_runtime(idx_left.n_steps, idx_right.n_steps);
    for (isize t = 0; t < n_steps; ++t) {
      m_total_size += stride(t).as_isize();
    }
  }

  auto total_size() const -> isize { return m_total_size; }
  auto stride(isize t) const -> gu::mul_t<rows_t, cols_t> {
    return idx_left.rows(t) * idx_right.rows(t);
  }

  auto rows(isize t) const -> rows_t { return idx_left.rows(t); }
  auto cols(isize t) const -> cols_t { return idx_right.rows(t); }

  using iterator = indexer_iter_t<outer_prod_indexer_t>;
  using reverse_iterator = std::reverse_iterator<iterator>;

  auto begin() const -> iterator { return {this, 0, 0}; }
  auto end() const -> iterator { return {this, n_steps, total_size()}; }
  auto rbegin() const -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() const -> reverse_iterator { return reverse_iterator{begin()}; }
};

template <typename Rows, typename Cols>
struct regular_indexer_t : np::shape_t<Rows, Cols> {
  isize n_steps;

  using rows_t = Rows;
  using cols_t = Cols;

private:
  using shape_t = np::shape_t<rows_t, cols_t>;

public:
  regular_indexer_t(isize n, rows_t rows, cols_t cols)
      : np::shape_t<rows_t, cols_t>{rows, cols}, n_steps(n) {}

  auto total_size() const -> isize { return stride(0).as_isize() * n_steps; }

  auto rows(isize) const -> rows_t { return shape_t::rows(); }
  auto cols(isize) const -> cols_t { return shape_t::cols(); }

  auto stride(isize t) const -> gu::mul_t<rows_t, cols_t> {
    return rows(t) * cols(t);
  }

  using iterator = indexer_iter_t<regular_indexer_t>;
  using const_iterator = iterator;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() const -> iterator { return {this, 0, 0}; }
  auto end() const -> iterator { return {this, n_steps, total_size()}; }
  auto rbegin() const -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() const -> reverse_iterator { return reverse_iterator{begin()}; }
};

template <typename Indexer> struct periodic_indexer_t {
  Indexer m_idx;
  isize m_period;
  isize m_delay;
  isize n_steps;
  isize m_total_size;

  using rows_t = np::dyn;
  using cols_t = np::fix<1>;

private:
  using shape_t = np::shape_t<rows_t, cols_t>;

public:
  periodic_indexer_t(Indexer idx, isize period, isize delay) noexcept
      : m_idx{idx},
        m_period{period},
        m_delay{delay},
        n_steps{idx.n_steps},
        m_total_size{0} {

    for (isize t = 0; t < n_steps; ++t) {
      m_total_size += stride(t).as_isize();
    }
    gu::assert_less_or_equal_runtime(0, period - 1);
    gu::assert_less_or_equal_runtime(delay, period - 1);
  }

  auto total_size() const noexcept -> isize {
    return stride(0).as_isize() * n_steps;
  }

  auto unfiltered_rows(isize t) const noexcept -> typename Indexer::rows_t {
    return m_idx.rows(t);
  }

  auto rows(isize t) const noexcept -> np::dyn {
    return (t % m_period == m_delay) ? np::dyn{unfiltered_rows(t)} : np::dyn{0};
  }

  auto cols(isize) const -> np::fix<1> { return np::fix<1>{}; }

  auto stride(isize t) const -> np::dyn { return rows(t) * cols(t); }

  using iterator = indexer_iter_t<periodic_indexer_t>;
  using const_iterator = iterator;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() const -> iterator { return {this, 0, 0}; }
  auto end() const -> iterator { return {this, n_steps, total_size()}; }
  auto rbegin() const -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() const -> reverse_iterator { return reverse_iterator{begin()}; }
};
} // namespace detail

template <typename... Args>
auto outer_prod(Args... args) -> detail::outer_prod_indexer_t<Args...> {
  return {std::move(args)...};
}

template <typename... Args>
auto row_concat(Args... args) -> detail::row_concat_indexer_t<Args...> {
  return {std::move(args)...};
}

template <typename Idx, typename... Args>
auto periodic_indexer(Idx idx, Args... args)
    -> detail::periodic_indexer_t<Idx> {
  return {std::move(idx), std::move(args)...};
}

template <typename Rows, typename Cols>
auto mat_regular_indexer(isize n, Rows rows, Cols cols)
    -> detail::regular_indexer_t<Rows, Cols> {
  return {n, rows, cols};
}

template <typename Rows>
auto vec_regular_indexer(isize n, Rows rows)
    -> detail::regular_indexer_t<Rows, gu::fix<1> > {
  return {n, rows, np::fix<1>{}};
}
} // namespace ddp

#endif /* end of include guard: INCLUDE_INDEXERS_HPP_HIXE7FRK */
