#ifndef INCLUDE_TRAJ_HPP_W8EAKEL6
#define INCLUDE_TRAJ_HPP_W8EAKEL6

#include "eigen/forward_declarations.hpp"
#include "eigen/matrix.hpp"
#include "general_utilities.hpp"
#include "mat_seq.hpp"

namespace ddp {
template <typename Scalar_, typename StateIdx, typename ControlIdx>
struct trajectory_t {
  using isize = gu::isize;
  using scalar_t = Scalar_;

  using x_indexer_t = StateIdx;
  using u_indexer_t = ControlIdx;

  using x_mat_seq_t = detail::mat_seq_t<scalar_t, x_indexer_t>;
  using u_mat_seq_t = detail::mat_seq_t<scalar_t, u_indexer_t>;

  using vec_x_t = typename x_mat_seq_t::matrix_t;
  using vec_u_t = typename u_mat_seq_t::matrix_t;

  x_mat_seq_t m_state_data;
  u_mat_seq_t m_control_data;

  explicit trajectory_t(
      x_mat_seq_t&& x_data, u_mat_seq_t&& u_data, bool init = true)
      : m_state_data(std::move(x_data)), m_control_data(std::move(u_data)) {
    if (init) {
      set_zero();
    }
  }

  trajectory_t(x_indexer_t x_idx, u_indexer_t u_idx, bool init = true)
      : trajectory_t(
            x_mat_seq_t{std::move(x_idx)},
            u_mat_seq_t{std::move(u_idx)},
            init) {}

  trajectory_t(const trajectory_t&) = delete;
  trajectory_t(trajectory_t&&) noexcept = default;
  auto operator=(const trajectory_t&) -> trajectory_t& = delete;
  auto operator=(trajectory_t&& other) noexcept -> trajectory_t& = default;
  ~trajectory_t() = default;

  // --------------------------------------------------------------------------------

  void set_zero() {
    for (auto xu : *this) {
      xu.x().eigen().setZero();
      xu.u().eigen().setZero();
    }
    x_f().eigen().setZero();
  }
  auto clone() const -> trajectory_t {
    return trajectory_t{m_state_data.clone(), m_control_data.clone()};
  }
  auto horizon() const -> isize { return m_state_data.idx.n_steps - 1; }

  template <bool IsConst> struct proxy_t {
    using x_proxy_t = typename x_mat_seq_t::template proxy_t<IsConst>;
    using u_proxy_t = typename u_mat_seq_t::template proxy_t<IsConst>;
    x_proxy_t m_state_proxy;
    u_proxy_t m_control_proxy;

    auto x() const RETURNS_AUTO(m_state_proxy.get())
    auto u() const RETURNS_AUTO(m_control_proxy.get())
    auto x() RETURNS_AUTO(m_state_proxy.get())
    auto u() RETURNS_AUTO(m_control_proxy.get())

    auto x_next() -> decltype(m_state_proxy.get()) {
      auto next_idx_proxy =
          *(++m_state_proxy.m_inner_proxy.to_forward_iterator());
      auto next_matseq_proxy = x_proxy_t{next_idx_proxy, m_state_proxy.m_data};
      return next_matseq_proxy.get();
    }

    auto time_idx() const -> isize { return m_state_proxy.time_idx(); }

    auto as_const() const -> proxy_t<true> {
      return {m_state_proxy.as_const(), m_control_proxy.as_const()};
    }
  };

  template <bool IsConst> struct iterator_impl_t {
    typename x_mat_seq_t::template iterator_impl_t<IsConst> x_iter;
    typename u_mat_seq_t::template iterator_impl_t<IsConst> u_iter;

    using proxy_t = trajectory_t::proxy_t<IsConst>;

    using difference_type = std::ptrdiff_t;
    using value_type = void;
    using pointer = proxy_t*;
    using reference = proxy_t;
    using iterator_category = std::bidirectional_iterator_tag;

    auto operator==(iterator_impl_t other) const -> bool {
      return (x_iter == other.x_iter and u_iter == other.u_iter);
    }
    auto operator!=(iterator_impl_t other) const -> bool {
      return not(*this == other);
    }
    auto operator++() -> iterator_impl_t& {
      ++x_iter;
      ++u_iter;
      return *this;
    }
    auto operator--() -> iterator_impl_t& {
      --x_iter;
      --u_iter;
      return *this;
    }
    DDP_DEFINE_POSTFIX(iterator_impl_t);
    auto operator*() const -> proxy_t { return {*x_iter, *u_iter}; }
    auto operator*() -> proxy_t { return {*x_iter, *u_iter}; }
  };

  using iterator = iterator_impl_t<false>;
  using const_iterator = iterator_impl_t<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() -> iterator {
    return {m_state_data.begin(), m_control_data.begin()};
  }
  auto end() -> iterator {
    return {--(m_state_data.end()), m_control_data.end()};
  }
  auto rbegin() -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() -> reverse_iterator { return reverse_iterator{begin()}; }

  auto begin() const -> const_iterator {
    return {m_state_data.begin(), m_control_data.begin()};
  }
  auto end() const -> const_iterator {
    return {--(m_state_data.end()), m_control_data.end()};
  }
  auto rbegin() const -> const_reverse_iterator {
    return const_reverse_iterator{end()};
  }
  auto rend() const -> const_reverse_iterator {
    return const_reverse_iterator{begin()};
  }

  auto x_0() RETURNS_AUTO((*begin()).x())
  auto x_0() const RETURNS_AUTO((*begin()).x())

  auto x_f() RETURNS_AUTO((*end()).x())
  auto x_f() const RETURNS_AUTO((*end()).x())
};
} // namespace ddp

#endif /* end of include guard: INCLUDE_TRAJ_HPP_W8EAKEL6 */
