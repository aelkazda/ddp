#ifndef MULTI_SHOOTING_HPP_KMGQYPUX
#define MULTI_SHOOTING_HPP_KMGQYPUX

#include "indexers.hpp"
#include "ddp.hpp"

namespace ddp {

template <
    typename Scalar,
    typename X_Idx,
    typename U_Idx,
    typename Eq_Idx,
    typename Ineq_Idx,
    typename Transition,
    typename Cost,
    typename Cost_Final,
    typename Eq_Constraint,
    typename Ineq_Constraint>
struct multi_shooting_t {
  using scalar_t = Scalar;

  using state_indexer_t = X_Idx;
  using control_indexer_t = detail::row_concat_indexer_t< //
      U_Idx,                                              //
      detail::periodic_indexer_t<state_indexer_t>         //
      >;
  using eq_indexer_t = detail::row_concat_indexer_t< //
      Eq_Idx,                                        //
      detail::periodic_indexer_t<state_indexer_t>    //
      >;
  using ineq_indexer_t = Ineq_Idx;

  using ss_transition_t = Transition;
  using ss_cost_t = Cost;
  using ss_cost_final_t = Cost_Final;
  using ss_eq_constraint_t = Eq_Constraint;
  using ss_ineq_constraint_t = Ineq_Constraint;

  using isize = gu::isize;
  template <isize N>
  using fixed_or_dyn = gu::conditional_t<N >= 0, gu::fix<N>, gu::dyn>;
  template <isize InDimL, isize InDimR, isize OutDim>
  using tensor_t = np::tensor<
      scalar_t,
      np::tensor_shape_t<
          fixed_or_dyn<InDimL>,
          fixed_or_dyn<InDimR>,
          fixed_or_dyn<OutDim> >,
      np::tags::detail::on_heap_t>;
  template <isize rows_t, isize cols_t>
  using matrix_t = np::mat<scalar_t, rows_t, cols_t>;
  template <isize Size> using vector_t = np::mat<scalar_t, Size, 1>;

  static constexpr isize x_dim_static = state_indexer_t::rows_t::value;

  struct transition_t {
    ss_transition_t m_fn_seq;
    using inner_functor_t = decltype(m_fn_seq(isize{}));

    using out_dim_t = decltype(m_fn_seq(isize{}).out_dim());
    static constexpr isize out_dim_static = out_dim_t::value;

    using u_dim_t = gu::dyn;
    static constexpr isize u_dim_static = u_dim_t::value;

    struct derivs_2nd_t {
      vector_t<out_dim_static> val;
      matrix_t<out_dim_static, x_dim_static> x;
      matrix_t<out_dim_static, u_dim_static> u;
      tensor_t<x_dim_static, x_dim_static, out_dim_static> xx;
      tensor_t<u_dim_static, x_dim_static, out_dim_static> ux;
      tensor_t<u_dim_static, u_dim_static, out_dim_static> uu;
    };

    struct functor_t {
      inner_functor_t m_functor_ss;

      template <typename InX, typename InU>
      auto operator()(InX const& in_x, InU const& in_u) const
          -> vector_t<out_dim_static> {

        auto nu_ss = m_functor_ss.u_dim();
        bool ms_node = (in_u.rows().as_usize() != nu_ss.as_usize());

        auto value = m_functor_ss(
            in_x,                     //
            np::top_rows(in_u, nu_ss) //
        );

        if (ms_node) {
          value += np::bot_rows(in_u, in_x.rows());
        }
        return value;
      }

      template <typename InX, typename InU>
      auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {
        auto nx = in_x.rows();
        auto nu = in_u.rows();
        auto nu_ss = m_functor_ss.u_dim();

        bool ms_node = (in_u.rows().as_usize() != nu_ss.as_usize());

        auto ss_derivs_2 = m_functor_ss.derivs_2( //
            in_x,                                 //
            np::top_rows(in_u, nu_ss)             //
        );

        using uninit = np::uninitialized<scalar_t>;

        auto retval = derivs_2nd_t{
            uninit::col(nx),                            //
            uninit::mat(nx, nx),                        //
            uninit::mat(nx, nu),                        //
            uninit::ten(nx, nx, nx, np::tags::on_heap), //
            uninit::ten(nu, nx, nx, np::tags::on_heap), //
            uninit::ten(nu, nu, nx, np::tags::on_heap), //
        };
        retval.ux = np::zero_v;
        retval.uu = np::zero_v;

        retval.val = ss_derivs_2.val;
        retval.x = ss_derivs_2.x;
        np::left_cols(retval.u, nu_ss) = ss_derivs_2.u;
        retval.xx = ss_derivs_2.xx;

        for (isize i = 0; i < in_x.rows().as_isize(); ++i) {
          np::top_rows(retval.ux.row(i), nu_ss) = ss_derivs_2.ux.row(i);
          np::top_left_block(retval.uu.row(i), nu_ss, nu_ss) =
              ss_derivs_2.uu.row(i);
        }

        if (ms_node) {
          retval.val += np::bot_rows(in_u, nx);
          np::right_cols(retval.u, nx).eigen().setIdentity();
        }
        return retval;
      }
    };

    auto operator()(isize t) const -> functor_t { return {m_fn_seq(t)}; }
  };

  using cost_final_t = ss_cost_final_t;
  struct cost_t {
    ss_cost_t m_fn_seq;

    using inner_functor_t = decltype(m_fn_seq(isize{}));

    using out_dim_t = decltype(m_fn_seq(isize{}).out_dim());
    static constexpr isize out_dim_static = out_dim_t::value;

    using u_dim_t = gu::dyn;
    static constexpr isize u_dim_static = u_dim_t::value;

    struct derivs_2nd_t {
      matrix_t<1, 1> val;
      matrix_t<1, x_dim_static> x;
      matrix_t<1, u_dim_static> u;
      matrix_t<x_dim_static, x_dim_static> xx;
      matrix_t<u_dim_static, x_dim_static> ux;
      matrix_t<u_dim_static, u_dim_static> uu;
    };

    struct functor_t {
      inner_functor_t m_functor_ss;

      template <typename InX, typename InU>
      auto operator()(InX const& in_x, InU const& in_u) const
          -> vector_t<out_dim_static> {

        auto nu_ss = m_functor_ss.u_dim();

        return np::eval(m_functor_ss(
            in_x,                     //
            np::top_rows(in_u, nu_ss) //
            ));
      }

      template <typename InX, typename InU>
      auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {
        auto nx = in_x.rows();
        auto nu = in_u.rows();
        auto nu_ss = m_functor_ss.u_dim();

        auto ss_derivs_2 = m_functor_ss.derivs_2( //
            in_x,                                 //
            np::top_rows(in_u, nu_ss)             //
        );

        using uninit = np::uninitialized<scalar_t>;

        auto retval = derivs_2nd_t{
            uninit::col(np::fix<1>{}), //
            uninit::row(nx),           //
            uninit::row(nu),           //
            uninit::mat(nx, nx),       //
            uninit::mat(nu, nx),       //
            uninit::mat(nu, nu),       //
        };
        retval.u = np::zero_v;
        retval.ux = np::zero_v;
        retval.uu = np::zero_v;

        retval.val = ss_derivs_2.val;
        retval.x = ss_derivs_2.x;
        np::left_cols(retval.u, nu_ss) = ss_derivs_2.u;
        retval.xx = ss_derivs_2.xx;

        np::top_rows(retval.ux, nu_ss) = ss_derivs_2.ux;
        np::top_left_block(retval.uu, nu_ss, nu_ss) = ss_derivs_2.uu;
        return retval;
      }
    };

    auto operator()(isize t) const -> functor_t { return {m_fn_seq(t)}; }
  };

  struct ineq_constraint_t {
    ss_ineq_constraint_t m_fn_seq;

    using inner_functor_t = decltype(m_fn_seq(isize{}));

    using out_dim_t = decltype(m_fn_seq(isize{}).out_dim());
    static constexpr isize out_dim_static = out_dim_t::value;

    using u_dim_t = gu::dyn;
    static constexpr isize u_dim_static = u_dim_t::value;

    struct derivs_2nd_t {
      vector_t<out_dim_static> val;
      matrix_t<out_dim_static, x_dim_static> x;
      matrix_t<out_dim_static, u_dim_static> u;
      tensor_t<x_dim_static, x_dim_static, out_dim_static> xx;
      tensor_t<u_dim_static, x_dim_static, out_dim_static> ux;
      tensor_t<u_dim_static, u_dim_static, out_dim_static> uu;
    };

    struct functor_t {
      inner_functor_t m_functor_ss;

      template <typename InX, typename InU>
      auto operator()(InX const& in_x, InU const& in_u) const
          -> vector_t<out_dim_static> {

        auto nu_ss = m_functor_ss.u_dim();

        return np::eval(m_functor_ss(
            in_x,                     //
            np::top_rows(in_u, nu_ss) //
            ));
      }

      template <typename InX, typename InU>
      auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {
        auto nx = in_x.rows();
        auto nu = in_u.rows();
        auto nu_ss = m_functor_ss.u_dim();
        auto ni = m_functor_ss.out_dim();

        auto ss_derivs_2 = m_functor_ss.derivs_2( //
            in_x,                                 //
            np::top_rows(in_u, nu_ss)             //
        );

        using uninit = np::uninitialized<scalar_t>;

        auto retval = derivs_2nd_t{
            uninit::col(ni),                            //
            uninit::mat(ni, nx),                        //
            uninit::mat(ni, nu),                        //
            uninit::ten(nx, nx, ni, np::tags::on_heap), //
            uninit::ten(nu, nx, ni, np::tags::on_heap), //
            uninit::ten(nu, nu, ni, np::tags::on_heap), //
        };
        retval.u = np::zero_v;
        retval.ux = np::zero_v;
        retval.uu = np::zero_v;

        retval.val = ss_derivs_2.val;
        retval.x = ss_derivs_2.x;
        np::left_cols(retval.u, nu_ss) = ss_derivs_2.u;
        retval.xx = ss_derivs_2.xx;

        for (isize i = 0; i < ni.as_isize(); ++i) {
          np::top_rows(retval.ux.row(i), nu_ss) = ss_derivs_2.ux.row(i);
          np::top_left_block(retval.uu.row(i), nu_ss, nu_ss) =
              ss_derivs_2.uu.row(i);
        }
        return retval;
      }
    };

    auto operator()(isize t) const -> functor_t { return {m_fn_seq(t)}; }
  };

  struct eq_constraint_t {
    ss_eq_constraint_t m_fn_seq;

    using inner_functor_t = decltype(m_fn_seq(isize{}));

    using out_dim_t = gu::dyn;
    static constexpr isize out_dim_static = out_dim_t::value;

    using u_dim_t = gu::dyn;
    static constexpr isize u_dim_static = u_dim_t::value;

    struct derivs_2nd_t {
      vector_t<out_dim_static> val;
      matrix_t<out_dim_static, x_dim_static> x;
      matrix_t<out_dim_static, u_dim_static> u;
      tensor_t<x_dim_static, x_dim_static, out_dim_static> xx;
      tensor_t<u_dim_static, x_dim_static, out_dim_static> ux;
      tensor_t<u_dim_static, u_dim_static, out_dim_static> uu;
    };

    struct functor_t {
      inner_functor_t m_functor_ss;

      template <typename InX, typename InU>
      auto operator()(InX const& in_x, InU const& in_u) const
          -> vector_t<out_dim_static> {

        auto nu_ss = m_functor_ss.u_dim();
        bool ms_node = (in_u.rows().as_usize() != nu_ss.as_usize());

        auto ne_ss = m_functor_ss.out_dim();
        auto nx = in_x.rows();
        auto ne = ne_ss + (ms_node ? gu::dyn(nx) : gu::dyn{0});

        auto retval = np::uninitialized<scalar_t>::col(ne);

        np::top_rows(retval, ne_ss) = m_functor_ss(
            in_x,                     //
            np::top_rows(in_u, nu_ss) //
        );

        if (ms_node) {
          np::bot_rows(retval, nx) = np::bot_rows(in_u, nx);
        }

        return retval;
      }

      template <typename InX, typename InU>
      auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {

        auto nu_ss = m_functor_ss.u_dim();
        bool ms_node = (in_u.rows().as_usize() != nu_ss.as_usize());

        auto ne_ss = m_functor_ss.out_dim();
        auto nx = in_x.rows();
        auto nu = in_u.rows();
        auto ne = ne_ss + (ms_node ? gu::dyn(nx) : gu::dyn{0});

        auto ss_derivs_2 = m_functor_ss.derivs_2( //
            in_x,                                 //
            np::top_rows(in_u, nu_ss)             //
        );

        using uninit = np::uninitialized<scalar_t>;

        auto retval = derivs_2nd_t{
            uninit::col(ne),                            //
            uninit::mat(ne, nx),                        //
            uninit::mat(ne, nu),                        //
            uninit::ten(nx, nx, ne, np::tags::on_heap), //
            uninit::ten(nu, nx, ne, np::tags::on_heap), //
            uninit::ten(nu, nu, ne, np::tags::on_heap), //
        };
        retval.u = np::zero_v;
        retval.x = np::zero_v;
        retval.ux = np::zero_v;
        retval.uu = np::zero_v;
        retval.xx = np::zero_v;

        np::top_rows(retval.val, ne_ss) = ss_derivs_2.val;
        np::top_rows(retval.x, ne_ss) = ss_derivs_2.x;

        for (isize i = 0; i < ne_ss.as_isize(); ++i) {
          retval.xx.row(i) = ss_derivs_2.xx.row(i);

          np::top_rows(retval.ux.row(i), nu_ss) = ss_derivs_2.ux.row(i);
          np::top_left_block(retval.uu.row(i), nu_ss, nu_ss) =
              ss_derivs_2.uu.row(i);
        }

        np::top_left_block(retval.u, ne_ss, nu_ss) = ss_derivs_2.u;

        if (ms_node) {
          np::bot_rows(retval.val, nx) = np::bot_rows(in_u, nx);
          np::bot_right_block(retval.u, nx, nx).eigen().setIdentity();
        }
        return retval;
      }
    };

    auto operator()(isize t) const -> functor_t { return {m_fn_seq(t)}; }
  };
};

template <
    typename Vec_X,
    typename X_Idx,
    typename U_Idx,
    typename Eq_Idx,
    typename Ineq_Idx,
    typename Transition,
    typename Cost,
    typename Cost_Final,
    typename Eq_Constraint,
    typename Ineq_Constraint,
    typename MS = multi_shooting_t<
        typename Vec_X::scalar_t,
        X_Idx,
        U_Idx,
        Eq_Idx,
        Ineq_Idx,
        Transition,
        Cost,
        Cost_Final,
        Eq_Constraint,
        Ineq_Constraint> >
auto multi_shooting_problem(
    isize period,
    Vec_X x_init,
    X_Idx x_idx,
    U_Idx u_idx,
    Eq_Idx eq_idx,
    Ineq_Idx ineq_idx,
    Transition f,
    Cost l,
    Cost_Final l_f,
    Eq_Constraint eq,
    Ineq_Constraint ineq)
    -> ddp_solver_t<
        typename MS::scalar_t,
        typename MS::state_indexer_t,
        typename MS::control_indexer_t,
        typename MS::eq_indexer_t,
        typename MS::ineq_indexer_t,
        typename MS::transition_t,
        typename MS::cost_t,
        typename MS::cost_final_t,
        typename MS::eq_constraint_t,
        typename MS::ineq_constraint_t> {

  auto x_trunc = x_idx;
  --(x_trunc.n_steps);

  return {
      std::move(x_init),
      X_Idx{x_idx},

      row_concat(std::move(u_idx), periodic_indexer(X_Idx{x_trunc}, period, 0)),
      row_concat(
          std::move(eq_idx), periodic_indexer(X_Idx{x_trunc}, period, 0)),

      std::move(ineq_idx),
      typename MS::transition_t{std::move(f)},
      typename MS::cost_t{std::move(l)},
      typename MS::cost_final_t{std::move(l_f)},
      typename MS::eq_constraint_t{std::move(eq)},
      typename MS::ineq_constraint_t{std::move(ineq)},
      x_trunc};
}

} // namespace ddp

#endif /* end of include guard MULTI_SHOOTING_HPP_KMGQYPUX */
