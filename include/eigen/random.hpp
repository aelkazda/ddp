#ifndef RANDOM_HPP_RAUGKK8B
#define RANDOM_HPP_RAUGKK8B

#include "eigen/matrix.hpp"

namespace np {
template <typename Scalar> struct random {
  using uninitialized = np::uninitialized<Scalar>;
  template <typename RowType, typename ColType, typename... Options>
  static auto mat(RowType n, ColType m, Options... opts)
      -> decltype(uninitialized::mat(n, m, opts...)) {
    auto out = uninitialized::mat(n, m, opts...);
    out.eigen().setRandom();
    return out;
  }

  template <typename SizeType, typename... Options>
  static auto pos_def(SizeType n, Options... opts)
      -> decltype(uninitialized::mat(n, n, opts...)) {
    auto out = uninitialized::mat(n, n, opts...);
    out.eigen().setRandom();
    out = out * trans(out);
    return out;
  }

  template <typename Rows, typename... Options>
  static auto col(Rows rows, Options... opts)
      RETURNS_AUTO(mat(rows, fix<1>{}, tags::col_major, opts...))

  template <typename cols_t, typename... Options>
  static auto row(cols_t cols, Options... opts)
      RETURNS_AUTO(mat(fix<1>{}, cols, tags::row_major, opts...))
};
} // namespace np

#endif /* end of include guard: RANDOM_HPP_RAUGKK8B */
