#ifndef INCLUDE_EIGEN_MATRIX_BASE_HPP_SF8C3OZL
#define INCLUDE_EIGEN_MATRIX_BASE_HPP_SF8C3OZL

#include <type_traits>

#include "Eigen/Dense"

#include "general_utilities.hpp"
#include "storage.hpp"
#include "eigen/zero.hpp"

namespace np {
template <
    typename Scalar,
    isize Rows,
    isize Cols,
    usize Options =
        (Rows == 1 and Cols != 1)
            ? options_to_u64<storage_config<tags::detail::row_major_t> >()
            : options_to_u64<storage_config<tags::detail::col_major_t> >()>
struct mat;

template <typename Scalar>
using dyn_mat_t = mat<Scalar, dyn::value, dyn::value>;

template <typename Scalar> //
using dyn_vec_t = mat<Scalar, dyn::value, 1>;

namespace detail {
template <typename T> struct matrix_base;
} // namespace detail

namespace concepts {
template <typename T> struct owning_matrix : gu::false_type {};
template <typename Scalar, isize Rows, isize Cols, usize Options>
struct owning_matrix<mat<Scalar, Rows, Cols, Options> > : gu::true_type {};

template <typename T>
struct matrix : std::is_base_of<detail::matrix_base<T>, T> {};

template <typename U, typename V, typename Enable = void>
struct matrix_same_type : std::false_type {};
template <typename U, typename V>
struct matrix_same_type<
    U,
    V,
    SPECIALIZE_IF(
        std::is_same<typename U::scalar_t, typename V::scalar_t>::value)>
    : std::true_type {};

template <typename U, typename V, typename Enable = void>
struct matrix_maybe_same_rows : std::false_type {};
template <typename U, typename V>
struct matrix_maybe_same_rows<
    U,
    V,
    SPECIALIZE_IF(
        (matrix<U>::value and      //
         matrix<V>::value and      //
         gu::concepts::maybe_same< //
             typename U::rows_t,   //
             typename V::rows_t    //
             >::value              //
         ))> : std::true_type {};

template <typename U, typename V, typename Enable = void>
struct matrix_maybe_same_cols : std::false_type {};
template <typename U, typename V>
struct matrix_maybe_same_cols<
    U,
    V,
    SPECIALIZE_IF(
        (matrix<U>::value and      //
         matrix<V>::value and      //
         gu::concepts::maybe_same< //
             typename U::cols_t,   //
             typename V::cols_t    //
             >::value              //
         ))> : std::true_type {};

template <typename U, typename V>
using matrix_maybe_same_shape = gu::and_c<
    matrix_maybe_same_rows<U, V>, //
    matrix_maybe_same_cols<U, V>  //
    >;
} // namespace concepts

template <typename T>
auto fill_constant(T&& mat, zero_t)
    -> gu::enable_if_t<concepts::matrix<gu::remove_cvref_t<T> >::value> {
  static_assert(
      not std::is_const<gu::deref_t<T> >::value,
      "first argument is not mutable.");
  mat.eigen().setConstant(0);
}

// 1x1 matrix
template <typename T>
auto as_eigen_ref(T&& mat) //
    -> gu::enable_if_t<
        (concepts::matrix<gu::remove_cvref_t<T> >::value and
         gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::cols_t>::
             value and
         gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::rows_t>::
             value),

        Eigen::Ref<                                    //
            gu::const_if_t<                            //
                std::is_const<gu::deref_t<T> >::value, //
                Eigen::Matrix<                         //
                    typename gu::decay_t<T>::scalar_t, //
                    1,                                 //
                    1                                  //
                    >                                  //
                >                                      //
            >                                          //
        >                                              //
{
  return mat.eigen();
}

// row vector
template <typename T>
auto as_eigen_ref(T&& mat) //
    -> gu::enable_if_t<
        (concepts::matrix<gu::remove_cvref_t<T> >::value and
         not gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::cols_t>::
             value and
         gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::rows_t>::
             value),

        Eigen::Ref<                                    //
            gu::const_if_t<                            //
                std::is_const<gu::deref_t<T> >::value, //
                Eigen::Matrix<                         //
                    typename gu::decay_t<T>::scalar_t, //
                    1,                                 //
                    Eigen::Dynamic                     //
                    >                                  //
                >                                      //
            >                                          //
        >                                              //
{
  return mat.eigen();
}

// column vector
template <typename T>
auto as_eigen_ref(T&& mat) //
    -> gu::enable_if_t<
        (concepts::matrix<gu::remove_cvref_t<T> >::value and
         gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::cols_t>::
             value and
         not gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::rows_t>::
             value),

        Eigen::Ref<                                    //
            gu::const_if_t<                            //
                std::is_const<gu::deref_t<T> >::value, //
                Eigen::Matrix<                         //
                    typename gu::decay_t<T>::scalar_t, //
                    Eigen::Dynamic,                    //
                    1                                  //
                    >                                  //
                >                                      //
            >                                          //
        >                                              //
{
  return mat.eigen();
}

// matrix
template <typename T>
auto as_eigen_ref(T&& mat) //
    -> gu::enable_if_t<
        (concepts::matrix<gu::remove_cvref_t<T> >::value and
         not gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::cols_t>::
             value and
         not gu::concepts::equal<gu::fix<1>, typename gu::deref_t<T>::rows_t>::
             value),

        Eigen::Ref<                                    //
            gu::const_if_t<                            //
                std::is_const<gu::deref_t<T> >::value, //
                Eigen::Matrix<                         //
                    typename gu::decay_t<T>::scalar_t, //
                    Eigen::Dynamic,                    //
                    Eigen::Dynamic                     //
                    >                                  //
                >                                      //
            >                                          //
        >                                              //
{
  return mat.eigen();
}

template <typename T>
auto as_eigen_cref(T const& mat) RETURNS_AUTO(as_eigen_ref(mat))

template <typename T>
auto is_zero_sized(T const& mat)
    -> gu::enable_if_t<concepts::matrix<T>::value, bool> {
  return mat.rows().as_isize() == 0 or mat.cols().as_isize() == 0;
}

template <typename T, REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto trans(T&& mat) -> detail::trans_op<T>;

namespace detail {
template <typename D> struct matrix_base {
protected:
  ~matrix_base() = default;
  matrix_base(matrix_base const&) = default;
  matrix_base(matrix_base&&) noexcept = default;
  auto operator=(matrix_base const& other) -> matrix_base& = default;
  auto operator=(matrix_base&& other) noexcept -> matrix_base& = default;

public:
  matrix_base() = default;
#define TPL template <typename TT = gu::remove_cvref_t<D> >

  TPL using owning_matrix = gu::conditional_t<
      concepts::owning_matrix<TT>::value,
      TT,
      mat< //
          typename TT::scalar_t,
          TT::rows_t::value,
          TT::cols_t::value //
          > >;

  auto derived() & noexcept -> gu::ref_t<D> {
    return static_cast<gu::ref_t<D> >(*this);
  }
  auto derived() const & noexcept -> gu::cref_t<D> {
    return static_cast<gu::cref_t<D> >(*this);
  }
  auto derived() && noexcept -> gu::rv_ref_t<D> {
    return static_cast<gu::rv_ref_t<D> >(*this);
  }
  auto derived() const && noexcept -> gu::rv_cref_t<D> {
    return static_cast<gu::rv_cref_t<D> >(*this);
  }

  TPL auto rows() const noexcept -> typename TT::rows_t {
    return derived().rows();
  }
  TPL auto cols() const noexcept -> typename TT::cols_t {
    return derived().cols();
  }
  TPL auto eigen() noexcept -> typename TT::eigen_expr_t {
    return derived().eigen();
  }

  TPL auto eigen() const noexcept -> typename TT::eigen_const_expr_t {
    return derived().eigen();
  }

  TPL auto clone() const noexcept(false /* FIXME */) -> owning_matrix<TT> {
    owning_matrix<TT> out{rows(), cols()};
    out.eigen() = eigen();
    return out;
  }

  TPL auto T() & -> detail::trans_op<TT&> { return trans(derived()); }
  TPL auto T() const& -> detail::trans_op<TT const&> {
    return trans(derived());
  }
  TPL auto T() && -> detail::trans_op<TT> {
    return trans(std::move(derived()));
  }
  TPL auto T() const&& -> detail::trans_op<TT const> {
    return trans(std::move(derived()));
  }
#undef TPL
};

namespace _ {
template <typename T> auto cref() -> gu::cref_t<T> {
  return std::declval<gu::remove_cvref_t<T> >();
};
} // namespace _
template <typename L, typename R> struct add_op : matrix_base<add_op<L, R> > {
  using scalar_t = typename gu::remove_cvref_t<L>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<L>::rows_t;
  using cols_t = typename gu::remove_cvref_t<L>::cols_t;
  using eigen_expr_t = decltype(_::cref<L>().eigen() + _::cref<R>().eigen());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  add_op(gu::ref_if_lvalue<L>&& l, gu::ref_if_lvalue<R>&& r)
      : left(std::move(l)), right(std::move(r)){};

  gu::ref_if_lvalue<L> left;
  gu::ref_if_lvalue<R> right;

  auto rows() const noexcept -> rows_t { return left.ref().rows(); }
  auto cols() const noexcept -> cols_t { return left.ref().cols(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return left.cref().eigen() + right.cref().eigen();
  }
};
template <typename L, typename R> struct sub_op : matrix_base<sub_op<L, R> > {
  using scalar_t = typename gu::remove_cvref_t<L>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<L>::rows_t;
  using cols_t = typename gu::remove_cvref_t<L>::cols_t;
  using eigen_expr_t = decltype(_::cref<L>().eigen() - _::cref<R>().eigen());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  sub_op(gu::ref_if_lvalue<L>&& l, gu::ref_if_lvalue<R>&& r)
      : left(std::move(l)), right(std::move(r)){};

  gu::ref_if_lvalue<L> left;
  gu::ref_if_lvalue<R> right;

  auto rows() const noexcept -> rows_t { return left.ref().rows(); }
  auto cols() const noexcept -> cols_t { return left.ref().cols(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return left.ref().eigen() - right.ref().eigen();
  }
};
template <typename L, typename R>
struct mat_mul_op : matrix_base<mat_mul_op<L, R> > {
  using scalar_t = typename gu::remove_cvref_t<L>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<L>::rows_t;
  using cols_t = typename gu::remove_cvref_t<R>::cols_t;
  using eigen_expr_t = decltype(_::cref<L>().eigen() * _::cref<R>().eigen());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  mat_mul_op(gu::ref_if_lvalue<L>&& l, gu::ref_if_lvalue<R>&& r)
      : left(std::move(l)), right(std::move(r)){};

  gu::ref_if_lvalue<L> left;
  gu::ref_if_lvalue<R> right;

  auto rows() const noexcept -> rows_t { return left.ref().rows(); }
  auto cols() const noexcept -> cols_t { return right.ref().cols(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return left.ref().eigen() * right.ref().eigen();
  }
};

template <typename Scalar, typename Matrix>
struct scal_mul_op<Scalar, Matrix, true>
    : matrix_base<scal_mul_op<Scalar, Matrix, true> > {
  using scalar_t = typename gu::remove_cvref_t<Matrix>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<Matrix>::rows_t;
  using cols_t = typename gu::remove_cvref_t<Matrix>::cols_t;
  using eigen_expr_t = decltype(_::cref<Scalar>() * _::cref<Matrix>().eigen());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  scal_mul_op(gu::ref_if_lvalue<Scalar>&& s, gu::ref_if_lvalue<Matrix>&& m)
      : scal(std::move(s)), mat(std::move(m)){};

  gu::ref_if_lvalue<Scalar> scal;
  gu::ref_if_lvalue<Matrix> mat;
  auto rows() const noexcept -> rows_t { return mat.ref().rows(); }
  auto cols() const noexcept -> cols_t { return mat.ref().cols(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return scal.ref() * mat.ref().eigen();
  }
};

template <typename Scalar, typename Matrix>
struct scal_mul_op<Scalar, Matrix, false>
    : matrix_base<scal_mul_op<Scalar, Matrix, true> > {
  using scalar_t = typename gu::remove_cvref_t<Matrix>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<Matrix>::rows_t;
  using cols_t = typename gu::remove_cvref_t<Matrix>::cols_t;
  using eigen_expr_t = decltype(_::cref<Matrix>().eigen() * _::cref<Scalar>());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  scal_mul_op(gu::ref_if_lvalue<Scalar>&& s, gu::ref_if_lvalue<Matrix>&& m)
      : scal(std::move(s)), mat(std::move(m)){};

  gu::ref_if_lvalue<Scalar> scal;
  gu::ref_if_lvalue<Matrix> mat;

  auto rows() const noexcept -> rows_t { return mat.ref().rows(); }
  auto cols() const noexcept -> cols_t { return mat.ref().cols(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return mat.ref().eigen() * scal.ref();
  }
};

template <typename Matrix, typename Scalar>
struct scal_div_op : matrix_base<scal_div_op<Matrix, Scalar> > {
  using scalar_t = typename gu::remove_cvref_t<Matrix>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<Matrix>::rows_t;
  using cols_t = typename gu::remove_cvref_t<Matrix>::cols_t;
  using eigen_expr_t = decltype(_::cref<Matrix>().eigen() / _::cref<Scalar>());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  scal_div_op(gu::ref_if_lvalue<Scalar>&& s, gu::ref_if_lvalue<Matrix>&& m)
      : scal(std::move(s)), mat(std::move(m)){};

  gu::ref_if_lvalue<Scalar> scal;
  gu::ref_if_lvalue<Matrix> mat;

  auto rows() const noexcept -> rows_t { return mat.ref().rows(); }
  auto cols() const noexcept -> cols_t { return mat.ref().cols(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return mat.ref().eigen() / scal.ref();
  }
};

template <typename T> struct no_alias_t : matrix_base<no_alias_t<T> > {
  static_assert(
      gu::remove_cvref_t<T>::is_writable,
      "only writable expressions can be marked 'no_alias'.");

  using scalar_t = typename gu::remove_cvref_t<T>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<T>::rows_t;
  using cols_t = typename gu::remove_cvref_t<T>::cols_t;
  using eigen_expr_t = decltype(std::declval<gu::deref_t<T>&>().eigen());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = true;

  explicit no_alias_t(gu::ref_if_lvalue<T>&& assignable_expr)
      : m_assignable_expr(assignable_expr){};

  gu::ref_if_lvalue<T> m_assignable_expr;

  auto rows() const noexcept -> rows_t {
    return m_assignable_expr.ref().rows();
  }
  auto cols() const noexcept -> cols_t {
    return m_assignable_expr.ref().cols();
  }
  auto eigen() noexcept -> eigen_expr_t {
    return m_assignable_expr.ref().eigen();
  }
  template <typename O, REQUIRES(is_writable and concepts::matrix<O>::value)>

  auto operator=(O const& o) -> no_alias_t& {
    gu::assert_eq(rows(), o.rows());
    gu::assert_eq(cols(), o.cols());
    if (not is_zero_sized(*this)) {
      eigen().noalias() = o.eigen();
    }
    return *this;
  }
  auto operator=(zero_t) -> no_alias_t& {
    fill_constant(*this, zero_v);
    return *this;
  }
};

template <typename T> struct trans_op : matrix_base<trans_op<T> > {
  using scalar_t = typename gu::remove_cvref_t<T>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<T>::cols_t;
  using cols_t = typename gu::remove_cvref_t<T>::rows_t;
  using eigen_expr_t = decltype(_::cref<T>().eigen().transpose());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  explicit trans_op(gu::ref_if_lvalue<T>&& expr) : m_expr(expr){};

  gu::ref_if_lvalue<T> m_expr;

  auto rows() const noexcept -> rows_t { return m_expr.ref().cols(); }
  auto cols() const noexcept -> cols_t { return m_expr.ref().rows(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return m_expr.ref().eigen().transpose();
  }
};

template <typename T> struct neg_op : matrix_base<neg_op<T> > {
  using scalar_t = typename gu::remove_cvref_t<T>::scalar_t;
  using rows_t = typename gu::remove_cvref_t<T>::rows_t;
  using cols_t = typename gu::remove_cvref_t<T>::cols_t;
  using eigen_expr_t = decltype(-_::cref<T>().eigen());
  using eigen_const_expr_t = eigen_expr_t;
  constexpr static bool is_writable = false;

  explicit neg_op(gu::ref_if_lvalue<T>&& expr) : m_expr(expr){};
  gu::ref_if_lvalue<T> m_expr;

  auto rows() const noexcept -> rows_t { return m_expr.ref().rows(); }
  auto cols() const noexcept -> cols_t { return m_expr.ref().cols(); }
  auto eigen() const noexcept -> eigen_expr_t {
    return -(m_expr.ref().eigen());
  }
};

template <template <typename> class Wrapper> struct wrap {
  template <typename T> using type = Wrapper<T>;
};

template <
    typename T,
    typename StartRow,
    typename StartCol,
    typename NumRows,
    typename NumCols>
struct block_view_op
    : matrix_base<block_view_op<T, StartRow, StartCol, NumRows, NumCols> > {
  using scalar_t = typename gu::remove_cvref_t<T>::scalar_t;
  using rows_t = NumRows;
  using cols_t = NumCols;
  using eigen_expr_t =
      decltype(std::declval<gu::ref_if_lvalue<T> >()
                   .ref()
                   .eigen()
                   .template block<NumRows::value, NumCols::value>(
                       int{}, int{}, int{}, int{}));
  using eigen_const_expr_t =
      decltype(std::declval<gu::ref_if_lvalue<T> >()
                   .cref()
                   .eigen()
                   .template block<NumRows::value, NumCols::value>(
                       int{}, int{}, int{}, int{}));
  constexpr static bool is_writable = gu::remove_cvref_t<T>::is_writable;

  block_view_op(
      gu::ref_if_lvalue<T>&& expr,
      StartRow start_row_,
      StartCol start_col_,
      NumRows nrows_,
      NumCols ncols_)
      : m_expr(expr),
        m_start_row(start_row_),
        m_start_col(start_col_),
        m_num_rows(nrows_),
        m_num_cols(ncols_) {}

  gu::ref_if_lvalue<T> m_expr;
  StartRow m_start_row;
  StartCol m_start_col;
  NumRows m_num_rows;
  NumCols m_num_cols;

  template <typename O, REQUIRES(is_writable and concepts::matrix<O>::value)>
  auto operator=(O const& o) -> block_view_op& {
    gu::assert_eq(rows(), o.rows());
    gu::assert_eq(cols(), o.cols());
    if (not is_zero_sized(*this)) {
      eigen() = o.eigen();
    }
    return *this;
  }

  template <REQUIRES(is_writable)> auto operator=(zero_t) -> block_view_op& {
    fill_constant(*this, zero_v);
    return *this;
  }

  auto rows() const noexcept -> rows_t { return m_num_rows; }
  auto cols() const noexcept -> cols_t { return m_num_cols; }
  auto eigen() noexcept -> eigen_expr_t {
    return m_expr.ref().eigen().template block<NumRows::value, NumCols::value>(
        m_start_row.as_isize(),
        m_start_col.as_isize(),
        m_num_rows.as_isize(),
        m_num_cols.as_isize());
  }

  auto eigen() const noexcept -> eigen_const_expr_t {
    return m_expr.ref().eigen().template block<NumRows::value, NumCols::value>(
        m_start_row.as_isize(),
        m_start_col.as_isize(),
        m_num_rows.as_isize(),
        m_num_cols.as_isize());
  }
};
} // namespace detail

namespace concepts {
template <typename T> struct can_alias : std::true_type {};
template <typename T>
struct can_alias<detail::no_alias_t<T> > : std::true_type {};
} // namespace concepts

template <
    typename L,
    typename R,
    REQUIRES((                             //
        concepts::matrix_same_type<        //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value and                   //
        concepts::matrix_maybe_same_shape< //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value                       //
        ))>
auto operator+(L&& l, R&& r) noexcept -> detail::add_op<L, R> {
  gu::assert_eq(l.rows(), r.rows());
  gu::assert_eq(l.cols(), r.cols());
  return {gu::wrap_ref(FWD(l)), gu::wrap_ref(FWD(r))};
}
template <
    typename L,
    typename R,
    REQUIRES((                             //
        concepts::matrix_same_type<        //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value and                   //
        concepts::matrix_maybe_same_shape< //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value                       //
        ))>
auto operator-(L&& l, R&& r) noexcept -> detail::sub_op<L, R> {
  gu::assert_eq(l.rows(), r.rows());
  gu::assert_eq(l.cols(), r.cols());
  return {gu::wrap_ref(FWD(l)), gu::wrap_ref(FWD(r))};
}

template <
    typename Matrix,
    typename Scalar,
    REQUIRES((                                                   //
        concepts::matrix<gu::remove_cvref_t<Matrix> >::value and //
        std::is_convertible<                                     //
            gu::remove_cvref_t<Scalar>,                          //
            typename gu::remove_cvref_t<Matrix>::scalar_t        //
            >::value                                             //
        ))>
auto operator/(Matrix&& mat, Scalar&& scal)
    -> detail::scal_div_op<Matrix, Scalar> {
  return {gu::wrap_ref(FWD(scal)), gu::wrap_ref(FWD(mat))};
}

template <
    typename Scalar,
    typename Matrix,
    REQUIRES((                                                   //
        concepts::matrix<gu::remove_cvref_t<Matrix> >::value and //
        std::is_convertible<                                     //
            gu::remove_cvref_t<Scalar>,                          //
            typename gu::remove_cvref_t<Matrix>::scalar_t        //
            >::value                                             //
        ))>
auto operator*(Scalar&& scal, Matrix&& mat)
    -> detail::scal_mul_op<Scalar, Matrix, true> {
  return {gu::wrap_ref(FWD(scal)), gu::wrap_ref(FWD(mat))};
}

template <
    typename Scalar,
    typename Matrix,
    REQUIRES((                                                   //
        concepts::matrix<gu::remove_cvref_t<Matrix> >::value and //
        std::is_convertible<                                     //
            gu::remove_cvref_t<Scalar>,                          //
            typename gu::remove_cvref_t<Matrix>::scalar_t        //
            >::value                                             //
        ))>
auto operator*(Matrix&& mat, Scalar&& scal)
    -> detail::scal_mul_op<Scalar, Matrix, false> {
  return {gu::wrap_ref(FWD(scal)), gu::wrap_ref(FWD(mat))};
}

template <
    typename L,
    typename R,
    REQUIRES((                                      //
        concepts::matrix_same_type<                 //
            gu::remove_cvref_t<L>,                  //
            gu::remove_cvref_t<R>                   //
            >::value and                            //
        gu::concepts::maybe_same<                   //
            typename gu::remove_cvref_t<L>::cols_t, //
            typename gu::remove_cvref_t<R>::rows_t  //
            >::value                                //
        ))>
auto operator*(L&& l, R&& r) -> detail::mat_mul_op<L, R> {
  gu::assert_eq(l.cols(), r.rows());
  return {gu::wrap_ref(FWD(l)), gu::wrap_ref(FWD(r))};
}

template <
    typename L,
    typename R,
    REQUIRES((                             //
        concepts::matrix_same_type<        //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value and                   //
        concepts::matrix_maybe_same_shape< //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value                       //
        ))>
auto operator+=(L&& l, R const& r) -> void {
  gu::assert_eq(l.rows(), r.rows());
  gu::assert_eq(l.cols(), r.cols());

  if (is_zero_sized(l)) {
    return;
  }

  if (concepts::can_alias<gu::remove_cvref_t<L> >::value) {
    l.eigen().noalias() += r.eigen();
  } else {
    l.eigen() += r.eigen();
  }
}
template <
    typename L,
    typename R,
    REQUIRES((                             //
        concepts::matrix_same_type<        //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value and                   //
        concepts::matrix_maybe_same_shape< //
            gu::remove_cvref_t<L>,         //
            gu::remove_cvref_t<R>          //
            >::value                       //
        ))>
auto operator-=(L&& l, R const& r) -> void {
  gu::assert_eq(l.rows(), r.rows());
  gu::assert_eq(l.cols(), r.cols());

  if (is_zero_sized(l)) {
    return;
  }

  if (concepts::can_alias<gu::remove_cvref_t<L> >::value) {
    l.eigen().noalias() -= r.eigen();
  } else {
    l.eigen() -= r.eigen().eval();
  }
}

template <
    typename Matrix,
    typename Scalar,
    REQUIRES((                                                   //
        concepts::matrix<gu::remove_cvref_t<Matrix> >::value and //
        gu::remove_cvref_t<Matrix>::is_writable and              //
        std::is_convertible<                                     //
            Scalar,                                              //
            typename gu::remove_cvref_t<Matrix>::scalar_t        //
            >::value                                             //
        ))>
auto operator/=(Matrix&& l, Scalar const& r) -> void {
  if (is_zero_sized(l)) {
    return;
  }
  l.eigen() /= r;
}
template <
    typename Matrix,
    typename Scalar,
    REQUIRES((
        concepts::matrix<gu::remove_cvref_t<Matrix> >::value and              //
        gu::bool_constant<gu::remove_cvref_t<Matrix>::is_writable>::value and //
        std::is_convertible<                                                  //
            gu::remove_cvref_t<Scalar>,                                       //
            typename gu::remove_cvref_t<Matrix>::scalar_t                     //
            >::value                                                          //
        ))>
auto operator*=(Matrix&& l, Scalar const& r) -> void {
  if (is_zero_sized(l)) {
    return;
  }
  l.eigen() *= r;
}

template <
    typename L,
    typename R,
    REQUIRES((                                      //
        gu::remove_cvref_t<L>::is_writable and      //
        concepts::matrix_same_type<                 //
            gu::remove_cvref_t<L>,                  //
            gu::remove_cvref_t<R>                   //
            >::value and                            //
        gu::concepts::maybe_same<                   //
            typename gu::remove_cvref_t<L>::cols_t, //
            typename gu::remove_cvref_t<R>::rows_t  //
            >::value                                //
        ))>
auto operator*=(L&& l, R const& r) -> void {
  gu::assert_eq(l.cols(), r.rows());
  gu::assert_eq(l.cols(), r.cols());

  if (is_zero_sized(l)) {
    return;
  }

  l.eigen() *= r.eigen();
}

template <
    typename T,
    REQUIRES_NO_DEFAULT_PARAMS(
        concepts::matrix<gu::remove_cvref_t<T> >::value)> //
auto trans(T&& mat) -> detail::trans_op<T> {
  return detail::trans_op<T>{gu::wrap_ref(FWD(mat))};
}

template <
    typename T,
    REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)> //
auto no_alias(T&& mat) -> detail::no_alias_t<T> {
  return detail::no_alias_t<T>{gu::wrap_ref(FWD(mat))};
}

template <typename T, REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto operator-(T&& mat) -> detail::neg_op<T> {
  return detail::neg_op<T>{gu::wrap_ref(FWD(mat))};
}

template <
    typename T,
    typename StartRow,
    typename StartCol,
    typename NumRows,
    typename NumCols,
    REQUIRES((                                              //
        concepts::matrix<gu::remove_cvref_t<T> >::value and //
        gu::concepts::meta_integral<StartRow>::value and    //
        gu::concepts::meta_integral<StartCol>::value and    //
        gu::concepts::meta_integral<NumRows>::value and     //
        gu::concepts::meta_integral<NumCols>::value         //
        ))>
auto block(
    T&& mat,
    StartRow start_row,
    StartCol start_col,
    NumRows nrows,
    NumCols ncols)
    -> detail::block_view_op<T, StartRow, StartCol, NumRows, NumCols> {
  fix<0> _0{};
  gu::assert_leq(_0, start_row);
  gu::assert_leq(start_row, mat.rows());
  gu::assert_leq(_0, nrows);
  gu::assert_leq(start_row + nrows, mat.rows());
  gu::assert_leq(_0, start_col);
  gu::assert_leq(start_col, mat.cols());
  gu::assert_leq(_0, ncols);
  gu::assert_leq(start_col + ncols, mat.cols());

  return {gu::wrap_ref(FWD(mat)), start_row, start_col, nrows, ncols};
}

template <
    typename T,
    typename StartRow,
    typename NumRows,
    REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto mid_rows(T&& mat, StartRow start_row, NumRows nrows)
    RETURNS_AUTO(block(FWD(mat), start_row, fix<0>{}, nrows, mat.cols()))
template <
    typename T,
    typename NumRows,
    REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto top_rows(T&& mat, NumRows nrows)
    RETURNS_AUTO(mid_rows(FWD(mat), fix<0>{}, nrows))
template <
    typename T,
    typename NumRows,
    REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto bot_rows(T&& mat, NumRows nrows)
    RETURNS_AUTO(mid_rows(FWD(mat), mat.rows() - nrows, nrows))

template <
    typename T,
    typename StartCol,
    typename NumCols,
    REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto mid_cols(T&& mat, StartCol start_col, NumCols ncols)
    RETURNS_AUTO(block(FWD(mat), fix<0>{}, start_col, mat.rows(), ncols))
template <
    typename T,
    typename NumCols,
    REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto left_cols(T&& mat, NumCols ncols)
    RETURNS_AUTO(mid_cols(FWD(mat), fix<0>{}, ncols))
template <
    typename T,
    typename NumCols,
    REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>
auto right_cols(T&& mat, NumCols ncols)
    RETURNS_AUTO(mid_cols(FWD(mat), mat.cols() - ncols, ncols))

#define BLOCK_FN_TEMPLATE(FnName, StartRow, StartCol)                          \
  template <                                                                   \
      typename T,                                                              \
      typename NumRows,                                                        \
      typename NumCols,                                                        \
      REQUIRES(concepts::matrix<gu::remove_cvref_t<T> >::value)>               \
  auto FnName(T&& mat, NumRows nrows, NumCols ncols)                           \
      RETURNS_AUTO(block(FWD(mat), StartRow, StartCol, nrows, ncols))

BLOCK_FN_TEMPLATE(top_left_block, fix<0>{}, fix<0>{})
BLOCK_FN_TEMPLATE(top_right_block, fix<0>{}, mat.cols() - ncols)
BLOCK_FN_TEMPLATE(bot_left_block, mat.rows() - nrows, fix<0>{})
BLOCK_FN_TEMPLATE(bot_right_block, mat.rows() - nrows, mat.cols() - ncols)

#undef BLOCK_FN_TEMPLATE

template <typename T, REQUIRES(concepts::matrix<T>::value)>
auto sum(const T& m_expr) -> typename T::scalar_t {
  return m_expr.eigen().sum();
}

template <typename T, REQUIRES(concepts::matrix<T>::value)>
auto norm2(const T& m_expr) -> typename T::scalar_t {
  return m_expr.eigen().squaredNorm();
}

template <typename T, REQUIRES(concepts::matrix<T>::value)>
auto norm(const T& m_expr) -> typename T::scalar_t {
  return m_expr.eigen().norm();
}

template <typename T, REQUIRES(concepts::matrix<T>::value)>
auto stable_norm(const T& m_expr) -> typename T::scalar_t {
  return m_expr.eigen().stableNorm();
}

template <typename T, REQUIRES(concepts::matrix<T>::value)>
auto value_from_1x1(const T& m_expr) -> typename T::scalar_t {
  gu::assert_eq(m_expr.rows(), fix<1>{});
  gu::assert_eq(m_expr.cols(), fix<1>{});
  return m_expr.eigen().value();
}

template <typename T, REQUIRES(concepts::matrix<T>::value)>
auto clone(T const& expr)
    -> mat<typename T::scalar_t, T::rows_t::value, T::cols_t::value> {
  using mat = np::mat<typename T::scalar_t, T::rows_t::value, T::cols_t::value>;
  auto out = uninitialized<typename T::scalar_t>::mat(expr.rows(), expr.cols());
  out = FWD(expr);
  return mat{out};
}

template <
    typename T,
    REQUIRES((                                //
        concepts::matrix<T>::value and        //
        not concepts::owning_matrix<T>::value //
        ))>
auto eval(T const& expr)
    -> mat<typename T::scalar_t, T::rows_t::value, T::cols_t::value> {
  return clone(expr);
}

template <typename T, REQUIRES(concepts::owning_matrix<T>::value)>
auto eval(T const& mat) noexcept -> gu::cref_t<T> {
  return mat;
}

namespace detail {
template <typename T> struct decomposition_t {
  T m_eigen_llt;

  using scalar_t = typename T::Scalar;
  auto success() const -> bool { return m_eigen_llt.info() == Eigen::Success; }
  template <typename V>
  auto solve(V&& v) const
      -> decltype(uninitialized<typename T::Scalar>::mat(v.rows(), v.cols())) {
    auto out = uninitialized<typename T::Scalar>::mat(v.rows(), v.cols());
    out.eigen() = m_eigen_llt.solve(v.eigen());
    return out;
  }
};

template <typename T> auto decomposition(T const& llt) -> decomposition_t<T> {
  return {llt};
}
} // namespace detail

template <typename T>
auto factorize(const T& mat)
    RETURNS_AUTO(detail::decomposition(as_eigen_cref(mat).llt()))

// Modifying operations
template <typename T, typename Scalar>
auto fill_constant(T&& mat, Scalar const& constant)
    -> gu::enable_if_t<concepts::matrix<gu::remove_cvref_t<T> >::value> {
  static_assert(
      not std::is_const<gu::deref_t<T> >::value,
      "first argument is not mutable.");
  mat.eigen().setConstant(constant);
}
} // namespace np

#endif /* end of include guard: INCLUDE_EIGEN_MATRIX_BASE_HPP_SF8C3OZL */
