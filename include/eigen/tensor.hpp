#ifndef INCLUDE_EIGEN_TENSOR_HPP_EWB0VID9
#define INCLUDE_EIGEN_TENSOR_HPP_EWB0VID9

#include "fmt/format.h"
#include "fmt/ostream.h"
#include "fmt/color.h"
#include "eigen/matrix.hpp"
#include "general_utilities.hpp"
#include <type_traits>

namespace np {

template <typename InDim_Left, typename InDim_Right, typename OutDim>
struct tensor_shape_t<                               //
    InDim_Left,                                      //
    InDim_Right,                                     //
    OutDim,                                          //
    gu::enable_if_t<                                 //
        gu::concepts::dynamic<InDim_Left>::value or  //
        gu::concepts::dynamic<InDim_Right>::value or //
        gu::concepts::dynamic<OutDim>::value         //
        >                                            //
    >                                                //
{
  using indiml_t = InDim_Left;
  using indimr_t = InDim_Right;
  using outdim_t = OutDim;
  using storage_size_t = dyn;
  constexpr tensor_shape_t(
      InDim_Left indiml_o, InDim_Right indimr_o, OutDim outdim_o) noexcept
      : _indiml(indiml_o),
        _indimr(indimr_o),
        _outdim(outdim_o){};
  constexpr auto indiml() const noexcept -> indiml_t { return this->_indiml; }
  constexpr auto indimr() const noexcept -> indimr_t { return this->_indimr; }
  constexpr auto outdim() const noexcept -> outdim_t { return this->_outdim; }
  constexpr auto size() const noexcept -> dyn {
    return this->indiml() * this->indimr() * this->outdim();
  }

private:
  indiml_t _indiml;
  indimr_t _indimr;
  outdim_t _outdim;
};

template <typename InDim_Left, typename InDim_Right, typename OutDim>
struct tensor_shape_t<                              //
    InDim_Left,                                     //
    InDim_Right,                                    //
    OutDim,                                         //
    gu::enable_if_t<                                //
        gu::concepts::fixed<InDim_Left>::value and  //
        gu::concepts::fixed<InDim_Right>::value and //
        gu::concepts::fixed<OutDim>::value          //
        >                                           //
    >                                               //
{
  using indiml_t = InDim_Left;
  using indimr_t = InDim_Right;
  using outdim_t = OutDim;
  using storage_size_t = gu::mul_t<gu::mul_t<InDim_Left, InDim_Right>, OutDim>;
  constexpr tensor_shape_t(InDim_Left, InDim_Right, OutDim){};
  constexpr auto indiml() const noexcept -> indiml_t { return {}; }
  constexpr auto indimr() const noexcept -> indimr_t { return {}; }
  constexpr auto outdim() const noexcept -> outdim_t { return {}; }
  constexpr auto size() const noexcept -> storage_size_t {
    return this->indiml() * this->indimr() * this->outdim();
  }
};

template <typename Scalar, typename Shape> struct tensor_view : Shape {
  using shape_t = Shape;
  using propagate_const_scalar_t = Scalar;
  using scalar_t = typename std::remove_const<Scalar>::type;

  using typename shape_t::indiml_t;
  using typename shape_t::indimr_t;
  using typename shape_t::outdim_t;

  using shape_t::indiml;
  using shape_t::indimr;
  using shape_t::outdim;

  using storage_size_t = decltype(gu::declval<shape_t>().size());

  using matrix_t = mat<                             //
      scalar_t,                                     //
      gu::mul_t<indiml_t, indimr_t>::value,         //
      outdim_t::value,                              //
      options_to_u64<                               //
          storage_config<tags::detail::col_major_t> //
          >()                                       //
      >;

  ~tensor_view() = default;

  tensor_view(
      propagate_const_scalar_t* data_ptr,
      indiml_t p,
      indimr_t q,
      outdim_t r) noexcept : shape_t(p, q, r),
                             m_data(data_ptr) {}
  tensor_view(tensor_view const&) noexcept = default;
  tensor_view(tensor_view&&) noexcept = default;

private:
  template <typename T>
  void assign_impl(T const& other)
      noexcept(std::is_copy_assignable<Scalar>::value) {

    gu::assert_eq(indiml(), other.indiml());
    gu::assert_eq(indimr(), other.indimr());
    gu::assert_eq(outdim(), other.outdim());
    if (not(indiml().as_isize() == 0) and //
        not(indimr().as_isize() == 0) and //
        not(outdim().as_isize() == 0)) {
      as_matrix_view() = other.as_matrix_view();
    }
  }

public:
  template <typename T>
  auto operator=(T const& other) noexcept(noexcept(assign_impl(other)))
      -> tensor_view& {
    assign_impl(other);
    return *this;
  }

  auto operator=(tensor_view const& other)
      noexcept(noexcept(assign_impl(other))) -> tensor_view& {
    if (this != &other) {
      assign_impl(other);
    }
    return *this;
  }
  auto operator=(tensor_view&& other) noexcept(noexcept(assign_impl(other)))
      -> tensor_view& {
    if (this != &other) {
      assign_impl(other);
    }
    return *this;
  }
  template <REQUIRES(not std::is_const<propagate_const_scalar_t>::value)>
  auto operator=(zero_t) -> tensor_view& {
    np::fill_constant(as_matrix_view(), zero_v);
    return *this;
  }

  template <typename T>
  auto operator%(const T& vec) const
      -> mat<scalar_t, indiml_t::value, indimr_t::value> {
    gu::assert_eq(outdim(), vec.rows());
    using inner_mat_t = mat<scalar_t, gu::mul_t<indiml_t, indimr_t>::value, 1>;
    using mat_view_t = mat_view<inner_mat_t>;

    auto res = uninitialized<scalar_t>::mat(indiml(), indimr());
    mat_view_t res_view{res.data(), indiml() * indimr(), fix<1>{}};
    res_view = as_matrix_view() * vec;

    return res;
  }

  auto operator()(isize l, isize r, isize o) noexcept
      -> propagate_const_scalar_t& {
    gu::assert_less_or_equal_runtime(l, indiml().as_isize());
    gu::assert_less_or_equal_runtime(r, indimr().as_isize());
    gu::assert_less_or_equal_runtime(o, outdim().as_isize());
    return as_matrix_view().eigen()(r * indiml().as_isize() + l, o);
  }

  auto operator()(isize l, isize r, isize o) const noexcept -> scalar_t const& {
    gu::assert_less_or_equal_runtime(l, indiml().as_isize());
    gu::assert_less_or_equal_runtime(r, indimr().as_isize());
    gu::assert_less_or_equal_runtime(o, outdim().as_isize());
    return as_matrix_view().eigen()(r * indiml().as_isize() + l, o);
  }

  auto as_view() -> tensor_view<scalar_t, shape_t> {
    return {data(), indiml(), indimr(), outdim()};
  };
  auto as_view() const -> tensor_view<scalar_t const, shape_t> {
    return {data(), indiml(), indimr(), outdim()};
  };

  auto as_matrix_view() const -> mat_view<const matrix_t> {
    return {data(), indiml() * indimr(), outdim()};
  }
  auto as_matrix_view() -> mat_view<matrix_t> {
    return {data(), indiml() * indimr(), outdim()};
  }

  auto data() const -> scalar_t const* { return m_data; }
  auto data() -> scalar_t* { return m_data; }

  propagate_const_scalar_t* m_data;
};

template <typename Scalar, typename Shape, typename... Options>
struct tensor : Shape {
  using shape_t = Shape;
  using scalar_t = Scalar;
  using opts_t = storage_config<Options..., tags::detail::col_major_t>;

  using typename shape_t::indiml_t;
  using typename shape_t::indimr_t;
  using typename shape_t::outdim_t;

  using shape_t::indiml;
  using shape_t::indimr;
  using shape_t::outdim;

  using storage_size_t = decltype(gu::declval<shape_t>().size());

  using storage_t = detail::storage_t<scalar_t, storage_size_t, opts_t>;

  storage_t m_storage;

  ~tensor() = default;
  tensor(indiml_t p, indimr_t q, outdim_t r)
      : shape_t(p, q, r), m_storage(p * q * r) {}
  tensor(tensor const& other)
      noexcept(std::is_nothrow_copy_constructible<storage_t>::value) = default;
  tensor(tensor&& other)
      noexcept(std::is_nothrow_move_constructible<storage_t>::value) = default;

public:
  auto as_view() -> tensor_view<scalar_t, shape_t> {
    return {data(), indiml(), indimr(), outdim()};
  };
  auto as_view() const -> tensor_view<scalar_t const, shape_t> {
    return {data(), indiml(), indimr(), outdim()};
  };

  template <typename T>
  auto operator=(T const& other) noexcept(noexcept(as_view() = other))
      -> tensor& {
    as_view() = other.as_view();
    return *this;
  }
  auto operator=(tensor const& other) noexcept(noexcept(as_view() = other))
      -> tensor& {
    if (this != &other) {
      as_view() = other.as_view();
    }
    return *this;
  }
  auto operator=(tensor&& other) noexcept(noexcept(as_view() = other))
      -> tensor& {
    if (this != &other) {
      as_view() = other.as_view();
    }
    return *this;
  }
  auto operator=(zero_t) -> tensor& {
    np::fill_constant(as_matrix_view(), zero_v);
    return *this;
  }

  using matrix_t =
      mat<scalar_t,
          gu::mul_t<indiml_t, indimr_t>::value,
          outdim_t::value,
          options_to_u64<opts_t>()>;

  template <typename T>
  auto operator%(const T& vec) const
      -> mat<scalar_t, indiml_t::value, indimr_t::value> {
    gu::assert_eq(outdim(), vec.rows());
    using inner_mat_t = mat<scalar_t, gu::mul_t<indiml_t, indimr_t>::value, 1>;
    using mat_view_t = mat_view<inner_mat_t>;

    auto res = uninitialized<scalar_t>::mat(indiml(), indimr());
    mat_view_t res_view{res.data(), indiml() * indimr(), fix<1>{}};
    res_view = as_matrix_view() * vec;

    return res;
  }

  auto operator()(isize l, isize r, isize o) noexcept -> scalar_t& {
    gu::assert_less_or_equal_runtime(l, indiml().as_isize());
    gu::assert_less_or_equal_runtime(r, indimr().as_isize());
    gu::assert_less_or_equal_runtime(o, outdim().as_isize());
    return as_matrix_view().eigen()(r * indiml().as_isize() + l, o);
  }

  auto operator()(isize l, isize r, isize o) const noexcept -> scalar_t const& {
    gu::assert_less_or_equal_runtime(l, indiml().as_isize());
    gu::assert_less_or_equal_runtime(r, indimr().as_isize());
    gu::assert_less_or_equal_runtime(o, outdim().as_isize());
    return as_matrix_view().eigen()(r * indiml().as_isize() + l, o);
  }

  auto as_matrix_view() const -> mat_view<const matrix_t> {
    return {data(), indiml() * indimr(), outdim()};
  }
  auto as_matrix_view() -> mat_view<matrix_t> {
    return {data(), indiml() * indimr(), outdim()};
  }

  auto data() const -> const scalar_t* { return m_storage.data(); }
  auto data() -> scalar_t* { return m_storage.data(); }
};

namespace concepts {
template <typename T, typename Enable = void>
struct tensor : std::false_type {};

template <typename Scalar, typename Shape, typename... Options>
struct tensor<np::tensor<Scalar, Shape, Options...> > : std::true_type {};

template <typename Scalar, typename Shape>
struct tensor<np::tensor_view<Scalar, Shape> > : std::true_type {};
} // namespace concepts

template <typename T, typename Scalar>
auto fill_constant(T&& ten, Scalar const& constant)
    -> gu::enable_if_t<concepts::tensor<gu::remove_cvref_t<T> >::value> {
  np::fill_constant(ten.as_matrix_view(), constant);
}

template <typename T>
auto fill_constant(T&& ten, zero_t)
    -> gu::enable_if_t<concepts::tensor<gu::remove_cvref_t<T> >::value> {
  np::fill_constant(ten.as_matrix_view(), zero_v);
}

template <typename Scalar>
template <
    typename InDimL,
    typename InDimR,
    typename OutDim,
    typename... Options>
auto uninitialized<Scalar>::ten(
    InDimL indiml,
    InDimR indimr,
    OutDim outdim,
    Options...) //
    -> tensor<Scalar, tensor_shape_t<InDimL, InDimR, OutDim>, Options...> {

  tensor<Scalar, tensor_shape_t<InDimL, InDimR, OutDim>, Options...> //
      out{indiml, indimr, outdim};

  np::fill_constant(
      out.as_matrix_view(), std::numeric_limits<Scalar>::quiet_NaN());
  return out;
}

template <
    typename Ten,
    typename RowVec,
    REQUIRES(concepts::matrix<RowVec>::value and concepts::tensor<Ten>::value)>
auto operator*(const RowVec& v, const Ten& t)
    -> mat<typename Ten::scalar_t, Ten::outdim_t::value, Ten::indimr_t::value> {
  gu::assert_eq(t.indiml(), v.cols());
  using scalar_t = typename Ten::scalar_t;
  using inner_mat_t =
      mat<scalar_t,
          1,
          gu::mul_t<typename Ten::outdim_t, typename Ten::indimr_t>::value>;
  using mat_view_t = mat_view<inner_mat_t>;

  using flat_tensor_t =
      mat<scalar_t,
          Ten::indiml_t::value,
          gu::mul_t<typename Ten::outdim_t, typename Ten::indimr_t>::value>;

  using flat_tensor_view_t = mat_view<const flat_tensor_t>;

  auto res = uninitialized<scalar_t>::mat(t.indimr(), t.outdim());
  mat_view_t res_view{res.data(), fix<1>{}, t.outdim() * t.indimr()};
  flat_tensor_view_t t_view{t.data(), t.indiml(), t.outdim() * t.indimr()};

  res_view = v * t_view;
  return mat<scalar_t, Ten::outdim_t::value, Ten::indimr_t::value>{
      np::trans(res)};
}

template <
    typename Ten,
    typename ColVec,
    REQUIRES(concepts::matrix<ColVec>::value and concepts::tensor<Ten>::value)>
auto operator*(const Ten& t, const ColVec& v)
    -> mat<typename Ten::scalar_t, Ten::outdim_t::value, Ten::indiml_t::value> {
  gu::assert_eq(t.indimr(), v.rows());
  using scalar_t = typename Ten::scalar_t;

  auto res = uninitialized<scalar_t>::mat(t.outdim(), t.indiml());
  res.eigen().setZero();

  const isize inl = t.indiml().as_isize();
  const isize inr = t.indimr().as_isize();
  const isize out = t.outdim().as_isize();

  for (int i = 0; i < out; ++i) {
    auto view = mat_view<
        const mat<scalar_t, Ten::indiml_t::value, Ten::indimr_t::value> >{
        t.data() + i * inl * inr, t.indiml(), t.indimr()};

    res.eigen().row(i).noalias() += (view * v).eigen();
  }

  return res;
}

// assumes no aliasing input/output
// arguments: out (L: p, R: q, O: k)
//            tensor (L: p, R: q, O: n)
//            matrix (k × n)
template <typename Out, typename Ten, typename Mat>
auto contraction_outer_axis(Out&& out, Ten const& ten, Mat const& mat)
    -> gu::enable_if_t<                                       //
        concepts::tensor<gu::remove_cvref_t<Out> >::value and //
        concepts::tensor<Ten>::value and                      //
        concepts::matrix<Mat>::value                          //
        > {
  gu::assert_eq(out.outdim(), mat.rows());
  gu::assert_eq(ten.outdim(), mat.cols());
  gu::assert_eq(ten.indiml(), out.indiml());
  gu::assert_eq(ten.indimr(), out.indimr());
  np::no_alias(out.as_matrix_view()) = ten.as_matrix_view() * mat.T();
}

// assumes no aliasing input/output
// arguments: out (L: k, R: q, O: n)
//            tensor (L: p, R: q, O: n)
//            matrix (k × p)
template <typename Out, typename Ten, typename Mat>
auto contraction_left_axis(Out&& out, Ten const& ten, Mat const& mat)
    -> gu::enable_if_t<                                       //
        concepts::tensor<gu::remove_cvref_t<Out> >::value and //
        concepts::tensor<Ten>::value and                      //
        concepts::matrix<Mat>::value                          //
        > {
  gu::assert_eq(out.outdim(), ten.outdim());
  gu::assert_eq(out.indiml(), mat.rows());
  gu::assert_eq(out.indimr(), ten.indimr());
  gu::assert_eq(ten.indiml(), mat.cols());
  // TODO: test

  using out_t = gu::remove_cvref_t<Out>;
  using scalar_t = typename Ten::scalar_t;
  using inner_mat_t = np::mat<
      scalar_t,
      out_t::indiml_t::value,
      gu::mul_t<typename out_t::indimr_t, typename out_t::outdim_t>::value>;

  using mat_view_t = mat_view<inner_mat_t>;

  using flat_tensor_t = np::mat<
      scalar_t,
      Ten::indiml_t::value,
      gu::mul_t<typename Ten::outdim_t, typename Ten::indimr_t>::value>;

  using flat_tensor_view_t = mat_view<const flat_tensor_t>;
  mat_view_t out_view{out.data(), out.indiml(), out.indimr() * out.outdim()};
  flat_tensor_view_t t_view{
      ten.data(), ten.indiml(), ten.outdim() * ten.indimr()};

  np::no_alias(out_view) = mat * t_view;
}

// assumes no aliasing input/output
// arguments: out (L: p, R: k, O: n)
//            tensor (L: p, R: q, O: n)
//            matrix (q × k)
template <typename Out, typename Ten, typename Mat>
auto contraction_right_axis(Out&& out, Ten const& ten, Mat const& mat)
    -> gu::enable_if_t<                                       //
        concepts::tensor<gu::remove_cvref_t<Out> >::value and //
        concepts::tensor<Ten>::value and                      //
        concepts::matrix<Mat>::value                          //
        > {
  gu::assert_eq(out.outdim(), ten.outdim());
  gu::assert_eq(out.indiml(), ten.indiml());
  gu::assert_eq(out.indimr(), mat.cols());
  gu::assert_eq(ten.indimr(), mat.rows());

  using scalar_t = typename Ten::scalar_t;

  out = np::zero_v;

  using out_t = gu::remove_cvref_t<Out>;
  for (int i = 0; i < ten.outdim().as_isize(); ++i) {
    auto view = mat_view<
        const np::mat<scalar_t, Ten::indiml_t::value, Ten::indimr_t::value> >{
        ten.data() + i * (ten.indiml() * ten.indimr()).as_isize(),
        ten.indiml(),
        ten.indimr()};

    auto out_view = mat_view<
        np::mat<scalar_t, out_t::indiml_t::value, out_t::indimr_t::value> >{
        out.data() + i * (out.indiml() * out.indimr()).as_isize(),
        out.indiml(),
        out.indimr()};

    np::no_alias(out_view) += (view * mat);
  }
}
} // namespace np

#endif /* end of include guard: INCLUDE_EIGEN_TENSOR_HPP_EWB0VID9 */
