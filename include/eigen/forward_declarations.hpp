#ifndef INCLUDE_EIGEN_FORWARD_DECLARATIONS_HPP_WGQYFMPJ
#define INCLUDE_EIGEN_FORWARD_DECLARATIONS_HPP_WGQYFMPJ

#include <tuple>
#include <type_traits>
#include "Eigen/Core"

#include "eigen/concepts.hpp"

namespace np {
using namespace gu::literals;
using gu::dyn;
using gu::fix;
using gu::isize;
using gu::u64;
using gu::usize;

template <
    typename Rows,
    typename Cols,
    isize Tag = 0,
    typename Enable = gu::enable_if_t<
        gu::concepts::meta_integral<Rows>::value and
        gu::concepts::meta_integral<Cols>::value> >
struct shape_t;

template <
    typename InDim_Left,                                    //
    typename InDim_Right,                                   //
    typename OutDim,                                        //
    typename Enable = gu::enable_if_t<                      //
        gu::concepts::meta_integral<InDim_Left>::value and  //
        gu::concepts::meta_integral<InDim_Right>::value and //
        gu::concepts::meta_integral<OutDim>::value          //
        >                                                   //
    >
struct tensor_shape_t;

template <typename Scalar, typename Shape, typename... Options> //
struct tensor;

namespace detail {

constexpr auto align_bytes = EIGEN_MAX_ALIGN_BYTES;

template <typename T, typename Align> struct mat_view;

// Unary ops
template <typename T> struct trans_op;
template <typename T> struct neg_op;
template <
    typename T,
    typename StartRow,
    typename StartCol,
    typename NumRows,
    typename NumCols>
struct block_view_op;

// Binary ops
template <typename L, typename R> struct add_op;
template <typename L, typename R> struct sub_op;
template <typename L, typename R> struct mat_mul_op;
template <typename Scalar, typename Matrix, bool Left_Mul> struct scal_mul_op;
template <typename Matrix, typename Scalar> struct scal_div_op;

template <typename T>
using rows_t = decltype(std::declval<gu::remove_cvref_t<T> >().rows());
template <typename T>
using cols_t = decltype(std::declval<gu::remove_cvref_t<T> >().cols());
} // namespace detail

template <typename Scalar> struct uninitialized;
} // namespace np

#endif /* end of include guard:                                                \
          INCLUDE_EIGEN_FORWARD_DECLARATIONS_HPP_WGQYFMPJ */
