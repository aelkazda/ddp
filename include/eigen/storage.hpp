#ifndef INCLUDE_EIGEN_STORAGE_HPP_A4XRWZUP
#define INCLUDE_EIGEN_STORAGE_HPP_A4XRWZUP

#include "fmt/format.h"
#include "eigen/forward_declarations.hpp"
#include "general_utilities.hpp"

#include <algorithm>
#include <type_traits>

namespace np {

namespace tags {
namespace detail {
enum struct alloc_t { on_heap, on_stack };
enum struct order_t { row_major, col_major };
enum struct mem_align_t { aligned, unaligned };
} // namespace detail

#define GENERATE_OPTION_TAG(Value, Type)                                       \
  namespace detail {                                                           \
  struct Value##_t {                                                           \
    static constexpr Type##_t storage_##Type = Type##_t::Value;                \
    constexpr operator Type##_t() const { return storage_##Type; }             \
  };                                                                           \
  }                                                                            \
  constexpr auto Value /* NOLINT(bugprone-macro-parentheses) */ =              \
      detail::Value##_t{};

GENERATE_OPTION_TAG(col_major, order)
GENERATE_OPTION_TAG(row_major, order)
GENERATE_OPTION_TAG(on_stack, alloc)
GENERATE_OPTION_TAG(on_heap, alloc)
GENERATE_OPTION_TAG(aligned, mem_align)
GENERATE_OPTION_TAG(unaligned, mem_align)

#undef GENERATE_OPTION_TAG
} // namespace tags

#define GENERATE_HAS_STORAGE_PROP(Property)                                    \
  template <typename T, typename Enable = void>                                \
  struct has_storage_##Property : gu::false_type {};                           \
  template <typename T>                                                        \
  struct has_storage_##Property<                                               \
      T,                                                                       \
      SPECIALIZE_IF_VALID_EXPR(T::storage_##Property)> : gu::true_type {};

GENERATE_HAS_STORAGE_PROP(order)
GENERATE_HAS_STORAGE_PROP(alloc)
GENERATE_HAS_STORAGE_PROP(mem_align)
#undef GENERATE_HAS_STORAGE_PROP

template <typename... Args>
struct storage_config
    : Args...,
      gu::conditional_t<
          gu::or_c<has_storage_order<Args>...>::value,
          gu::monostate_tpl<gu::index_constant<0> >, // empty struct
          tags::detail::col_major_t>,
      gu::conditional_t<
          gu::or_c<has_storage_alloc<Args>...>::value,
          gu::monostate_tpl<gu::index_constant<1> >, // empty struct
          tags::detail::on_stack_t>,
      gu::conditional_t<
          gu::or_c<has_storage_mem_align<Args>...>::value,
          gu::monostate_tpl<gu::index_constant<2> >, // empty struct
          tags::detail::unaligned_t> {};

constexpr auto set_bit_if(bool cond, u64 n, u64 pos) -> u64 {
  return cond ? n | (1_usize << pos) : n;
}
template <typename Options> constexpr auto options_to_u64() -> u64 {
  using namespace tags;
  return set_bit_if(
      Options::storage_mem_align == aligned,
      set_bit_if(
          Options::storage_alloc == on_heap,
          set_bit_if(Options::storage_order == row_major, 0, 0),
          1),
      2);
}

template <u64 N> struct u64_to_options {
  static constexpr auto is_set(u64 pos) -> bool {
    return (N & (1_usize << pos)) == (1_usize << pos);
  }
  using type = storage_config<
      gu::conditional_t<
          is_set(0),
          tags::detail::row_major_t,
          tags::detail::col_major_t>,
      gu::conditional_t<
          is_set(1), //
          tags::detail::on_heap_t,
          tags::detail::on_stack_t>,
      gu::conditional_t<
          is_set(2),
          tags::detail::aligned_t,
          tags::detail::unaligned_t> >;
};
constexpr u64 default_storage_config_u64 = options_to_u64<storage_config<> >();

namespace detail {

template <
    typename Scalar,
    typename Size,
    typename Options,
    typename Enable = void>
struct storage_t;

template <typename T, typename Size, typename Options> struct storage_base_t {

  // Traits
  static constexpr int /* same as Eigen size type */ n = Size::value;
  static constexpr isize dyn_v = gu::dyn::value;
  static constexpr isize align_val = EIGEN_MAX_ALIGN_BYTES / alignof(T);
  static constexpr isize rem = n % align_val;

  static constexpr bool has_fixed_size = not Size::is_runtime;

  static constexpr auto padded_size() -> int /* same as Eigen size type */ {
    return (n == dyn_v or Options::storage_alloc == tags::on_heap)
               ? Eigen::Dynamic
               : (align_val > 0 and (rem != 0) ? (n - rem + align_val) : n);
  }

  static constexpr auto eigen_size() -> int /* same as Eigen size type */ {
    return (n == dyn_v) ? Eigen::Dynamic : n;
  }

  // FIXME: Padding?
  static constexpr auto eigen_size_or_dyn()
      -> int /* same as Eigen size type */ {
    return (n == dyn_v or Options::storage_alloc == tags::on_heap)
               ? Eigen::Dynamic
               : n;
  }

  static constexpr isize align =
      (alignof(T) > detail::align_bytes) ? alignof(T) : detail::align_bytes;

  using data_t = Eigen::Matrix<
      T,
      eigen_size_or_dyn(),
      1,
      Eigen::AutoAlign,
      eigen_size_or_dyn(),
      1>;
  data_t m_data;

  ~storage_base_t() = default;

protected:
  explicit storage_base_t(data_t const& other) : m_data(other){};
  template <REQUIRES(has_fixed_size)> //
  storage_base_t() noexcept : m_data() {}

public:
  storage_base_t(const storage_base_t& other) = default;
  storage_base_t(storage_base_t&& other) noexcept = default;

  explicit storage_base_t(Size size)
      : m_data(has_fixed_size ? eigen_size() : size.as_isize()) {}

  auto operator=(const storage_base_t& other) -> storage_base_t& {
    // Doesn't resize
    if (this != &other) {
      gu::assert_eq(size(), other.size());
      std::copy(other.begin(), other.end(), begin());
    }
    return *this;
  }
  auto operator=(storage_base_t&& other) noexcept -> storage_base_t& = default;

  auto size() const -> Size { return Size{this->m_data.rows(), gu::unsafe}; }
  auto dyn_size() const -> isize { return size().as_isize(); }

  auto data() -> T* { return this->m_data.data(); }
  auto data() const -> const T* { return this->m_data.data(); }

  auto begin() -> T* { return data(); }
  auto begin() const -> const T* { return data(); }
  auto end() -> T* { return data() + dyn_size(); }
  auto end() const -> const T* { return data() + dyn_size(); }
};

template <typename T, typename Size, typename Options>
struct storage_t<T, Size, Options, gu::enable_if_t<not Size::is_runtime> >
    : storage_base_t<T, Size, Options> {
  using base_t = storage_base_t<T, Size, Options>;
  using base_t::has_fixed_size /* = true*/;

  using base_t::operator=;
  using base_t::base_t;
  using typename base_t::data_t;

protected:
  explicit storage_t(data_t const& other) : base_t(other){};

public:
  storage_t() noexcept = default;
  auto clone() const -> storage_t { return storage_t{base_t::m_data}; };
};

template <typename T, typename Size, typename Options>
struct storage_t<T, Size, Options, gu::enable_if_t<Size::is_runtime> >
    : storage_base_t<T, Size, Options> {
  using base_t = storage_base_t<T, Size, Options>;
  using base_t::has_fixed_size /* = false*/;

  using base_t::operator=;
  using base_t::base_t;
  using typename base_t::data_t;

protected:
  explicit storage_t(data_t const& other) : base_t(other){};

public:
  auto clone() const -> storage_t { return storage_t{base_t::m_data}; };
};
} // namespace detail
} // namespace np
#endif /* end of include guard: INCLUDE_EIGEN_STORAGE_HPP_A4XRWZUP */
