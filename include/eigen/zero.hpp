#ifndef INCLUDE_EIGEN_ZERO_HPP_9IJC4LH1
#define INCLUDE_EIGEN_ZERO_HPP_9IJC4LH1

#include "general_utilities.hpp"
#include "eigen/concepts.hpp"
#include <type_traits>

namespace np {
struct zero_t {
  auto T() const -> zero_t { return {}; }
  template <typename T> auto operator=(T const&) -> zero_t& {
    std::terminate();
  };
};

template <typename Lhs, typename Rhs>
auto assign_if_nonzero(Lhs&& lhs, Rhs&& rhs) -> void {
  FWD(lhs) = FWD(rhs);
}

template <typename Rhs> auto assign_if_nonzero(zero_t, Rhs &&) -> void {
  std::terminate();
}

constexpr zero_t zero_v{};
constexpr auto eval(zero_t) noexcept -> zero_t {
  return {};
}
constexpr auto clone(zero_t) noexcept -> zero_t {
  return {};
}
constexpr auto trans(zero_t) noexcept -> zero_t {
  return {};
}
template <typename T> constexpr auto operator%(zero_t, T&&) noexcept -> zero_t {
  return {};
}
template <typename T> constexpr auto operator%(T&&, zero_t) noexcept -> zero_t {
  return {};
}
constexpr auto operator%(zero_t, zero_t) noexcept -> zero_t {
  return {};
}

template <typename T> constexpr auto operator*(zero_t, T&&) noexcept -> zero_t {
  return {};
}
template <typename T> constexpr auto operator*(T&&, zero_t) noexcept -> zero_t {
  return {};
}
constexpr auto operator*(zero_t, zero_t) noexcept -> zero_t {
  return {};
}
template <typename T> constexpr auto operator/(zero_t, T&&) noexcept -> zero_t {
  return {};
}
template <typename T> void operator/(T&&, zero_t) noexcept = delete;
template <typename T> void operator/(zero_t, zero_t) noexcept = delete;

template <typename T>
auto operator+(zero_t, T&& x) RETURNS_DECLTYPE_AUTO(FWD(x))
template <typename T>
auto operator+(T&& x, zero_t) RETURNS_DECLTYPE_AUTO(FWD(x))
constexpr auto operator+(zero_t, zero_t) noexcept -> zero_t {
  return {};
}

template <typename T> auto operator+=(T&&, zero_t) noexcept -> void {}
template <typename T> auto operator-=(T&&, zero_t) noexcept -> void {}

template <typename T>
auto operator-(zero_t, T&& x) RETURNS_DECLTYPE_AUTO(-FWD(x))
template <typename T>
auto operator-(T&& x, zero_t) RETURNS_DECLTYPE_AUTO(FWD(x))
constexpr auto operator-(zero_t, zero_t) -> zero_t {
  return {};
}
constexpr auto operator+(zero_t) -> zero_t {
  return {};
}
constexpr auto operator-(zero_t) -> zero_t {
  return {};
}

} // namespace np

#endif /* end of include guard: INCLUDE_EIGEN_ZERO_HPP_9IJC4LH1 */
