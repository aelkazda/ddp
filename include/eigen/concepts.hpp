#ifndef INCLUDE_EIGEN_CONCEPTS_HPP_QM9CEQYA
#define INCLUDE_EIGEN_CONCEPTS_HPP_QM9CEQYA

#include <type_traits>
#include "general_utilities.hpp"
#include "Eigen/Core"

namespace np {
namespace concepts {
template <typename T>
struct eigen_matrix
    : std::is_base_of<Eigen::MatrixBase<typename std::remove_cv<T>::type>, T> {
};

template <typename T>
struct eigen_owning_matrix
    : std::is_base_of<
          Eigen::PlainObjectBase<typename std::remove_cv<T>::type>,
          T> {};

template <typename T>
struct eigen_writable_matrix
    : gu::and_c<
          eigen_matrix<T>,
          gu::bool_constant<
              (T::Flags & Eigen::LvalueBit) == Eigen::LvalueBit> > {};

} // namespace concepts
} // namespace np

#endif /* end of include guard: INCLUDE_EIGEN_CONCEPTS_HPP_QM9CEQYA */
