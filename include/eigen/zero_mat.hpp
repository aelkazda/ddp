#ifndef INCLUDE_EIGEN_ZERO_MAT_HPP_UW3IA6K7
#define INCLUDE_EIGEN_ZERO_MAT_HPP_UW3IA6K7

#include "general_utilities.hpp"
#include "eigen/matrix.hpp"
#include "eigen/zero.hpp"

namespace np {
namespace detail {
template <typename Scalar, isize Rows, isize Cols>
struct zero_mat
    : np::shape_t<
          gu::conditional_t<Rows == gu::dyn::value, dyn, fix<Rows> >,
          gu::conditional_t<Cols == gu::dyn::value, dyn, fix<Cols> > > {
  using scalar_t = Scalar;
  using shape_t = np::shape_t<
      gu::conditional_t<Rows == gu::dyn::value, dyn, fix<Rows> >,
      gu::conditional_t<Cols == gu::dyn::value, dyn, fix<Cols> > >;

  using shape_t::cols;
  using shape_t::rows;
  using rows_t = typename shape_t::rows_t;
  using cols_t = typename shape_t::cols_t;

  auto eigen() const -> zero_t { return {}; }
  zero_mat(rows_t n, cols_t m) : shape_t(n, m) {}
};
} // namespace detail

template <typename Scalar> struct static_zero {
  template <typename RowType, typename ColType>
  auto mat(RowType n, ColType m)
      -> detail::zero_mat<Scalar, RowType::value, ColType::value> {
    return {n, m};
  }

  template <typename rows_t>
  auto col(rows_t rows) //
      -> detail::zero_mat<Scalar, rows_t::value, 1> {
    return {rows, fix<1>{}};
  }

  template <typename cols_t>
  auto row(cols_t cols) //
      -> detail::zero_mat<Scalar, 1, cols_t::value> {
    return {fix<1>{}, cols};
  }
};

} // namespace np

#endif /* end of include guard: INCLUDE_EIGEN_ZERO_MAT_HPP_UW3IA6K7 */
