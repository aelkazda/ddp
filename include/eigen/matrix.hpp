#ifndef INCLUDE_EIGEN_MATRIX_HPP_FYEGSZMI
#define INCLUDE_EIGEN_MATRIX_HPP_FYEGSZMI

#include <limits>
#include <ostream>
#include <tuple>
#include <utility>

#include "Eigen/Core"

// FIXME: remove
#include "eigen/zero.hpp"
#include "general_utilities.hpp"

#include "eigen/forward_declarations.hpp"
#include "eigen/storage.hpp"
#include "eigen/matrix_base.hpp"

namespace np {

template <typename Rows, typename Cols, isize Tag>
struct shape_t<
    Rows,
    Cols,
    Tag,
    gu::enable_if_t<
        gu::concepts::dynamic<Rows>::value or
        gu::concepts::dynamic<Cols>::value> > {
  using rows_t = Rows;
  using cols_t = Cols;
  constexpr shape_t(rows_t rows, cols_t cols) noexcept : m_rows{rows},
                                                         m_cols{cols} {}
  constexpr auto rows() const noexcept -> rows_t { return m_rows; }
  constexpr auto cols() const noexcept -> cols_t { return m_cols; }
  constexpr auto size() const -> gu::mul_t<rows_t, cols_t> {
    return rows() * cols();
  }

private:
  rows_t m_rows;
  cols_t m_cols;
};

template <typename Rows, typename Cols, isize Tag>
struct shape_t<
    Rows,
    Cols,
    Tag,
    gu::enable_if_t<
        gu::concepts::fixed<Rows>::value and
        gu::concepts::fixed<Cols>::value> > {
  using rows_t = Rows;
  using cols_t = Cols;
  constexpr shape_t(rows_t, cols_t) noexcept {}
  constexpr auto rows() const noexcept -> rows_t { return {}; }
  constexpr auto cols() const noexcept -> cols_t { return {}; }
  constexpr auto size() const -> gu::mul_t<rows_t, cols_t> {
    return rows() * cols();
  }
};

template <typename Scalar, isize Rows, isize Cols, usize Options>
struct mat : public shape_t<
                 gu::conditional_t<Rows == gu::dyn::value, dyn, fix<Rows> >,
                 gu::conditional_t<Cols == gu::dyn::value, dyn, fix<Cols> > >,
             public detail::matrix_base<mat<Scalar, Rows, Cols, Options> > {
  using scalar_t = Scalar;

  using shape_t = np::shape_t<
      gu::conditional_t<Rows == gu::dyn::value, dyn, fix<Rows> >,
      gu::conditional_t<Cols == gu::dyn::value, dyn, fix<Cols> > >;

  using typename shape_t::cols_t;
  using typename shape_t::rows_t;

  using shape_t::cols;
  using shape_t::rows;

  using opts_t = typename u64_to_options<Options>::type;
  using storage_t =
      detail::storage_t<scalar_t, gu::mul_t<rows_t, cols_t>, opts_t>;
  using eigen_matrix_t = Eigen::Matrix<
      scalar_t,
      rows_t::value,
      cols_t::value,
      ((Cols == 1 or (opts_t::storage_order == tags::col_major and Rows != 1))
           ? Eigen::ColMajor
           : Eigen::RowMajor),
      rows_t::value,
      cols_t::value>;
  constexpr static auto eigen_align = opts_t::storage_mem_align == tags::aligned
                                          ? Eigen::AlignedMax
                                          : Eigen::Unaligned;
  constexpr static bool is_writable = true;
  using eigen_expr_t = Eigen::Map<eigen_matrix_t, eigen_align>;
  using eigen_const_expr_t = Eigen::Map<eigen_matrix_t const, eigen_align>;

  ~mat() = default;
  mat(rows_t n, cols_t m) : shape_t(n, m), m_storage(n * m) {}
  mat(rows_t n, cols_t m, storage_t&& s)
      : shape_t(n, m), m_storage(std::move(s)) {
    gu::assert_eq(m_storage.size(), n * m);
  }

  template <
      typename T,
      REQUIRES_(
          concepts::matrix<T>::value and
          not std::is_same<mat, gu::decay_t<T> >::value)>
  explicit mat(T const& other)
      noexcept(noexcept(eigen().operator=(other.eigen())))
      : shape_t(rows_t{other.rows()}, cols_t{other.cols()}),
        m_storage(other.rows() * other.cols()) {
    *this = other;
  }

  mat(mat const& other) = default;
  mat(mat&& other) noexcept = default;

  auto data() const -> const scalar_t* { return m_storage.data(); }
  auto data() -> scalar_t* { return m_storage.data(); }

  auto eigen() const -> Eigen::Map<const eigen_matrix_t, eigen_align> {
    return Eigen::Map<eigen_matrix_t const, eigen_align>(
        data(), rows().as_isize(), cols().as_isize());
  }
  auto eigen() -> Eigen::Map<eigen_matrix_t, eigen_align> {
    return Eigen::Map<eigen_matrix_t, eigen_align>(
        data(), rows().as_isize(), cols().as_isize());
  }

  template <
      typename T,
      REQUIRES_(
          concepts::matrix<gu::remove_cvref_t<T> >::value and
          not std::is_same<gu::remove_cvref_t<T>, mat>{})>
  auto operator=(T&& other) -> mat& {
    gu::assert_eq(rows(), other.rows());
    gu::assert_eq(cols(), other.cols());
    if (not is_zero_sized(*this)) {
      eigen() = FWD(other).eigen();
    }
    return *this;
  }
  auto operator=(const mat& other) -> mat& {
    if (this != &other) {
      gu::assert_eq(rows(), other.rows());
      gu::assert_eq(cols(), other.cols());
      if (not is_zero_sized(*this)) {
        eigen() = other.eigen();
      }
    }
    return *this;
  }
  auto operator=(mat&& other) noexcept -> mat& = default;
  auto operator=(zero_t) -> mat& {
    fill_constant(*this, zero_v);
    return *this;
  }

  storage_t m_storage;
};

template <typename T, tags::detail::mem_align_t Align = tags::unaligned>
struct mat_view : shape_t<typename T::rows_t, typename T::cols_t>,
                  public detail::matrix_base<mat_view<T, Align> > {
  using shape_t = np::shape_t<typename T::rows_t, typename T::cols_t>;
  using rows_t = typename shape_t::rows_t;
  using cols_t = typename shape_t::cols_t;

  using shape_t::cols;
  using shape_t::rows;

  using scalar_t = typename T::scalar_t;

  // FIXME remove
  template <typename U = scalar_t>
  operator gu::enable_if_t<std::is_same<U, zero_t>::value, zero_t>() {
    return {};
  }

  using eigen_matrix_t =
      gu::const_if_t<std::is_const<T>::value, typename T::eigen_matrix_t>;
  constexpr static auto eigen_align =
      Align == tags::aligned ? Eigen::AlignedMax : Eigen::Unaligned;

  using eigen_expr_t = Eigen::Map<eigen_matrix_t, eigen_align>;
  using eigen_const_expr_t = Eigen::Map<eigen_matrix_t const, eigen_align>;

  constexpr static bool is_writable = not std::is_const<T>::value;
  using ptr_t = gu::const_if_t<std::is_const<T>{}, typename T::scalar_t>*;

  ~mat_view() = default;
  mat_view(ptr_t data, rows_t n, cols_t m) : shape_t(n, m), m_data(data) {}
  mat_view(mat_view const&) noexcept = default;
  mat_view(mat_view&&) noexcept = default;

private:
  template <typename Other> void assign_impl(Other const& o) {
    gu::assert_eq(rows(), o.rows());
    gu::assert_eq(cols(), o.cols());
    if (not is_zero_sized(*this)) {
      eigen() = o.eigen();
    }
  }

public:
  template <
      typename Other,
      REQUIRES((                                   //
          concepts::matrix<Other>::value and       //
          is_writable and                          //
          not std::is_same<Other, mat_view>::value //
          ))>
  auto operator=(Other const& other) -> mat_view& {
    assign_impl(other);
    return *this;
  }

  auto operator=(mat_view const& other) -> mat_view& {
    if (this != &other) {
      assign_impl(other);
    }
    return *this;
  }
  auto operator= /* NOLINT(performance-noexcept-move-constructor) */(
      mat_view&& other) -> mat_view& {
    if (this != &other) {
      assign_impl(other);
    }
    return *this;
  }
  auto operator=(zero_t) -> mat_view& {
    fill_constant(*this, zero_v);
    return *this;
  }

  auto as_const() const -> mat_view<const T, Align> {
    return mat_view<const T, Align>(m_data, rows(), cols());
  }

  auto eigen() const -> Eigen::Map<const eigen_matrix_t, eigen_align> {
    return Eigen::Map<eigen_matrix_t const, eigen_align>(
        m_data, rows().as_isize(), cols().as_isize());
  }
  auto eigen() -> Eigen::Map<eigen_matrix_t, eigen_align> {
    return Eigen::Map<eigen_matrix_t, eigen_align>(
        m_data, rows().as_isize(), cols().as_isize());
  }

  ptr_t m_data;
};

template <typename T, REQUIRES(concepts::matrix<T>::value)>
auto operator<<(std::ostream& out, const T& mat) -> std::ostream& {
  out << mat.eigen();
  return out;
}

template <typename Scalar> struct uninitialized {
  template <
      typename InDimL,
      typename InDimR,
      typename OutDim,
      typename... Options>
  static auto ten(InDimL, InDimR, OutDim, Options...)
      -> tensor<Scalar, tensor_shape_t<InDimL, InDimR, OutDim>, Options...>;

  template <typename Rows, typename Cols, typename... Options>
  static auto mat(Rows rows, Cols cols, Options...) -> np::mat<
      Scalar,
      Rows::value,
      Cols::value,
      options_to_u64<storage_config<Options...> >()> {

    np::mat<
        Scalar,
        Rows::value,
        Cols::value,
        options_to_u64<storage_config<Options...> >()>

        out{rows, cols};

    out.eigen().setConstant(std::numeric_limits<Scalar>::quiet_NaN());
    return out;
  }

  template <typename Rows, typename... Options>
  static auto col(Rows rows, Options... opts)
      RETURNS_DECLTYPE_AUTO(mat(rows, fix<1>{}, tags::col_major, opts...))

  template <typename Cols, typename... Options>
  static auto row(Cols cols, Options... opts)
      RETURNS_DECLTYPE_AUTO(mat(fix<1>{}, cols, tags::row_major, opts...))
};

} // namespace np

#endif /* end of include guard: INCLUDE_EIGEN_MATRIX_HPP_FYEGSZMI */
