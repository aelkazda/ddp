#ifndef TENSOR_MAT_SEQ_HPP_PVX8TIJP
#define TENSOR_MAT_SEQ_HPP_PVX8TIJP

#include "eigen/tensor.hpp"
#include "general_utilities.hpp"
#include "indexers.hpp"

namespace ddp {
namespace detail {

using gu::isize;
template <
    typename Indexer_InDim_L,
    typename Indexer_InDim_R,
    typename Indexer_OutDim>
struct tensor_indexer_t {
  Indexer_InDim_L m_indim_l;
  Indexer_InDim_R m_indim_r;
  Indexer_OutDim m_outdim;
  isize m_total_size = 0;
  isize m_number_of_steps;

  using tensor_shape_t = np::tensor_shape_t<
      typename Indexer_InDim_L::rows_t,
      typename Indexer_InDim_R::rows_t,
      typename Indexer_OutDim::rows_t>;

  using indiml_t = typename tensor_shape_t::indiml_t;
  using indimr_t = typename tensor_shape_t::indimr_t;
  using outdim_t = typename tensor_shape_t::outdim_t;

  tensor_indexer_t(Indexer_InDim_L l, Indexer_InDim_R r, Indexer_OutDim o)
      : m_indim_l{std::move(l)},
        m_indim_r{std::move(r)},
        m_outdim{std::move(o)},
        m_number_of_steps{m_indim_l.n_steps} {

    gu::assert_eq(gu::dyn(m_indim_l.n_steps), gu::dyn(m_indim_r.n_steps));
    gu::assert_eq(gu::dyn(m_indim_l.n_steps), gu::dyn(m_outdim.n_steps));

    for (isize t = 0; t < m_number_of_steps; ++t) {
      m_total_size += stride(t).as_isize();
    }
  }

  auto stride(isize t) const -> typename tensor_shape_t::storage_size_t {
    return indiml(t) * indimr(t) * outdim(t);
  }

  auto total_size() const -> isize { return m_total_size; }

  auto indiml(isize t) const -> indiml_t { return m_indim_l.rows(t); }
  auto indimr(isize t) const -> indimr_t { return m_indim_r.rows(t); }
  auto outdim(isize t) const -> outdim_t { return m_outdim.rows(t); }

  struct iterator;
  struct proxy {
    tensor_indexer_t const& m_indexer;
    isize m_steps_so_far;
    isize m_memory_offset;

    auto indiml() const -> indiml_t { return m_indexer.indiml(m_steps_so_far); }
    auto indimr() const -> indimr_t { return m_indexer.indimr(m_steps_so_far); }
    auto outdim() const -> outdim_t { return m_indexer.outdim(m_steps_so_far); }

    auto time_idx() const -> isize { return m_steps_so_far; }
    auto to_forward_iterator() const -> iterator;
  };

  struct iterator {
    using difference_type = std::ptrdiff_t;
    using value_type = void;
    using pointer = proxy*;
    using reference = proxy;
    using iterator_category = std::bidirectional_iterator_tag;

    tensor_indexer_t const* m_indexer_ptr;
    isize m_steps_so_far;
    isize m_memory_offset;

    auto operator==(iterator other) const -> bool {
      return m_steps_so_far == other.m_steps_so_far;
    }
    auto operator!=(iterator other) const -> bool {
      return not(*this == other);
    }
    auto operator++() -> iterator& {
      if (m_indexer_ptr == nullptr) {
        std::terminate();
      }
      m_memory_offset += m_indexer_ptr->stride(m_steps_so_far).as_isize();
      ++m_steps_so_far;
      return *this;
    }
    auto operator--() -> iterator& {
      if (m_indexer_ptr == nullptr) {
        std::terminate();
      }
      --m_steps_so_far;
      m_memory_offset -= m_indexer_ptr->stride(m_steps_so_far).as_isize();
      return *this;
    }
    DDP_DEFINE_POSTFIX(iterator);

    auto operator*() const -> proxy {
      return proxy{*m_indexer_ptr, m_steps_so_far, m_memory_offset};
    }
  };

  using const_iterator = iterator;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() const -> iterator { return {this, 0, 0}; }
  auto end() const -> const_iterator {
    return {this, m_number_of_steps, total_size()};
  }
  auto rbegin() const -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() const -> const_reverse_iterator {
    return reverse_iterator{begin()};
  }
};

template <
    typename Indexer_InDim_L,
    typename Indexer_InDim_R,
    typename Indexer_OutDim>
auto tensor_indexer_t<Indexer_InDim_L, Indexer_InDim_R, Indexer_OutDim>::proxy::
    to_forward_iterator() const -> iterator {
  return iterator{&m_indexer, m_steps_so_far, m_memory_offset};
}

template <typename Scalar, typename Indexer> struct tensor_seq_t {
  struct storage_config {
    static constexpr auto storage_alloc = np::tags::on_heap;
    static constexpr auto storage_mem_align = np::tags::aligned;
  };

  using scalar_t = Scalar;
  using indexer_t = Indexer;
  using storage_t = np::detail::storage_t<scalar_t, gu::dyn, storage_config>;

  using indiml_t = typename indexer_t::indiml_t;
  using indimr_t = typename indexer_t::indimr_t;
  using outdim_t = typename indexer_t::outdim_t;

  using shape_t = np::tensor_shape_t<indiml_t, indimr_t, outdim_t>;

  indexer_t m_indexer;
  storage_t m_storage;

  using view_t = np::tensor_view<scalar_t, shape_t>;
  using cview_t = np::tensor_view<scalar_t const, shape_t>;

  explicit tensor_seq_t(indexer_t indexer)
      : m_indexer{std::move(indexer)},
        m_storage{gu::dyn(m_indexer.total_size())} {}

private:
  tensor_seq_t(indexer_t&& indexer, storage_t&& storage)
      : m_indexer(std::move(indexer)), m_storage(std::move(storage)) {}

public:
  auto clone() const -> tensor_seq_t {
    return {indexer_t{m_indexer}, m_storage.clone()};
  }

  template <bool IsConst> struct proxy_t {
    using inner_proxy_t = typename indexer_t::proxy;
    using data_ptr_t = gu::const_if_t<IsConst, Scalar>*;
    inner_proxy_t m_inner_proxy;
    data_ptr_t m_data;

    auto indiml() const -> indiml_t { return m_inner_proxy.indiml(); }
    auto indimr() const -> indimr_t { return m_inner_proxy.indimr(); }
    auto outdim() const -> outdim_t { return m_inner_proxy.outdim(); }

    auto offset() const -> isize { return m_inner_proxy.m_memory_offset; }

    auto get() const -> cview_t {
      return {m_data + offset(), indiml(), indimr(), outdim()};
    }
    auto get() -> gu::conditional_t<IsConst, cview_t, view_t> {
      return {m_data + offset(), indiml(), indimr(), outdim()};
    }

    auto operator*() RETURNS_AUTO(get())
    auto operator*() const RETURNS_AUTO(get())

    auto time_idx() const -> isize { return m_inner_proxy.time_idx(); }
    auto as_const() const -> proxy_t<true> { return {m_inner_proxy, m_data}; }
  };

  template <bool IsConst> struct iterator_impl_t {
    using indexer_iterator_t = typename indexer_t::iterator;
    using data_ptr_t = gu::const_if_t<IsConst, Scalar>*;
    using proxy_t = proxy_t<IsConst>;

    using difference_type = std::ptrdiff_t;
    using value_type = void;
    using pointer = proxy_t*;
    using reference = proxy_t;
    using iterator_category = std::bidirectional_iterator_tag;

    indexer_iterator_t m_iter;
    data_ptr_t m_data;

    auto operator==(iterator_impl_t other) const -> bool {
      return m_iter == other.m_iter;
    }
    auto operator!=(iterator_impl_t other) const -> bool {
      return not(*this == other);
    }
    auto operator++() -> iterator_impl_t& {
      ++m_iter;
      return *this;
    }
    auto operator--() -> iterator_impl_t& {
      --m_iter;
      return *this;
    }
    DDP_DEFINE_POSTFIX(iterator_impl_t);

    auto operator*() const -> proxy_t { return {*m_iter, m_data}; }
    auto operator*() -> proxy_t { return {*m_iter, m_data}; }
  };

  using iterator = iterator_impl_t<false>;
  using const_iterator = iterator_impl_t<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() -> iterator { return {m_indexer.begin(), m_storage.data()}; }
  auto end() -> iterator { return {m_indexer.end(), m_storage.data()}; }
  auto rbegin() -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() -> reverse_iterator { return reverse_iterator{begin()}; }

  auto begin() const -> const_iterator {
    return {m_indexer.begin(), m_storage.data()};
  }
  auto end() const -> const_iterator {
    return {m_indexer.end(), m_storage.data()};
  }
  auto rbegin() const -> const_reverse_iterator {
    return const_reverse_iterator{end()};
  }
  auto rend() const -> const_reverse_iterator {
    return const_reverse_iterator{begin()};
  }
};
} // namespace detail

template <
    typename Scalar,
    typename Indexer_InDim_L,
    typename Indexer_InDim_R,
    typename Indexer_OutDim>
auto tensor_seq(
    Indexer_InDim_L idx_indiml,
    Indexer_InDim_R idx_indimr,
    Indexer_OutDim idx_outdim)
    -> detail::tensor_seq_t<
        Scalar,
        detail::tensor_indexer_t<
            Indexer_InDim_L,
            Indexer_InDim_R,
            Indexer_OutDim> > {

  using indexer_t = detail::
      tensor_indexer_t<Indexer_InDim_L, Indexer_InDim_R, Indexer_OutDim>;

  return detail::tensor_seq_t<Scalar, indexer_t>{indexer_t{
      std::move(idx_indiml), //
      std::move(idx_indimr), //
      std::move(idx_outdim)  //
  }};
}
} // namespace ddp

#endif /* end of include guard TENSOR_MAT_SEQ_HPP_PVX8TIJP */
