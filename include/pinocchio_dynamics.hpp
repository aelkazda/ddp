#ifndef INCLUDE_PINOCCHIO_DYNAMICS_HPP_HECNMZKO
#define INCLUDE_PINOCCHIO_DYNAMICS_HPP_HECNMZKO

#include <csignal>
#include <cstdio>
#include <iostream>

#include "eigen/matrix.hpp"
#include "eigen/tensor.hpp"
#include "eigen/random.hpp"

#include "general_utilities.hpp"
#include "model.hpp"

#include "boost/optional.hpp"

#include "pinocchio/algorithm/aba-derivatives.hpp"
#include "pinocchio/algorithm/aba.hpp"

namespace ddp {

template <
    typename Model,
    typename Data,
    typename Nq_t = gu::dyn,
    typename Nv_t = gu::dyn>
struct pinocchio_dynamics_t {
  using model_t = Model;
  using data_t = Data;
  using scalar_t = typename model_t::Scalar;

  model_t m_model;
  mutable data_t m_data;
  scalar_t m_dt;
  bool m_eval_hessians;

  using config_vector_t = typename model_t::ConfigVectorType;
  using tangent_vector_t = typename model_t::TangentVectorType;

  using isize = gu::isize;
  template <isize N>
  using fixed_or_dyn = gu::conditional_t<N >= 0, gu::fix<N>, gu::dyn>;
  template <isize InDimL, isize InDimR, isize OutDim>
  using tensor_t = np::tensor<
      scalar_t,
      np::tensor_shape_t<
          fixed_or_dyn<InDimL>,
          fixed_or_dyn<InDimR>,
          fixed_or_dyn<OutDim> >,
      np::tags::detail::on_heap_t>;
  template <isize rows_t, isize cols_t>
  using matrix_t = np::mat<scalar_t, rows_t, cols_t>;
  template <isize Size> using vector_t = np::mat<scalar_t, Size, 1>;

public:
  static constexpr auto config_dim_static = Nq_t::value;
  static constexpr auto velocity_dim_static = Nv_t::value;
  static constexpr auto state_dim_static = gu::add_t<Nq_t, Nv_t>::value;
  static constexpr auto dstate_dim_static = gu::add_t<Nv_t, Nv_t>::value;

  auto nq() const -> Nq_t { return Nq_t{m_model.nq}; }
  auto nv() const -> Nv_t { return Nv_t{m_model.nv}; }

  template <typename InX, typename InU>
  auto f(const InX& in_x, const InU& in_u) const -> vector_t<state_dim_static> {
    using gu::fix;
    using uninit = np::uninitialized<scalar_t>;

    using np::bot_rows;
    using np::top_rows;

    const auto pos = top_rows(in_x, nq());
    const auto vel = bot_rows(in_x, nv());
    // pinocchio::aba(m_model, m_data, pos.eigen(), vel.eigen(), in_u.eigen());
    pinocchio::aba(
        m_model,
        m_data,
        np::as_eigen_cref(pos),
        np::as_eigen_cref(vel),
        np::as_eigen_cref(in_u));

    auto res = uninit::col(nq() + nv());
    top_rows(res, nq()) = pos + m_dt * vel;
    bot_rows(res, nv()).eigen() = vel.eigen() + m_dt * m_data.ddq;

    return res;
  }

  struct derivs_2nd_t {
    vector_t<state_dim_static> val;
    matrix_t<dstate_dim_static, dstate_dim_static> x;
    matrix_t<dstate_dim_static, velocity_dim_static> u;
    tensor_t<dstate_dim_static, dstate_dim_static, dstate_dim_static> xx;
    tensor_t<velocity_dim_static, dstate_dim_static, dstate_dim_static> ux;
    tensor_t<velocity_dim_static, velocity_dim_static, dstate_dim_static> uu;
  };

  template <typename InX, typename InU>
  auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {
    using gu::fix;
    using uninit = np::uninitialized<scalar_t>;

    using np::bot_rows;
    using np::top_rows;

    using np::bot_left_block;
    using np::bot_right_block;
    using np::top_left_block;
    using np::top_right_block;

    const auto pos = np::eval(top_rows(in_x, nq()));
    const auto vel = np::eval(bot_rows(in_x, nv()));
    const auto u = np::eval(in_u);

    struct input_t {
      decltype(uninit::col(pos.rows())) const& pos;
      decltype(uninit::col(vel.rows())) const& vel;
      decltype(uninit::col(in_u.rows())) const& u;
    };

    auto nx = nv() + nv();
    auto nu = nv();

    auto val = uninit::col(nq() + nv());
    auto jx = uninit::mat(nx, nx);
    auto ju = uninit::mat(nx, nu);

    // used for finite difference derivatives
    auto jx_2 = uninit::mat(nx, nx);
    auto ju_2 = uninit::mat(nx, nu);

    auto hxx = uninit::ten(nx, nx, nx, np::tags::on_heap);
    auto hux = uninit::ten(nu, nx, nx, np::tags::on_heap);
    auto huu = uninit::ten(nu, nu, nx, np::tags::on_heap);

    auto set_derivs = [this](
                          decltype(jx) & jx_out, //
                          decltype(ju) & ju_out, //
                          input_t const& in      //
                      ) {
      top_left_block(jx_out, nv(), nv()).eigen().setIdentity();

      top_right_block(jx_out, nv(), nv()).eigen().setIdentity();
      top_right_block(jx_out, nv(), nv()).eigen() *= m_dt;

      np::fill_constant(top_rows(ju_out, nv()), 0);

      pinocchio::computeABADerivatives(
          m_model,
          m_data,
          np::as_eigen_cref(in.pos),
          np::as_eigen_cref(in.vel),
          np::as_eigen_cref(in.u),
          np::as_eigen_ref(bot_left_block(jx_out, nv(), nv())),
          np::as_eigen_ref(bot_right_block(jx_out, nv(), nv())),
          np::as_eigen_ref(bot_rows(ju_out, nv())));

      bot_rows(jx_out, nv()).eigen() *= m_dt;
      // TODO optimize
      {
        auto identity = uninit::mat(nv(), nv());
        identity.eigen().setIdentity();
        bot_right_block(jx_out, nv(), nv()).eigen() += identity.eigen();
      }
      bot_rows(ju_out, nv()).eigen() *= m_dt;
    };

    set_derivs(jx, ju, input_t{pos, vel, u});
    top_rows(val, nq()) = pos + m_dt * vel;
    bot_rows(val, nv()).eigen() = vel.eigen() + m_dt * m_data.ddq;

    np::fill_constant(hxx.as_matrix_view(), 0);
    np::fill_constant(hux.as_matrix_view(), 0);
    np::fill_constant(huu.as_matrix_view(), 0);

    if (m_eval_hessians) {
      using std::sqrt;
      auto eps = sqrt(std::numeric_limits<scalar_t>::epsilon());

      auto pos_2 = np::eval(pos);
      auto vel_2 = np::eval(vel);
      auto u_2 = np::eval(in_u);

      for (gu::isize i = 0; i < nx.as_isize(); ++i) {

        if (i < nq().as_isize()) {
          pos_2.eigen()(i) += eps;
        } else {
          vel_2.eigen()(i - nq().as_isize()) += eps;
        }

        set_derivs(jx_2, ju_2, input_t{pos_2, vel_2, u_2});

        for (gu::isize k = 0; k < nx.as_isize(); ++k) {

          for (gu::isize j = 0; j < nx.as_isize(); ++j) {
            hxx(j, i, k) = (jx_2 - jx).eigen()(k, j) / eps;
          }
          for (gu::isize j = 0; j < nu.as_isize(); ++j) {
            hux(j, i, k) = (ju_2 - ju).eigen()(k, j) / eps;
          }
        }

        if (i < nq().as_isize()) {
          pos_2.eigen()(i) = pos.eigen()(i);
        } else {
          vel_2.eigen()(i - nq().as_isize()) = vel.eigen()(i - nq().as_isize());
        }
      }

      for (gu::isize i = 0; i < nu.as_isize(); ++i) {

        u_2.eigen()(i) += eps;
        set_derivs(jx_2, ju_2, input_t{pos_2, vel_2, u_2});

        for (gu::isize k = 0; k < nx.as_isize(); ++k) {
          for (gu::isize j = 0; j < nu.as_isize(); ++j) {
            huu(j, i, k) = (ju_2 - ju).eigen()(k, j) / eps;
          }
        }

        u_2.eigen()(i) = u.eigen()(i);
      }
    }

    return {
        std::move(val),
        std::move(jx),
        std::move(ju),
        std::move(hxx),
        std::move(hux),
        std::move(huu)};
  }

  struct functor_t {
    gu::cref_t<pinocchio_dynamics_t> parent;

    auto out_dim() const noexcept -> gu::add_t<Nq_t, Nv_t> {
      return parent.nq() + parent.nv();
    }
    auto x_dim() const noexcept -> gu::add_t<Nq_t, Nv_t> {
      return parent.nq() + parent.nv();
    }
    auto u_dim() const noexcept -> Nv_t { return parent.nv(); }

    template <typename InX, typename InU>
    auto operator()(InX const& in_x, InU const& in_u) const
        -> vector_t<state_dim_static> {
      return parent.f(in_x, in_u);
    }

    template <typename InX, typename InU>
    auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {
      return parent.derivs_2(in_x, in_u);
    }
  };

  auto operator()(isize) const -> functor_t { return {*this}; }
};
} /* namespace ddp */

#endif /* end of include guard: INCLUDE_PINOCCHIO_DYNAMICS_HPP_HECNMZKO */
