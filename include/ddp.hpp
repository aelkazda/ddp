#ifndef DDP_HPP_0IVMFIAH
#define DDP_HPP_0IVMFIAH

#include "detail/ddp.hpp"

#define DDP_PREFIX(...)                                                        \
  template <                                                                   \
      typename Scalar,                                                         \
      typename StateIdx,                                                       \
      typename ControlIdx,                                                     \
      typename EqIdx,                                                          \
      typename IneqIdx,                                                        \
      typename Transition,                                                     \
      typename Cost,                                                           \
      typename CostFinal,                                                      \
      typename EqConstraint,                                                   \
      typename IneqConstraint>                                                 \
  __VA_ARGS__ auto ddp_solver_t<                                               \
      Scalar,                                                                  \
      StateIdx,                                                                \
      ControlIdx,                                                              \
      EqIdx,                                                                   \
      IneqIdx,                                                                 \
      Transition,                                                              \
      Cost,                                                                    \
      CostFinal,                                                               \
      EqConstraint,                                                            \
      IneqConstraint>

namespace ddp {
DDP_PREFIX(template <method M, typename Logger, typename Update_Strategy>)::
    solve(
        Logger&& log,
        Update_Strategy const& update,
        solver_parameters_t<scalar_t> solver_parameters,
        trajectory_t traj) const->void {

  using std::max;
  using std::pow;

  auto mults = zero_multipliers<M>();

  struct iterate_info {
    isize iteration_num;
    trajectory_t const& traj;
    decltype(mults) const& mults;
    solver_parameters_t<scalar_t> const& old_parameters;
    scalar_t const& new_reg;
    scalar_t const& step;
    scalar_t const& opt_score;
    scalar_t const& opt_obj;
    scalar_t const& opt_constr;
    scalar_t const& opt_lag;
  };

  auto& mu = solver_parameters.mu;

  auto& w = solver_parameters.w;
  auto& n = solver_parameters.n;
  auto& reg = solver_parameters.reg;

  auto derivs = uninit_derivative_storage(traj);

  log.header();
  for (isize i = 0; i < solver_parameters.max_iterations; ++i) {
    compute_derivatives(traj, derivs);
    scalar_t opt_constr = optimality_constr(derivs);
    scalar_t opt_obj = optimality_obj(traj, mults, mu, derivs);
    scalar_t opt_lag = optimality_lag(traj, mults, derivs);

    auto const& opt = max(opt_lag, opt_constr);

    auto traj_concat = np::uninitialized<scalar_t>::col(
        np::dyn{x_idx.total_size() + u_idx.total_size()});
    auto eq_mult_concat =
        np::uninitialized<scalar_t>::col(np::dyn(eq_idx.total_size()));

    if (opt < solver_parameters.optimality_stopping_threshold) {
      break;
    }

    auto b_res = backward_pass<M>(traj, mults, reg, mu, derivs);
    auto f_res = forward_pass<M>(traj, mults, b_res, true);

    // clang-format off
    log(iterate_info{i, traj, mults,
        solver_parameters, b_res.regularization, f_res.step,
        opt, opt_obj, opt_constr, opt_lag});
    // clang-format on

    {
      auto new_reg = update.reg(b_res.regularization, f_res.step);
      if (new_reg.has_value()) {
        reg = *new_reg;
      } else {
        break;
      }
    }

    if (opt_obj < w) {
      if (opt_constr < n) {
        mults = std::move(f_res.mults);
        update.on_success(mu, n, w);
      } else {
        update.on_failure(mu, n, w);
      }
    }

    traj = std::move(f_res.new_traj);
  }
}

DDP_PREFIX(template <method M>)
::backward_pass(                                             //
    trajectory_t const& traj,                                //
    typename multiplier_sequence_type<M>::type const& mults, //
    scalar_t reg,                                            //
    scalar_t mu,                                             //
    derivative_storage_t const& d                            //
) const                                                      //
    ->backward_pass_result_t<M> {

  bool success = false;
  auto ctrl_fb = control_feedback_t(u_idx, x_idx_truncated);
  auto mult_fb = zero_feedback_multipliers<M>();

  while (not success) {
    auto const x_f = traj.x_f();
    auto const l_derivs = l_f.derivs_2(x_f);

    auto V_xx = eval(l_derivs.xx);
    auto V_x = eval(l_derivs.x.T());
    // clang-format off
      for (auto elems : gu::ranges::zip(
               traj, d.l(), d.f(), d.eq(), d.ineq(),
               mults.eq, mults.ineq, ctrl_fb, mult_fb.eq, mult_fb.ineq)
              | boost::adaptors::reversed) {
        BIND(auto&&,
            (xu, l, f, eq, ineq,
             eq_mult_old, ineq_mult_old, u_fb, eq_fb, ineq_fb),
            elems);
      // clang-format on
      isize t = xu.time_idx();

      auto const x = xu.x();
      auto const pe = eq_mult_old.derivs_1(x);
      auto const pi = ineq_mult_old.derivs_1(x);
      gu::unused(ineq, ineq_mult_old, ineq_fb, pi); // FIXME

      using detail::sum;
      using detail::sum_into;

      auto tmp = sum(pe.val, mu * eq.val);

      auto const Q_x = sum(   //
          l.x.T(),            //
          f.x.T() * V_x,      //
          eq.x.T() * tmp,     //
          pe.jac.T() * eq.val //
      );
      auto const Q_u = sum( //
          l.u.T(),          //
          f.u.T() * V_x,    //
          eq.u.T() * tmp    //
      );
      auto const Q_xx = sum(               //
          l.xx,                            //
          f.x.T() * V_xx * f.x,            //
          f.xx % V_x,                      //
          eq.xx % tmp,                     //
          eq.x.T() * (pe.jac + mu * eq.x), //
          pe.jac.T() * eq.x                //
      );
      auto const Q_uu = sum(    //
          l.uu,                 //
          f.u.T() * V_xx * f.u, //
          f.uu % V_x,           //
          eq.uu % tmp,          //
          mu * eq.u.T() * eq.u  //
      );
      auto const Q_ux = sum(              //
          l.ux,                           //
          f.u.T() * V_xx * f.x,           //
          f.ux % V_x,                     //
          eq.ux % tmp,                    //
          eq.u.T() * (mu * eq.x + pe.jac) //
      );

      auto I_u = np::uninitialized<scalar_t>::mat(Q_uu.rows(), Q_uu.cols());
      auto I_x = np::uninitialized<scalar_t>::mat(Q_xx.rows(), Q_xx.cols());
      I_u.eigen().setIdentity();
      I_x.eigen().setIdentity();

      auto const fact = factorize(Q_uu + reg * I_u);
      if (not fact.success()) {
        spdlog::debug("Increasing reg at t={}: {}", t, reg);
        for (auto const& xu : traj) {
          fmt::print("\n{}\n", xu.x().T());
          fmt::print("{}\n", xu.u().T());
        }
        fmt::print("\n{}\n", traj.x_f().T());
        fmt::print("\n\n");
        fmt::print("\n\n{}\n\n", Q_uu);
        fmt::print("\n\n{}\n\n", l.uu);
        fmt::print("\n\n{}\n\n", f.u.T() * V_xx * f.u);
        fmt::print("\n\n{}\n\n", f.u);
        fmt::print("\n\n{}\n\n", V_xx);
        fmt::print("\n\n{}\n\n", f.uu % V_x);
        fmt::print("\n\n{}\n\n", reg * I_u);
        if (reg == 0) {
          reg = 1.0;
        }
        mu *= 2;
        reg *= 2;
        break;
      }

      u_fb.val() = fact.solve(-Q_u);
      u_fb.jac() = fact.solve(-Q_ux);

      auto const k = u_fb.val();
      auto const K = u_fb.jac();

      if (M != method::primal) {
        eq_fb.jac() = mu * (eq.x + eq.u * K);
      }

      if (M == method::primal_dual_constant_multipliers) {
        eq_fb.val() = mu * (eq.val + eq.u * k);
      }
      if (M == method::primal_dual_affine_multipliers) {
        eq_fb.val() = eq_mult_old.val() +
                      mu * (eq.val - eq.x * x + eq.u * k - eq.u * K * x);
      }

      sum_into(np::no_alias(V_x), Q_x, Q_ux.T() * k);
      sum_into(np::no_alias(V_xx), Q_xx, Q_ux.T() * K);

      if (t == 0) {
        success = true;
      }
    }
  }

  return {
      std::move(ctrl_fb), std::move(mult_fb), std::move(mu), std::move(reg)};
}

DDP_PREFIX(template <method M>)
::forward_pass(                                                  //
    trajectory_t const& old_traj,                                //
    typename multiplier_sequence_type<M>::type const& old_mults, //
    backward_pass_result_t<M> const& bp_res,                     //
    bool do_linesearch                                           //
) const->forward_pass_result_t<M> {

  auto const& mu = bp_res.mu;
  scalar_t s = 1;
  auto new_traj = old_traj.clone();
  auto new_mults = zero_multipliers<M>();

  auto costs = cost_seq_aug(old_traj, old_mults, mu);
  bool success = false;
  while (not success) {
    if (s < 1e-10) {
      break;
    }

    // clang-format off
    for (auto elems : gu::ranges::zip(
             old_traj, new_traj, old_mults.eq, new_mults.eq,
             old_mults.ineq, new_mults.ineq,
             bp_res.feedback, bp_res.mults.eq, bp_res.mults.ineq)) {
      BIND(auto&&,
          (xu_o, xu_n, eq_o, eq_n,
           ineq_o, ineq_n,
           fb, eq_fb, ineq_fb),
          elems);
      //clang-format on
      // FIXME
      gu::unused(ineq_o, ineq_n, ineq_fb);

      isize t = xu_o.time_idx();
      auto const x_n = xu_n.x();
      auto const u_n = xu_n.u();
      auto const x_o = xu_o.x();
      auto const u_o = xu_o.u();

      auto const k = fb.val();
      auto const K = fb.jac();

      update_at_pos(
          xu_n, //
          u_o + (s * k) + K * (x_n - x_o));

      if (M == method::primal) {
        eq_n.val() = eq_o.val() + mu * eq_seq(t)(x_n, u_n);
      }
      if (M == method::primal_dual_constant_multipliers) {
        eq_n.val() = eq_o.val() + (s * eq_fb.val()) + eq_fb.jac() * (x_n - x_o);
      }
      if (M == method::primal_dual_affine_multipliers) {
        eq_n.jac() = eq_o.jac() + 1 * eq_fb.jac();
        eq_n.val() = -eq_n.jac() * x_n + eq_o(x_n) + mu * eq_seq(t)(x_n, u_n);
      }
    }

    auto new_costs = cost_seq_aug(new_traj, old_mults, bp_res.mu);
    if (do_linesearch) {
      // TODO: line search stopping criterion
      // get value function decrease from or infinitesimal step forward pass
      if (np::sum(new_costs - costs) < 0) {
        success = true;
      } else {
        s /= 2;
      }
    } else {
      success = true;
    }
  }

  return {std::move(new_traj), std::move(new_mults), std::move(s)};
}

} // namespace ddp

#undef DDP_PREFIX
#endif /* end of include guard DDP_HPP_0IVMFIAH */
