#ifndef MACROS_IMPL_HPP_G1B8SIJ0
#define MACROS_IMPL_HPP_G1B8SIJ0

#include "boost/preprocessor/cat.hpp"
#include "boost/preprocessor/tuple/size.hpp"
#include "boost/preprocessor/tuple/to_seq.hpp"
#include "boost/preprocessor/seq/for_each.hpp"
#include "boost/preprocessor/punctuation/remove_parens.hpp"

#define UTIL_FWD(...) (::std::forward<decltype(__VA_ARGS__)>(__VA_ARGS__))
#define UTIL_FWD_PACK(...) ::std::forward<decltype(__VA_ARGS__)>(__VA_ARGS__)...
#define UTIL_RETURNS_AUTO(...)                                                 \
  noexcept(noexcept(gu::decay_t<decltype(__VA_ARGS__)>{__VA_ARGS__}))          \
      ->gu::decay_t<decltype(__VA_ARGS__)> {                                   \
    return __VA_ARGS__;                                                        \
  }
#define UTIL_RETURNS_DECLTYPE_AUTO(...)                                        \
  noexcept(noexcept(__VA_ARGS__))->decltype(__VA_ARGS__) { return __VA_ARGS__; }
#define UTIL_REQUIRES(...)                                                     \
  typename _concept_Dummy_ = ::std::true_type,                                 \
           typename ::std::enable_if < (__VA_ARGS__) and                       \
               _concept_Dummy_::value,                                         \
           int > ::type = 0
#define UTIL_REQUIRES_NO_DEFAULT_PARAMS(...)                                   \
  typename _concept_Dummy_,                                                    \
      typename ::std::                                                         \
          enable_if<(__VA_ARGS__) and _concept_Dummy_::value, int>::type

// Use to suppress clang-tidy warning when specializing forwarding reference
// // constructors
#define UTIL_REQUIRES_(...)                                                    \
  typename = typename ::std::enable_if<__VA_ARGS__>::type

#define UTIL_SPECIALIZE_IF(...) typename ::std::enable_if<__VA_ARGS__>::type
#define UTIL_SPECIALIZE_IF_VALID_EXPR(...) ::gu::void_t<decltype(__VA_ARGS__)>

/******************************************************************************/
#define Z_UTIL_BIND_HELPER(r, Tuple, Index, Identifier)                        \
  auto&& Identifier = ::gu::detail::get<Index>(Tuple);

#define Z_UTIL_BIND_ID_SEQ(CV_Auto, Identifiers, Tuple, SeqSize, TupleId)      \
  static_assert(                                                               \
      ::std::tuple_size<                                                       \
          typename ::std::decay<decltype(Tuple)>::type>::value == SeqSize,     \
      "Wrong number of identifiers.");                                         \
  CV_Auto TupleId = Tuple;                                                     \
  BOOST_PP_SEQ_FOR_EACH_I(Z_UTIL_BIND_HELPER, TupleId, Identifiers)            \
  ((void)0)

#define Z_UTIL_BIND(CV_Auto, Identifiers, Tuple)                               \
  Z_UTIL_BIND_ID_SEQ(                                                          \
      CV_Auto,                                                                 \
      BOOST_PP_TUPLE_TO_SEQ(Identifiers),                                      \
      Tuple,                                                                   \
      BOOST_PP_TUPLE_SIZE(Identifiers),                                        \
      BOOST_PP_CAT(_dummy_tuple_variable_id_, __LINE__))
/******************************************************************************/

#endif /* end of include guard MACROS_IMPL_HPP_G1B8SIJ0 */
