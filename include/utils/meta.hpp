#ifndef META_HPP_XNWUCBGJ
#define META_HPP_XNWUCBGJ

#include <cstddef>
#include <type_traits>

namespace gu {
template <typename T> using decay_t = typename std::decay<T>::type;

template <typename T>
using remove_cvref_t =
    typename std::remove_cv<typename std::remove_reference<T>::type>::type;

template <typename T> using ref_t = remove_cvref_t<T>&;
template <typename T> using cref_t = remove_cvref_t<T> const&;
template <typename T> using rv_ref_t = remove_cvref_t<T>&&;
template <typename T> using rv_cref_t = remove_cvref_t<T> const&&;
template <typename T> using deref_t = typename std::remove_reference<T>::type;

template <bool Cond, typename T = void> //
using enable_if_t = typename std::enable_if<Cond, T>::type;

using std::false_type;
using std::true_type;

using std::declval;

template <typename... Ts> struct always_true : std::true_type {};
template <typename... Ts> struct always_false : std::false_type {};

template <std::size_t N>
using index_constant = std::integral_constant<std::size_t, N>;
template <bool Cond> using bool_constant = std::integral_constant<bool, Cond>;
template <bool Cond, typename If_True, typename If_False>
using conditional_t = typename std::conditional<Cond, If_True, If_False>::type;

namespace detail {
template <typename... Bs> struct and_c_impl;
template <> struct and_c_impl<> : true_type {};
template <typename B1, typename... Bs>
struct and_c_impl<B1, Bs...> : conditional_t<B1::value, and_c_impl<Bs...>, B1> {
};

template <typename... Bs> struct or_c_impl;
template <> struct or_c_impl<> : false_type {};
template <typename B1, typename... Bs>
struct or_c_impl<B1, Bs...> : conditional_t<B1::value, B1, or_c_impl<Bs...> > {
};

template <typename... Ts> struct void_t_impl { using type = void; };
} // namespace detail

namespace concepts {
template <typename T, typename Enable = void> struct predicate : false_type {};
template <typename T>
struct predicate<        //
    T,                   //
    enable_if_t<         //
        std::is_base_of< //
            typename std::
                integral_constant<typename T::value_type, T::value>, //
            T                                                        //
            >::value                                                 //
        >                                                            //
    > : true_type {};
} // namespace concepts

template <typename... Ts>
using and_c =
    detail::and_c_impl<enable_if_t<concepts::predicate<Ts>::value, Ts>...>;
template <typename... Ts>
using or_c =
    detail::or_c_impl<enable_if_t<concepts::predicate<Ts>::value, Ts>...>;
template <typename... Ts>
using void_t = typename detail::void_t_impl<Ts...>::type;

template <bool Cond, typename T>
using const_if_t = conditional_t<Cond, T const, T>;

template <typename T>
using not_c =
    enable_if_t<concepts::predicate<T>::value, bool_constant<not T::value> >;
} // namespace gu

#endif /* end of include guard META_HPP_XNWUCBGJ */
