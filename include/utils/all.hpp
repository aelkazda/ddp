#ifndef ALL_HPP_DGFYMMSI
#define ALL_HPP_DGFYMMSI

#include <cstdint>
#include <utility>
#include <tuple>

#include "boost/fusion/adapted/std_tuple.hpp"
#include "boost/range/concepts.hpp"
#include "boost/range/combine.hpp"

#include "boost/convert.hpp"
#include "boost/utility/string_view.hpp"

#include "backward.hpp"

#include "gsl-lite/gsl-lite.hpp"
#include "gsl/gsl-lite.hpp"

#include "fmt/format.h"
#include "fmt/ostream.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

#include "utils/macros.hpp"
#include "utils/meta.hpp"

namespace gu {
using isize = std::ptrdiff_t;
using usize = std::size_t;
using u64 = std::uint64_t;
namespace literals {
constexpr auto operator"" _isize(unsigned long long n) -> isize {
  return static_cast<isize>(n);
}
constexpr auto operator"" _usize(unsigned long long n) -> usize {
  return static_cast<usize>(n);
}
} // namespace literals

namespace detail {
namespace adl {
template <std::size_t I, typename T>
auto get(T &&) -> gu::enable_if_t<gu::always_false<T>::value, T> = delete;
} // namespace adl
using adl::get;
template <std::size_t I, typename T>
auto get(T&& object) UTIL_RETURNS_DECLTYPE_AUTO(get<I>(object))
} // namespace detail

#define BIND(...) Z_UTIL_BIND(__VA_ARGS__)

namespace concepts {
template <typename T, typename Enable = void>
struct meta_integral : false_type {};

template <typename T>
struct meta_integral<
    T,
    enable_if_t< //
        std::is_integral<decltype(T::value)>::value and
        std::is_signed<decltype(T::value)>::value and
        std::is_same<decay_t<decltype(T::is_runtime)>, bool>::value //
        > > : true_type {};
} // namespace concepts

template <bool Cond, typename T, typename... Extra>
struct fail_unless { // NOLINT(cppcoreguidelines-special-member-functions)
  static_assert(Cond, "Invalid arguments");
  using type = T;
  ~fail_unless() = delete;
  fail_unless() = delete;
};
template <bool Cond, typename T, typename... Extra>
using fail_unless_t = typename fail_unless<Cond, T, Extra...>::type;

template <typename... Ts> struct invalid;

template <typename... Types, typename... Args>
auto print_types(Args&&...) -> invalid<Types..., Args...>;
template <typename... Ts> void unused(Ts const&...) {}

inline auto unimplemented() -> void {
  std::terminate();
}

struct unsafe_t {};
constexpr unsafe_t unsafe;

// Compile-time/Dynamic integer wrappers
struct dyn {
  isize _val;

  static constexpr bool is_runtime = true;
  static constexpr isize value = -1;

  // Not unsafe. Used for interface compatibility with `fix`
  constexpr explicit dyn(isize n, unsafe_t) noexcept : _val(n) {}

  constexpr explicit dyn(isize n) noexcept : _val(n) {}

  template <typename T, REQUIRES(concepts::meta_integral<T>::value)>
  constexpr explicit dyn(T n) noexcept : _val(n.as_isize()) {}

  constexpr auto as_isize() const noexcept -> isize { return _val; }
  auto as_usize() const noexcept -> usize { return gsl::narrow<usize>(_val); }
  auto as_usize(unsafe_t) const noexcept -> usize {
    return gsl::narrow_cast<usize>(_val);
  }
};

template <isize N> struct fix {
  static constexpr bool is_runtime = false;
  static constexpr isize value = N;

  // Assumes the parameter is equal to N
  explicit fix(isize, unsafe_t) noexcept {}

  explicit fix(isize n) { gsl_Expects(N == n); }
  explicit fix(dyn n) { gsl_Expects(N == n.as_isize()); }
  constexpr fix() noexcept = default;

  constexpr auto as_isize() const noexcept -> isize { return value; }
  constexpr auto as_usize() const noexcept -> usize { return value; }
};

namespace concepts {
// TODO remove
template <typename N, typename M, typename Enable = void>
struct maybe_same : false_type {};
template <typename N, typename M>
struct maybe_same<
    N,
    M,
    SPECIALIZE_IF(                      //
        and_c<                          //
            meta_integral<M>,           //
            meta_integral<N>,           //
            bool_constant<              //
                N::value == M::value or //
                N::is_runtime or        //
                M::is_runtime           //
                >                       //
            >::value                    //
        )                               //
    > : true_type {};

template <typename T, typename Enable = void> struct fixed : false_type {};
template <typename T>
struct fixed<T, SPECIALIZE_IF(meta_integral<T>::value and not T::is_runtime)>
    : true_type {};

template <typename T, typename Enable = void> struct dynamic : false_type {};
template <typename T>
struct dynamic<T, SPECIALIZE_IF(meta_integral<T>::value and T::is_runtime)>
    : true_type {};

template <typename N, typename M, typename Enable = void>
struct equal : false_type {};

template <typename N, typename M>
struct equal<
    N,
    M,
    SPECIALIZE_IF(
        and_c<fixed<N>, fixed<M>, bool_constant<(N::value == M::value)> >::
            value)> : true_type {};

template <typename N, typename M>
using unequal = and_c<fixed<N>, fixed<M>, not_c<equal<N, M> > >;

template <typename N, typename M, typename Enable = void>
struct less_or_eq : false_type {};
template <typename N, typename M>
struct less_or_eq<
    N,
    M,
    SPECIALIZE_IF(
        and_c<fixed<N>, fixed<M>, bool_constant<(N::value <= M::value)> >::
            value)> : true_type {};

template <typename N, typename M>
using greater = and_c<fixed<N>, fixed<M>, not_c<less_or_eq<N, M> > >;

template <
    template <typename, typename, typename...>
    class Predicate,
    typename N,
    typename M>
struct maybe : or_c<Predicate<N, M>, dynamic<N>, dynamic<M> > {};
} // namespace concepts

inline void assert_eq_runtime(isize n, isize m) {
  if (n != m) {
    spdlog::error(
        "Assertion failed:\n"
        "{} not equal to {}\n"
        "----------------------------------------"
        "----------------------------------------",
        n,
        m);
    std::terminate();
  }
}
inline void assert_less_or_equal_runtime(isize n, isize m) {
  if (n > m) {
    spdlog::error(
        "Assertion failed:\n"
        "{} strictly greater than {}\n"
        "----------------------------------------"
        "----------------------------------------",
        n,
        m);
    std::terminate();
  }
}

void compile_time_assertion_fail() = delete;

template <
    typename N,
    typename M, //
    REQUIRES(concepts::greater<N, M>::value)>
void assert_leq(N, M) = delete;

template <
    typename N,
    typename M, //
    REQUIRES(concepts::less_or_eq<N, M>::value)>
void assert_leq(N, M) noexcept {}

template <
    typename N,
    typename M,
    REQUIRES(concepts::dynamic<N>::value or concepts::dynamic<M>::value)>
void assert_leq(N n, M m) {
  assert_less_or_equal_runtime(n.as_isize(), m.as_isize());
}

template <
    typename N,
    typename M, //
    REQUIRES(concepts::unequal<N, M>::value)>
void assert_eq(N, M) = delete;

template <
    typename N,
    typename M, //
    REQUIRES(concepts::equal<N, M>::value)>
void assert_eq(N, M) noexcept {}

template <
    typename N,
    typename M,
    REQUIRES(concepts::dynamic<N>::value or concepts::dynamic<M>::value)>
void assert_eq(N n, M m) { //
  assert_eq_runtime(n.as_isize(), m.as_isize());
}

// Arithmetic ops
#define GENERATE_ARITHMETIC_OP(OpName, Expr)                                   \
  template <typename U, typename V>                                            \
  constexpr auto OpName(const U& a, const V& b) noexcept(noexcept(Expr))       \
      ->decltype((Expr)) {                                                     \
    return (Expr);                                                             \
  }
GENERATE_ARITHMETIC_OP(plus, (a + b))
GENERATE_ARITHMETIC_OP(minus, (a - b))
GENERATE_ARITHMETIC_OP(multiplies, (a * b))
GENERATE_ARITHMETIC_OP(divides, (a / b))
GENERATE_ARITHMETIC_OP(less, (a < b))
GENERATE_ARITHMETIC_OP(greater_eq, (a >= b))
#undef GENERATE_ARITHMETIC_OP

#define GENERATE_META_INT_OP(Op, OpName)                                       \
  template <                                                                   \
      typename N,                                                              \
      typename M,                                                              \
      REQUIRES((concepts::fixed<N>::value and concepts::fixed<M>::value))>     \
  auto Op(N, M) noexcept->fix<OpName(N::value, M::value)> {                    \
    return {};                                                                 \
  }                                                                            \
  template <                                                                   \
      typename N,                                                              \
      typename M,                                                              \
      REQUIRES((concepts::dynamic<N>::value or concepts::dynamic<M>::value))>  \
  auto Op(N n, M m) noexcept->dyn {                                            \
    return dyn{OpName(n.as_isize(), m.as_isize())};                            \
  }

GENERATE_META_INT_OP(operator+, plus)
GENERATE_META_INT_OP(operator-, minus)
GENERATE_META_INT_OP(operator*, multiplies)
#undef GENERATE_META_INT_OP

template <typename L, typename R>
using add_t = decltype(declval<L>() + declval<R>());
template <typename L, typename R>
using sub_t = decltype(declval<L>() - declval<R>());
template <typename L, typename R>
using mul_t = decltype(declval<L>() * declval<R>());
template <typename L, typename R>
using div_t = decltype(declval<L>() / declval<R>());

/*
 * Contains a reference when used on lvalues, or a moved instance when used on
 * rvalues.
 */
template <typename T> struct ref_if_lvalue {
  explicit ref_if_lvalue(T inner) noexcept(noexcept(T{FWD(inner)}))
      : m_inner(FWD(inner)) {}

  auto cref() const noexcept -> cref_t<T> { return m_inner; }
  auto ref() const noexcept -> cref_t<T> { return m_inner; }
  auto ref() noexcept -> T& { return m_inner; }

  T m_inner;
};

template <typename T>
auto wrap_ref(T&& obj) noexcept(noexcept(ref_if_lvalue<T>{FWD(obj)}))
    -> ref_if_lvalue<T> {
  return ref_if_lvalue<T>{FWD(obj)};
}

// rvalue reference to const lvalue reference
template <typename T> auto unmove(T&& arg) noexcept -> cref_t<T> {
  return static_cast<cref_t<T> >(arg);
}

template <typename T> auto as_const(T const& arg) -> T const& {
  return arg;
}

template <typename... Ts> struct monostate_tpl {};
using monostate = monostate_tpl<void>;

using boost::zip_iterator;
// Boost
template <typename... Iters>
auto zip(Iters&&... iters) RETURNS_DECLTYPE_AUTO(
    boost::zip_iterator<std::tuple< //
        enable_if_t<
            always_true< // detect if boost concept is valid
                boost::ForwardIteratorConcept<Iters> //
                >::value,                            //
            Iters                                    //
            >...> >{std::make_tuple(FWD(iters)...)})

namespace ranges {
template <typename... Ranges>
auto zip(Ranges&&... rngs) RETURNS_DECLTYPE_AUTO(
    boost::range::combined_range<std::tuple< //
        enable_if_t<
            always_true< // detect if boost concept is valid
                boost::SinglePassRangeConcept<Ranges> //
                >::value,                             //
            decltype(FWD(rngs).begin())               //
            >...> >{
        std::make_tuple(FWD(rngs).begin()...),
        std::make_tuple(FWD(rngs).end()...)})
} // namespace ranges

namespace detail {
template <typename... Fs> struct overload;
template <typename F> struct overload<F> : F {
  explicit overload(F f) noexcept(noexcept(F{std::move(f)}))
      : F{std::move(f)} {}
  using F::operator();
};

template <typename F, typename... Fs>
struct overload<F, Fs...> : F, overload<Fs...> {
  explicit overload(F f, Fs... fs)
      noexcept(noexcept((F{std::move(f)}, overload<Fs...>{std::move(fs)...})))
      : F{std::move(f)}, overload<Fs...>{std::move(fs)...} {}

  using F::operator();
  using overload<Fs...>::operator();
};
} // namespace detail

template <typename... Fs>
auto overload(Fs... fs)
    UTIL_RETURNS_AUTO(detail::overload<Fs...>(std::move(fs)...))

namespace detail {
template <typename T> struct remove_self;

template <typename Self, typename Return, typename... Args>
struct remove_self<Return (Self::*)(Args...)> {
  using type = Return(Args...);
};
template <typename Self, typename Return, typename... Args>
struct remove_self<Return (Self::*)(Args...) const> {
  using type = Return(Args...);
};
template <typename Self, typename Return, typename... Args>
struct remove_self<Return (Self::*)(Args...) volatile> {
  using type = Return(Args...);
};
template <typename Self, typename Return, typename... Args>
struct remove_self<Return (Self::*)(Args...) const volatile> {
  using type = Return(Args...);
};
} // namespace detail

template <typename F>
auto dyn_function(F&& fn) UTIL_RETURNS_AUTO(             //
    std::function<                                       //
        typename detail::remove_self<                    //
            decltype(&gu::remove_cvref_t<F>::operator()) //
            >::type                                      //
        >                                                //
    {FWD(fn)}                                            //
)

template <typename... Ts>
auto to_array(Ts&&... args) UTIL_RETURNS_AUTO(  //
    std::array<                                 //
        typename std::common_type<Ts...>::type, //
        sizeof...(Ts)                           //
        >                                       //
    {FWD(args)...}                              //
)

} // namespace gu
#endif /* end of include guard ALL_HPP_DGFYMMSI */
