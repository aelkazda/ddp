#ifndef DDP_HPP_PJJ8HEEW
#define DDP_HPP_PJJ8HEEW

#include <iterator>
#include <vector>

#include "boost/optional/optional.hpp"
#include "boost/range/adaptor/reversed.hpp"
#include "boost/preprocessor/variadic/to_seq.hpp"
#include "boost/preprocessor/seq/variadic_seq_to_seq.hpp"
#include "boost/preprocessor/seq/seq.hpp"

#include "general_utilities.hpp"

#include "eigen/matrix.hpp"
#include "eigen/zero.hpp"
#include "eigen/random.hpp"
#include "tensor_seq.hpp"

#include "seq_wrappers.hpp"
#include "traj.hpp"
#include "model.hpp"

namespace ddp {

namespace detail {
template <typename Out> auto add_assign(Out&) -> void {}
template <typename Out, typename T, typename... Ts>
auto add_assign(Out& out, T&& first, Ts&&... rest) -> void {
  out += FWD(first);
  add_assign(out, FWD_PACK(rest));
}

template <typename Out, typename T, typename... Ts>
auto sum_into(Out&& out, T&& first, Ts&&... rest) -> void {
  out = FWD(first);
  add_assign(out, FWD_PACK(rest));
}

template <typename T, typename... Ts>
auto sum(T&& first, Ts&&... rest) -> decltype(np::clone(FWD(first))) {
  auto out = np::clone(FWD(first));
  add_assign(out, FWD_PACK(rest));
  return out;
}
struct no_multiplier_feedback_t {
  struct noop_mult_t {
    struct proxy_t {
      static auto val() -> np::zero_t { return {}; }
      static auto jac() -> np::zero_t { return {}; }
    };
    struct iterator {
      using difference_type = std::ptrdiff_t;
      using value_type = void;
      using pointer = proxy_t*;
      using reference = proxy_t;
      using iterator_category = std::bidirectional_iterator_tag;

      gu::isize i;

      auto operator==(iterator other) const -> bool { return i == other.i; }
      auto operator!=(iterator other) const -> bool { return i != other.i; }
      auto operator++() -> iterator& {
        ++i;
        return *this;
      }
      auto operator--() -> iterator& {
        --i;
        return *this;
      }
      DDP_DEFINE_POSTFIX(iterator);
      auto operator*() const -> proxy_t { return {}; }
      auto operator*() -> proxy_t { return {}; }
    };
    using const_iterator = iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    static auto begin() -> iterator { return {0}; }
    auto end() const -> iterator { return {num_steps}; }
    auto rbegin() const -> reverse_iterator {
      return reverse_iterator{{num_steps}};
    }
    static auto rend() -> reverse_iterator { return reverse_iterator{{0}}; }

    gu::isize num_steps;
  };
  noop_mult_t eq;
  noop_mult_t ineq;
};

} // namespace detail

enum struct method {
  primal,
  primal_dual_constant_multipliers,
  primal_dual_affine_multipliers
};

template <typename Scalar> struct solution_candidate_t {
  np::dyn_vec_t<Scalar> primal;
  np::dyn_vec_t<Scalar> dual;
  Scalar optimality_score;
};

template <typename Scalar> struct iterate_t {
  isize idx;
  solution_candidate_t<Scalar> sol;
  Scalar reg_after_bkw;
  Scalar reg_after_fwd;
  Scalar step;
  Scalar primal_optimality;
  Scalar dual_optimality;
};

template <typename Scalar> struct solver_parameters_t {
  isize max_iterations;
  Scalar optimality_stopping_threshold;
  Scalar mu;
  Scalar reg;
  Scalar w;
  Scalar n;
};

template <
    typename Scalar,
    typename StateIdx,
    typename ControlIdx,
    typename EqIdx,
    typename IneqIdx,
    typename Transition,
    typename Cost,
    typename CostFinal,
    typename EqConstraint,
    typename IneqConstraint>
struct ddp_solver_t {
  using isize = gu::isize;

  using scalar_t = Scalar;
  using state_indexer_t = StateIdx;
  using control_indexer_t = ControlIdx;
  using eq_indexer_t = EqIdx;
  using ineq_indexer_t = IneqIdx;

  using transition_t = Transition;
  using cost_t = Cost;
  using cost_final_t = CostFinal;
  using eq_constraint_t = EqConstraint;
  using ineq_constraint_t = IneqConstraint;

  using trajectory_t = ddp::trajectory_t<scalar_t, StateIdx, ControlIdx>;

  using eq_mult_fn_seq_t = linear_vector_fn_seq_t<scalar_t, EqIdx, StateIdx>;
  using ineq_mult_fn_seq_t =
      linear_vector_fn_seq_t<scalar_t, IneqIdx, StateIdx>;

  using eq_mult_seq_t = vector_seq_t<scalar_t, EqIdx>;
  using ineq_mult_seq_t = vector_seq_t<scalar_t, IneqIdx>;

  struct mult_seq_t {
    eq_mult_seq_t eq;
    ineq_mult_seq_t ineq;
  };
  struct mult_fn_seq_t {
    eq_mult_fn_seq_t eq;
    ineq_mult_fn_seq_t ineq;
  };

  using control_feedback_t =
      linear_vector_fn_seq_t<scalar_t, ControlIdx, StateIdx>;

  using dyn_vec_t = np::mat<scalar_t, gu::dyn::value, 1>;

  template <method M, typename Logger, typename Update_Strategy>
  auto solve(
      Logger&& log,
      Update_Strategy const& update,
      solver_parameters_t<scalar_t> solver_parameters,
      trajectory_t) const -> void;

  template <method M> struct multiplier_sequence_type;
  template <method M> struct multiplier_feedback_sequence_type;

  template <> struct multiplier_sequence_type<method::primal> {
    using type = mult_seq_t;
  };
  template <>
  struct multiplier_sequence_type<method::primal_dual_constant_multipliers> {
    using type = mult_seq_t;
  };
  template <>
  struct multiplier_sequence_type<method::primal_dual_affine_multipliers> {
    using type = mult_fn_seq_t;
  };

  template <> struct multiplier_feedback_sequence_type<method::primal> {
    using type = detail::no_multiplier_feedback_t;
  };
  template <>
  struct multiplier_feedback_sequence_type<
      method::primal_dual_constant_multipliers> {
    using type = mult_fn_seq_t;
  };
  template <>
  struct multiplier_feedback_sequence_type<
      method::primal_dual_affine_multipliers> {
    using type = mult_fn_seq_t;
  };

  template <method M> struct backward_pass_result_t {
    control_feedback_t feedback;
    typename multiplier_feedback_sequence_type<M>::type //
        mults;
    scalar_t mu;
    scalar_t regularization;
  };

  template <method M> struct forward_pass_result_t {
    trajectory_t new_traj;
    typename multiplier_sequence_type<M>::type mults;
    scalar_t step;
  };

  template <method M>
  auto zero_multipliers() const -> gu::enable_if_t<
      M == method::primal or M == method::primal_dual_constant_multipliers,
      typename multiplier_sequence_type<M>::type> {
    return zero_multipliers_constant();
  }
  template <method M>
  auto zero_multipliers() const -> gu::enable_if_t<
      M == method::primal_dual_affine_multipliers,
      typename multiplier_sequence_type<M>::type> {
    return zero_multipliers_affine();
  }

  template <method M>
  auto zero_feedback_multipliers() const -> gu::enable_if_t<
      M == method::primal,
      typename multiplier_feedback_sequence_type<M>::type> {
    return detail::no_multiplier_feedback_t{{u_idx.n_steps}, {u_idx.n_steps}};
  }
  template <method M>
  auto zero_feedback_multipliers() const -> gu::enable_if_t<
      (M == method::primal_dual_constant_multipliers or
       M == method::primal_dual_affine_multipliers),
      typename multiplier_feedback_sequence_type<M>::type> {
    return zero_multipliers_affine();
  }

  template <typename ControlSequence>
  auto make_trajectory(ControlSequence const& u_new) const -> trajectory_t {
    // uninitializedialized trajectory_t
    trajectory_t traj(x_idx, u_idx, false);

    traj.x_0() = x_init;
    for (auto cur : traj) {
      isize t = cur.time_idx();
      update_at_pos(cur, u_new(t, cur.x()));
    }
    return traj;
  }

  auto horizon() const -> isize {
    using gu::dyn;
    isize n = x_idx.n_steps - 1;
    gu::assert_eq_runtime(n, u_idx.n_steps);
    gu::assert_eq_runtime(n, eq_idx.n_steps);
    gu::assert_eq_runtime(n, ineq_idx.n_steps);
    return n;
  }

  auto zero_multipliers_constant() const -> mult_seq_t {
    auto multipliers = mult_seq_t //
        {eq_mult_seq_t(eq_idx), ineq_mult_seq_t(ineq_idx)};

    for (auto elems : gu::ranges::zip(multipliers.eq, multipliers.ineq)) {
      BIND(auto&&, (eq, ineq), elems);
      np::fill_constant(eq.val(), 0);
      np::fill_constant(ineq.val(), 0);
    }
    return multipliers;
  }

  auto zero_multipliers_affine() const -> mult_fn_seq_t {
    auto multipliers = mult_fn_seq_t //
        {eq_mult_fn_seq_t(eq_idx, x_idx_truncated),
         ineq_mult_fn_seq_t(ineq_idx, x_idx_truncated)};

    for (auto elems : gu::ranges::zip(multipliers.eq, multipliers.ineq)) {
      BIND(auto&&, (eq, ineq), elems);
      np::fill_constant(eq.val(), 0);
      np::fill_constant(eq.jac(), 0);
      np::fill_constant(ineq.val(), 0);
      np::fill_constant(ineq.jac(), 0);
    }
    return multipliers;
  }

  template <typename Control>
  void update_at_pos(
      typename trajectory_t::template proxy_t<false> p,
      const Control& u_new) const {
    isize t = p.time_idx();
    p.u() = u_new;
    p.x_next() = f_seq(t)(p.as_const().x(), p.as_const().u());
  }

  template <typename Idx> //
  using mat_seq_t = detail::mat_seq_t<scalar_t, Idx>;
  template <typename L, typename R>
  using prod_t = mat_seq_t<detail::outer_prod_indexer_t<L, R> >;

  using one_idx_t = detail::regular_indexer_t<np::fix<1>, np::fix<1> >;
  using zero_generator_t = detail::mat_seq_t<np::zero_t, one_idx_t>;
  template <typename L, typename R, typename O>
  using tensor_seq_t = // zero_generator_t;
      ::ddp::detail::tensor_seq_t<scalar_t, detail::tensor_indexer_t<L, R, O> >;

  struct derivative_storage_t {
    static constexpr isize xf_dim =
        decltype(std::declval<trajectory_t>().x_f().rows())::value;

    np::mat<scalar_t, 1, xf_dim> lfx;
    np::mat<scalar_t, xf_dim, xf_dim> lfxx;

    prod_t<one_idx_t, StateIdx> lx;
    prod_t<one_idx_t, ControlIdx> lu;
    prod_t<StateIdx, StateIdx> lxx;
    prod_t<ControlIdx, StateIdx> lux;
    prod_t<ControlIdx, ControlIdx> luu;

    prod_t<StateIdx, StateIdx> fx;
    prod_t<StateIdx, ControlIdx> fu;
    tensor_seq_t<StateIdx, StateIdx, StateIdx> fxx;
    tensor_seq_t<ControlIdx, StateIdx, StateIdx> fux;
    tensor_seq_t<ControlIdx, ControlIdx, StateIdx> fuu;

    mat_seq_t<EqIdx> eq_val;
    prod_t<EqIdx, StateIdx> eq_x;
    prod_t<EqIdx, ControlIdx> eq_u;
    tensor_seq_t<StateIdx, StateIdx, EqIdx> eq_xx;
    tensor_seq_t<ControlIdx, StateIdx, EqIdx> eq_ux;
    tensor_seq_t<ControlIdx, ControlIdx, EqIdx> eq_uu;

    mat_seq_t<IneqIdx> ineq_val;
    prod_t<IneqIdx, StateIdx> ineq_x;
    prod_t<IneqIdx, ControlIdx> ineq_u;
    tensor_seq_t<StateIdx, StateIdx, IneqIdx> ineq_xx;
    tensor_seq_t<ControlIdx, StateIdx, IneqIdx> ineq_ux;
    tensor_seq_t<ControlIdx, ControlIdx, IneqIdx> ineq_uu;

    template <typename T> using cview_t = typename T::cview_t;

#define DDP_HEAD(First, ...) First
#define DDP_TAIL(First, ...) __VA_ARGS__

#define DDP_PROXY_MEMBER(r, _, Elem) cview_t<DDP_TAIL Elem> DDP_HEAD Elem;
#define DDP_ITERATOR_MEMBER(r, _, Elem)                                        \
  typename DDP_TAIL Elem ::const_iterator DDP_HEAD Elem;

#define DDP_AND_EQUALITY(r, _, Elem) and DDP_HEAD Elem == other.DDP_HEAD Elem
#define DDP_PREFIX(r, Prefix, Elem) Prefix DDP_HEAD Elem;
#define DDP_COMMA_PREFIX(r, Prefix, Elem) , Prefix DDP_HEAD Elem
#define DDP_COMMA_CATPREFIX_SUFFIX(r, Prefix_Suffix, Elem)                     \
  , BOOST_PP_CAT(DDP_HEAD Prefix_Suffix, DDP_HEAD Elem) DDP_TAIL Prefix_Suffix
#define DDP_IDENTITY(X) X

#define DDP_DEFINE_ACCESSOR(Accessor_Name, Parent_Access_Prefix, Members_Seq)  \
  Z_DDP_DEFINE_ACCESSOR(                                                       \
      Accessor_Name,                                                           \
      Parent_Access_Prefix,                                                    \
      BOOST_PP_VARIADIC_SEQ_TO_SEQ(Members_Seq))

#define Z_DDP_DEFINE_ACCESSOR(                                                 \
    Accessor_Name, Parent_Access_Prefix, Members_Seq)                          \
  struct Accessor_Name {                                                       \
    derivative_storage_t const& parent;                                        \
    struct proxy {                                                             \
      BOOST_PP_SEQ_FOR_EACH(DDP_PROXY_MEMBER, _, Members_Seq)                  \
    };                                                                         \
    struct iterator {                                                          \
      using difference_type = std::ptrdiff_t;                                  \
      using value_type = void;                                                 \
      using pointer = proxy*;                                                  \
      using reference = proxy;                                                 \
      using iterator_category = std::bidirectional_iterator_tag;               \
      BOOST_PP_SEQ_FOR_EACH(DDP_ITERATOR_MEMBER, _, Members_Seq)               \
      auto operator==(iterator const& other) const -> bool {                   \
        return true BOOST_PP_SEQ_FOR_EACH(DDP_AND_EQUALITY, _, Members_Seq);   \
      }                                                                        \
      auto operator!=(iterator const& other) const -> bool {                   \
        return not(*this == other);                                            \
      }                                                                        \
                                                                               \
      auto operator++() /**/                                                   \
          -> iterator& {                                                       \
        BOOST_PP_SEQ_FOR_EACH(DDP_PREFIX, ++, Members_Seq)                     \
        return *this;                                                          \
      }                                                                        \
      auto operator--() /**/                                                   \
          -> iterator& {                                                       \
        BOOST_PP_SEQ_FOR_EACH(DDP_PREFIX, --, Members_Seq)                     \
        return *this;                                                          \
      }                                                                        \
      auto operator++(int) -> iterator {                                       \
        iterator cur = *this;                                                  \
        ++(*this);                                                             \
        return cur;                                                            \
      }                                                                        \
      auto operator--(int) -> iterator {                                       \
        iterator cur = *this;                                                  \
        --(*this);                                                             \
        return cur;                                                            \
      }                                                                        \
      auto operator*() const -> proxy {                                        \
        return {                                                               \
            **BOOST_PP_IDENTITY(DDP_HEAD BOOST_PP_SEQ_HEAD(Members_Seq))()     \
                 BOOST_PP_SEQ_FOR_EACH(                                        \
                     DDP_COMMA_PREFIX, **, BOOST_PP_SEQ_TAIL(Members_Seq))};   \
      }                                                                        \
    };                                                                         \
    using reverse_iterator = std::reverse_iterator<iterator>;                  \
    auto begin() const -> iterator {                                           \
      return {                                                                 \
          BOOST_PP_CAT(                                                        \
              parent.Parent_Access_Prefix,                                     \
              BOOST_PP_IDENTITY(DDP_HEAD BOOST_PP_SEQ_HEAD(Members_Seq))())    \
              .begin() BOOST_PP_SEQ_FOR_EACH(                                  \
                  DDP_COMMA_CATPREFIX_SUFFIX,                                  \
                  (parent.Parent_Access_Prefix, .begin()),                     \
                  BOOST_PP_SEQ_TAIL(Members_Seq))};                            \
    }                                                                          \
    auto end() const -> iterator {                                             \
      return {                                                                 \
          BOOST_PP_CAT(                                                        \
              parent.Parent_Access_Prefix,                                     \
              BOOST_PP_IDENTITY(DDP_HEAD BOOST_PP_SEQ_HEAD(Members_Seq))())    \
              .end() BOOST_PP_SEQ_FOR_EACH(                                    \
                  DDP_COMMA_CATPREFIX_SUFFIX,                                  \
                  (parent.Parent_Access_Prefix, .end()),                       \
                  BOOST_PP_SEQ_TAIL(Members_Seq))};                            \
    }                                                                          \
    auto rbegin() const -> iterator {                                          \
      return {                                                                 \
          BOOST_PP_CAT(                                                        \
              parent.Parent_Access_Prefix,                                     \
              BOOST_PP_IDENTITY(DDP_HEAD BOOST_PP_SEQ_HEAD(Members_Seq))())    \
              .rbegin() BOOST_PP_SEQ_FOR_EACH(                                 \
                  DDP_COMMA_CATPREFIX_SUFFIX,                                  \
                  (parent.Parent_Access_Prefix, .rbegin()),                    \
                  BOOST_PP_SEQ_TAIL(Members_Seq))};                            \
    }                                                                          \
    auto rend() const -> iterator {                                            \
      return {                                                                 \
          BOOST_PP_CAT(                                                        \
              parent.Parent_Access_Prefix,                                     \
              BOOST_PP_IDENTITY(DDP_HEAD BOOST_PP_SEQ_HEAD(Members_Seq))())    \
              .rend() BOOST_PP_SEQ_FOR_EACH(                                   \
                  DDP_COMMA_CATPREFIX_SUFFIX,                                  \
                  (parent.Parent_Access_Prefix, .rend()),                      \
                  BOOST_PP_SEQ_TAIL(Members_Seq))};                            \
    }                                                                          \
  };
    DDP_DEFINE_ACCESSOR(
        costs_t,
        l,
        (x, prod_t<one_idx_t, StateIdx>)     //
        (u, prod_t<one_idx_t, ControlIdx>)   //
        (xx, prod_t<StateIdx, StateIdx>)     //
        (ux, prod_t<ControlIdx, StateIdx>)   //
        (uu, prod_t<ControlIdx, ControlIdx>) //
    )
    DDP_DEFINE_ACCESSOR(
        transition_t,
        f,
        (x, prod_t<StateIdx, StateIdx>)                      //
        (u, prod_t<StateIdx, ControlIdx>)                    //
        (xx, tensor_seq_t<StateIdx, StateIdx, StateIdx>)     //
        (ux, tensor_seq_t<ControlIdx, StateIdx, StateIdx>)   //
        (uu, tensor_seq_t<ControlIdx, ControlIdx, StateIdx>) //
    )
    DDP_DEFINE_ACCESSOR(
        eq_t,
        eq_,
        (val, mat_seq_t<EqIdx>)                           //
        (x, prod_t<EqIdx, StateIdx>)                      //
        (u, prod_t<EqIdx, ControlIdx>)                    //
        (xx, tensor_seq_t<StateIdx, StateIdx, EqIdx>)     //
        (ux, tensor_seq_t<ControlIdx, StateIdx, EqIdx>)   //
        (uu, tensor_seq_t<ControlIdx, ControlIdx, EqIdx>) //
    )
    DDP_DEFINE_ACCESSOR(
        ineq_t,
        ineq_,
        (val, mat_seq_t<IneqIdx>)                           //
        (x, prod_t<IneqIdx, StateIdx>)                      //
        (u, prod_t<IneqIdx, ControlIdx>)                    //
        (xx, tensor_seq_t<StateIdx, StateIdx, IneqIdx>)     //
        (ux, tensor_seq_t<ControlIdx, StateIdx, IneqIdx>)   //
        (uu, tensor_seq_t<ControlIdx, ControlIdx, IneqIdx>) //
    )
#undef DDP_HEAD
#undef DDP_TAIL
#undef DDP_PROXY_MEMBER
#undef DDP_ITERATOR_MEMBER
#undef DDP_AND_EQUALITY
#undef DDP_PREFIX
#undef DDP_COMMA_PREFIX
#undef DDP_COMMA_CATPREFIX_SUFFIX
#undef DDP_DEFINE_ACCESSOR
#undef Z_DDP_DEFINE_ACCESSOR

    auto l() const -> costs_t { return {*this}; }
    auto f() const -> transition_t { return {*this}; }
    auto eq() const -> eq_t { return {*this}; }
    auto ineq() const -> ineq_t { return {*this}; }
  };

  auto uninit_derivative_storage(trajectory_t const& traj) const
      -> derivative_storage_t {

    auto one_idx = [this] {
      return vec_regular_indexer(x_idx_truncated.n_steps, gu::fix<1>{});
    };

    auto storage = derivative_storage_t{
        // Final cost
        np::uninitialized<scalar_t>::row(traj.x_f().rows()), //
        np::uninitialized<scalar_t>::mat(
            traj.x_f().rows(), traj.x_f().rows()), //
        // Cost
        prod_t<one_idx_t, StateIdx>{
            outer_prod(one_idx(), StateIdx{x_idx_truncated})}, //
        prod_t<one_idx_t, ControlIdx>{
            outer_prod(one_idx(), ControlIdx{u_idx})}, //
        prod_t<StateIdx, StateIdx>{
            outer_prod(x_idx_truncated, x_idx_truncated)},                //
        prod_t<ControlIdx, StateIdx>{outer_prod(u_idx, x_idx_truncated)}, //
        prod_t<ControlIdx, ControlIdx>{outer_prod(u_idx, u_idx)},         //
        // Transition function
        prod_t<StateIdx, StateIdx>{
            outer_prod(x_idx_truncated, x_idx_truncated)},
        prod_t<StateIdx, ControlIdx>{outer_prod(x_idx_truncated, u_idx)}, //
        tensor_seq<scalar_t>(x_idx_truncated, x_idx_truncated, x_idx_truncated),
        tensor_seq<scalar_t>(u_idx, x_idx_truncated, x_idx_truncated),
        tensor_seq<scalar_t>(u_idx, u_idx, x_idx_truncated),
        // Equality constraints
        mat_seq_t<EqIdx>{EqIdx{eq_idx}},                              //
        prod_t<EqIdx, StateIdx>{outer_prod(eq_idx, x_idx_truncated)}, //
        prod_t<EqIdx, ControlIdx>{outer_prod(eq_idx, u_idx)},         //
        tensor_seq<scalar_t>(x_idx_truncated, x_idx_truncated, eq_idx),
        tensor_seq<scalar_t>(u_idx, x_idx_truncated, eq_idx),
        tensor_seq<scalar_t>(u_idx, u_idx, eq_idx),
        // Inequality constraints
        mat_seq_t<IneqIdx>{IneqIdx{ineq_idx}},                            //
        prod_t<IneqIdx, StateIdx>{outer_prod(ineq_idx, x_idx_truncated)}, //
        prod_t<IneqIdx, ControlIdx>{outer_prod(ineq_idx, u_idx)},         //
        tensor_seq<scalar_t>(x_idx_truncated, x_idx_truncated, ineq_idx),
        tensor_seq<scalar_t>(u_idx, x_idx_truncated, ineq_idx),
        tensor_seq<scalar_t>(u_idx, u_idx, ineq_idx)};

    // clang-format off
    for (auto elems : gu::ranges::zip(
             storage.lx, storage.lu, storage.lxx, storage.lux, storage.luu,
             storage.fx, storage.fu, storage.fxx, storage.fux, storage.fuu,
             storage.eq_val, storage.eq_x, storage.eq_u,
             storage.eq_xx, storage.eq_ux, storage.eq_uu,
             storage.ineq_val, storage.ineq_x, storage.ineq_u,
             storage.ineq_xx, storage.ineq_ux, storage.ineq_uu
             )) {
      BIND(auto&&, (
             lx, lu, lxx, lux, luu,
             fx, fu, fxx, fux, fuu,
             eq_val, eq_x, eq_u, eq_xx, eq_ux, eq_uu,
             ineq_val, ineq_x, ineq_u, ineq_xx, ineq_ux, ineq_uu
           ),
          elems);
      gu::unused(
          lux,
          fxx, fux, fuu,
          eq_val, eq_xx, eq_ux, eq_uu,
          ineq_val, ineq_xx, ineq_ux, ineq_uu);
      // clang-format on

      auto nan = std::numeric_limits<scalar_t>::quiet_NaN();
      np::fill_constant(*lx, nan);
      np::fill_constant(*lu, nan);
      np::fill_constant(*lxx, nan);
      np::fill_constant(*lux, nan);
      np::fill_constant(*luu, nan);

      np::fill_constant(*fx, nan);
      np::fill_constant(*fu, nan);
      np::fill_constant(*fxx, nan);
      np::fill_constant(*fux, nan);
      np::fill_constant(*fuu, nan);

      np::fill_constant(*eq_val, nan);
      np::fill_constant(*eq_x, nan);
      np::fill_constant(*eq_u, nan);
      // *eq_xx = eq.xx;
      // *eq_ux = eq.ux;
      // *eq_uu = eq.uu;

      np::fill_constant(*ineq_val, nan);
      np::fill_constant(*ineq_x, nan);
      np::fill_constant(*ineq_u, nan);
      // *ineq_xx = ineq.xx;
      // *ineq_ux = ineq.ux;
      // *ineq_uu = ineq.uu;
    }

    return storage;
  }

  auto compute_derivatives(
      trajectory_t const& traj, derivative_storage_t& storage) const -> void {
    auto const x_f = traj.x_f();
    auto const l_derivs = l_f.derivs_2(x_f);
    storage.lfx = l_derivs.x;
    storage.lfxx = l_derivs.xx;
    // clang-format off
    for (auto elems : gu::ranges::zip(
             traj,
             storage.lx, storage.lu, storage.lxx, storage.lux, storage.luu,
             storage.fx, storage.fu, storage.fxx, storage.fux, storage.fuu,
             storage.eq_val, storage.eq_x, storage.eq_u,
             storage.eq_xx, storage.eq_ux, storage.eq_uu,
             storage.ineq_val, storage.ineq_x, storage.ineq_u,
             storage.ineq_xx, storage.ineq_ux, storage.ineq_uu
             )) {
      BIND(auto&&, (
             xu,
             lx, lu, lxx, lux, luu,
             fx, fu, fxx, fux, fuu,
             eq_val, eq_x, eq_u, eq_xx, eq_ux, eq_uu,
             ineq_val, ineq_x, ineq_u, ineq_xx, ineq_ux, ineq_uu
           ),
          elems);
      // clang-format on
      isize t = xu.time_idx();
      auto const x = eval(xu.x());
      auto const u = eval(xu.u());
      auto const l = l_seq(t).derivs_2(x, u);
      auto const f = f_seq(t).derivs_2(x, u);
      auto const eq = eq_seq(t).derivs_2(x, u);
      auto const ineq = ineq_seq(t).derivs_2(x, u);

      *lx = l.x;
      *lu = l.u;
      *lxx = l.xx;
      *lux = l.ux;
      *luu = l.uu;

      *fx = f.x;
      *fu = f.u;
      *fxx = f.xx;
      *fux = f.ux;
      *fuu = f.uu;

      *eq_val = eq.val;
      *eq_x = eq.x;
      *eq_u = eq.u;
      *eq_xx = eq.xx;
      *eq_ux = eq.ux;
      *eq_uu = eq.uu;

      *ineq_val = ineq.val;
      *ineq_x = ineq.x;
      *ineq_u = ineq.u;
      *ineq_xx = ineq.xx;
      *ineq_ux = ineq.ux;
      *ineq_uu = ineq.uu;
    }
  }

  auto d_traj(
      control_feedback_t const& fb,
      derivative_storage_t const& derivatives) const -> trajectory_t {
    trajectory_t dir(x_idx, u_idx, false);
    np::fill_constant(dir.x_0(), 0);
    for (auto cur : gu::ranges::zip(dir, fb, derivatives.f())) {
      BIND(auto&&, (traj_cur, fb_cur, f), cur);
      auto&& du = traj_cur.u();
      auto&& dx = traj_cur.x();
      auto&& dxn = traj_cur.x_next();

      auto const& k = fb_cur().val();
      auto const& K = fb_cur().jac();

      du = k + K * dx;
      dxn = f.x * dx + f.u * du;
    }
    return dir;
  }

  auto d_cost(trajectory_t const& d_xu, derivative_storage_t const& deriv) const
      -> scalar_t {
    scalar_t retval = 0;
    for (auto cur : gu::ranges::zip(d_xu, deriv.l())) {
      BIND(auto const&, (traj_cur, l), cur);
      auto const& du = traj_cur.u();
      auto const& dx = traj_cur.x();
      retval += l.x * dx + l.u * du;
    }
    return retval;
  }

  auto d_constr_lag(
      trajectory_t const& traj,
      trajectory_t const& d_xu,
      mult_seq_t const& mults,
      derivative_storage_t const& deriv) const -> scalar_t {

    gu::unused(traj);

    scalar_t retval = 0;
    for (auto cur : gu::ranges::zip(d_xu, mults.eq, deriv.eq())) {
      BIND(auto const&, (d_xu_cur, mults_eq_cur, eq), cur);
      auto const& du = d_xu_cur.u();
      auto const& dx = d_xu_cur.x();
      auto const& pe = mults_eq_cur.val();

      retval += pe.T() * (eq.x * dx + eq.u * du);
    }
    return retval;
  }

  auto d_constr_lag(
      trajectory_t const& traj,
      trajectory_t const& d_xu,
      mult_fn_seq_t const& mults,
      derivative_storage_t const& deriv) const -> scalar_t {
    scalar_t retval = 0;
    for (auto cur : gu::ranges::zip(traj, d_xu, mults.eq, deriv.eq())) {
      BIND(auto const&, (traj_cur, d_xu_cur, mults_eq_cur, eq), cur);
      auto const& x = traj.x();
      auto const& du = d_xu_cur.u();
      auto const& dx = d_xu_cur.x();
      auto const pe = mults_eq_cur(x);
      auto const pe_x = mults_eq_cur.jac();

      retval += np::value_from_1x1(
          pe.T() * (eq.x * dx + eq.u * du) + (pe_x * dx).T() * eq.val);
    }
    return retval;
  }

  auto d_constr_norm2(
      trajectory_t const& d_xu, derivative_storage_t const& deriv) const
      -> scalar_t {

    scalar_t retval = 0;
    for (auto cur : gu::ranges::zip(d_xu, deriv.eq())) {
      BIND(auto const&, (d_xu_cur, eq), cur);
      auto const& du = d_xu_cur.u();
      auto const& dx = d_xu_cur.x();

      retval += np::value_from_1x1(eq.val * (eq.x * dx + eq.u * du));
    }
    return retval;
  }

  template <typename Mult>
  auto d_cost_aug(
      trajectory_t const& traj,
      trajectory_t const& d_xu,
      Mult const& mults,
      scalar_t const& mu,
      derivative_storage_t const& deriv) -> scalar_t {
    return d_constr_lag(traj, d_xu, mults, deriv) +
           mu * d_constr_norm2(d_xu, deriv);
  }

  auto optimality_constr(derivative_storage_t const& derivs) const -> scalar_t {
    using std::max;
    scalar_t retval = 0;
    for (auto const& eq : derivs.eq()) {
      retval = max(retval, np::stable_norm(eq.val));
    }
    return retval;
  }

  template <typename Mult_Seq>
  auto optimality_lag(
      trajectory_t const& traj,
      Mult_Seq const& mults,
      derivative_storage_t const& derivs) const -> scalar_t {

    using boost::adaptors::reversed;
    using std::max;

    scalar_t retval = 0;
    auto adj = np::eval(derivs.lfx);

    for (auto cur :
         gu::ranges::zip(
             traj, mults.eq, mults.ineq, derivs.l(), derivs.f(), derivs.eq()) |
             reversed) {
      BIND(auto const&, (xu, p_eq_, p_ineq_, l, f, eq), cur);

      gu::unused(p_ineq_); // FIXME

      auto p_eq = np::eval(p_eq_(xu.x()));
      retval = max(                 //
          retval,                   //
          np::stable_norm(          //
              l.u + p_eq.T() * eq.u //
              + adj * f.u           //
              )                     //
      );

      adj = adj * f.x + l.x   //
            + p_eq.T() * eq.x //
            + eq.val.T() * p_eq_.jac();
    }
    return retval;
  }

  template <typename Mult_Seq>
  auto optimality_obj(
      trajectory_t const& traj,
      Mult_Seq const& mults,
      scalar_t const& mu,
      derivative_storage_t const& derivs) const -> scalar_t {
    using std::max;

    scalar_t retval = 0;
    auto adj = np::eval(derivs.lfx);

    for (auto cur :
         gu::ranges::zip(
             traj, mults.eq, mults.ineq, derivs.l(), derivs.f(), derivs.eq()) |
             boost::adaptors::reversed) {
      BIND(auto const&, (xu, p_eq_, p_ineq_, l, f, eq), cur);

      gu::unused(p_ineq_); // FIXME

      auto p_eq = np::eval(p_eq_(xu.x()));
      retval = max(                                 //
          retval,                                   //
          np::stable_norm(                          //
              l.u + (p_eq + mu * eq.val).T() * eq.u //
              + adj * f.u                           //
              )                                     //
      );

      adj = adj * f.x + l.x                   //
            + (p_eq + mu * eq.val).T() * eq.x //
            + eq.val.T() * p_eq_.jac();
    }
    return retval;
  }

  auto cost_seq(const trajectory_t& traj) const -> dyn_vec_t {
    using np::value_from_1x1;
    auto costs = np::uninitialized<scalar_t>::col(gu::dyn{horizon() + 1});
    for (auto xu : traj) {
      isize t = xu.time_idx();
      auto const x = xu.x();
      auto const u = xu.u();
      costs.eigen()(t) = value_from_1x1(l_seq(t)(x, u));
    }
    auto const x = traj.x_f();
    costs.eigen()(horizon()) = value_from_1x1(l_f(x));
    return costs;
  }

  template <typename Mults>
  auto cost_seq_aug(            //
      trajectory_t const& traj, //
      Mults const& mults,       //
      scalar_t const& mu        //
  ) const -> dyn_vec_t {

    using np::value_from_1x1;
    auto costs = np::uninitialized<scalar_t>::col(gu::dyn{horizon() + 1});

    auto it_traj = traj.begin();
    auto it_eq = mults.eq.begin();
    auto it_ineq = mults.ineq.begin();
    auto end = traj.end();

    while (it_traj != end) {
      using np::norm2;

      isize t = (*it_traj).time_idx();

      auto const x = (*it_traj).x();
      auto const u = (*it_traj).u();
      auto const l = eval(l_seq(t)(x, u));
      auto const ce = eval(eq_seq(t)(x, u));
      auto const ci = eval(ineq_seq(t)(x, u));
      auto const pe = (*it_eq)(x);
      auto const pi = (*it_ineq)(x);

      // FIXME: Unused for now
      gu::unused(ci);
      gu::unused(pi);

      costs.eigen()(t) = value_from_1x1 //
                         (l + pe.T() * ce) +
                         (mu / 2) * norm2(ce);

      ++it_traj;
      ++it_eq;
      ++it_ineq;
    }
    auto const x = traj.x_f();
    costs.eigen()(horizon()) = value_from_1x1(l_f(x, np::zero_t{}));
    return costs;
  }

  template <method M>
  auto backward_pass(                                          //
      trajectory_t const& traj,                                //
      typename multiplier_sequence_type<M>::type const& mults, //
      scalar_t reg,                                            //
      scalar_t mu,                                             //
      derivative_storage_t const& d                            //
  ) const                                                      //
      -> backward_pass_result_t<M>;

  template <method M>
  auto forward_pass(                                               //
      trajectory_t const& old_traj,                                //
      typename multiplier_sequence_type<M>::type const& old_mults, //
      backward_pass_result_t<M> const& bp_res,                     //
      bool do_linesearch = true                                    //
  ) const -> forward_pass_result_t<M>;

  typename trajectory_t::vec_x_t x_init;

  StateIdx x_idx;
  ControlIdx u_idx;
  EqIdx eq_idx;
  IneqIdx ineq_idx;

  Transition f_seq;
  Cost l_seq;
  CostFinal l_f;
  EqConstraint eq_seq;
  IneqConstraint ineq_seq;

  // TODO: ref wrapper to x_idx
  StateIdx x_idx_truncated;
}; // namespace ddp

template <typename Scalar, typename Vec_X, typename StateIdx, typename... Args>
auto make_ddp_solver(Vec_X const& x_init, StateIdx x_idx, Args... args)
    -> ddp_solver_t<Scalar, StateIdx, Args...> {
  auto trunc_x = x_idx;
  --(trunc_x.n_steps);
  return {x_init.clone(), x_idx, FWD_PACK(args), trunc_x};
}
} // namespace ddp

#endif /* end of include guard: INCLUDE_DDP_HPP_PJJ8HEEW */
