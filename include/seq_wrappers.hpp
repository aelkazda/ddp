#ifndef LINEAR_MODEL_SEQ_HPP_QUPNBESD
#define LINEAR_MODEL_SEQ_HPP_QUPNBESD

#include "eigen/forward_declarations.hpp"
#include "eigen/matrix.hpp"
#include "mat_seq.hpp"
#include "traj.hpp"

namespace ddp {

template <typename Scalar, typename OutIdx, typename InIdx>
struct linear_vector_fn_seq_t {
  using isize = gu::isize;

  using val_indexer_t = OutIdx;
  using jac_indexer_t = detail::outer_prod_indexer_t<OutIdx, InIdx>;

  using val_mat_seq_t = detail::mat_seq_t<Scalar, val_indexer_t>;
  using jac_mat_seq_t = detail::mat_seq_t<Scalar, jac_indexer_t>;
  val_mat_seq_t m_val_data;
  jac_mat_seq_t m_jac_data;

  ~linear_vector_fn_seq_t() = default;

  linear_vector_fn_seq_t(OutIdx out_idx, InIdx in_idx)
      : m_val_data{val_indexer_t{out_idx}},
        m_jac_data{outer_prod(std::move(out_idx), std::move(in_idx))} {}

private:
  linear_vector_fn_seq_t(val_mat_seq_t val_data, jac_mat_seq_t jac_data)
      : m_val_data{std::move(val_data)}, m_jac_data{std::move(jac_data)} {}

public:
  linear_vector_fn_seq_t(linear_vector_fn_seq_t const&) = delete;
  linear_vector_fn_seq_t(linear_vector_fn_seq_t&&) noexcept = default;
  auto operator=(linear_vector_fn_seq_t const&)
      -> linear_vector_fn_seq_t& = delete;
  auto operator=(linear_vector_fn_seq_t&&) noexcept
      -> linear_vector_fn_seq_t& = default;

  template <bool IsConst> struct proxy_t {
    typename val_mat_seq_t::template proxy_t<IsConst> val_proxy;
    typename jac_mat_seq_t::template proxy_t<IsConst> jac_proxy;

    using val_t = typename val_mat_seq_t::matrix_t;
    using jac_t = typename jac_mat_seq_t::matrix_t;

    template <typename T>
    using propagate_const_view = np::mat_view<gu::const_if_t<IsConst, T> >;
    template <typename T> using const_view = np::mat_view<const T>;

    auto val() const -> const_view<val_t> { return val_proxy.get(); }
    auto jac() const -> const_view<jac_t> { return jac_proxy.get(); }
    auto val() -> propagate_const_view<val_t> { return val_proxy.get(); }
    auto jac() -> propagate_const_view<jac_t> { return jac_proxy.get(); }

    template <typename T>
    auto operator()(const T& input) const RETURNS_AUTO(jac() * input + val())
    struct derivs {
      val_t val;
      const_view<jac_t> jac;
    };
    template <typename T> auto derivs_1(const T& input) const -> derivs {
      return {val_t{this->operator()(input)}, jac()};
    }
  };

  template <bool IsConst> struct iterator_impl_t {
    typename val_mat_seq_t::template iterator_impl_t<IsConst> val_iter;
    typename jac_mat_seq_t::template iterator_impl_t<IsConst> jac_iter;

    using proxy_t = linear_vector_fn_seq_t::proxy_t<IsConst>;

    using difference_type = std::ptrdiff_t;
    using value_type = void;
    using pointer = proxy_t*;
    using reference = proxy_t;
    using iterator_category = std::bidirectional_iterator_tag;

    auto operator==(iterator_impl_t other) const -> bool {
      return val_iter == other.val_iter and jac_iter == other.jac_iter;
    }
    auto operator!=(iterator_impl_t other) const -> bool {
      return (*this != other);
    }
    auto operator++() -> iterator_impl_t& {
      ++val_iter;
      ++jac_iter;
      return *this;
    }
    auto operator--() -> iterator_impl_t& {
      --val_iter;
      --jac_iter;
      return *this;
    }
    DDP_DEFINE_POSTFIX(iterator_impl_t);
    auto operator*() const -> proxy_t { return {*val_iter, *jac_iter}; }
    auto operator*() -> proxy_t { return {*val_iter, *jac_iter}; }
  };

  using iterator = iterator_impl_t<false>;
  using const_iterator = iterator_impl_t<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() -> iterator { return {m_val_data.begin(), m_jac_data.begin()}; }
  auto end() -> iterator { return {m_val_data.end(), m_jac_data.end()}; }
  auto rbegin() -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() -> reverse_iterator { return reverse_iterator{begin()}; }

  auto begin() const -> const_iterator {
    return {m_val_data.begin(), m_jac_data.begin()};
  }
  auto end() const -> const_iterator {
    return {m_val_data.end(), m_jac_data.end()};
  }
  auto rbegin() const -> const_reverse_iterator {
    return const_reverse_iterator{end()};
  }
  auto rend() const -> const_reverse_iterator {
    return const_reverse_iterator{begin()};
  }

  auto operator()(gu::isize t) const -> proxy_t<true> {
    // TODO: Conditionally based on random access iter
    auto it = begin();
    for (isize i = 0; i < t; ++i) {
      ++it;
    }
    return *it;
  }

  auto operator()(gu::isize t) -> proxy_t<false> {
    // TODO: Conditionally based on random access iter
    auto it = begin();
    for (isize i = 0; i < t; ++i) {
      ++it;
    }
    return *it;
  }

  auto clone() const -> linear_vector_fn_seq_t {
    return {m_val_data, m_jac_data};
  }
};

template <typename Scalar, typename OutIdx> struct vector_seq_t {
  using isize = gu::isize;
  using indexer_t = OutIdx;
  using mat_seq_t = detail::mat_seq_t<Scalar, indexer_t>;
  mat_seq_t m_data;

  ~vector_seq_t() = default;
  explicit vector_seq_t(OutIdx out_idx) : m_data(std::move(out_idx)) {}
  vector_seq_t(vector_seq_t const&) = delete;
  vector_seq_t(vector_seq_t&&) noexcept = default;
  auto operator=(vector_seq_t const&) -> vector_seq_t& = delete;
  auto operator=(vector_seq_t&&) noexcept -> vector_seq_t& = default;

  template <bool IsConst> struct proxy_t {
    using inner_proxy_t = typename mat_seq_t::template proxy_t<IsConst>;
    inner_proxy_t mat_seq_proxy;

    auto val() const RETURNS_AUTO(mat_seq_proxy.get())
    auto val() RETURNS_AUTO(mat_seq_proxy.get())
    auto jac() -> np::zero_t { return {}; }
    auto jac() const -> np::zero_t { return {}; }

    template <typename T> auto operator()(const T&) const RETURNS_AUTO(val())
    auto operator()() const RETURNS_AUTO(val())
    struct derivs {
      decltype(std::declval<inner_proxy_t const&>().get()) val;
      np::zero_t jac;
    };
    template <typename T> auto derivs_1(const T& input) const -> derivs {
      return {this->operator()(input), np::zero_v};
    }
  };

  template <bool IsConst> struct iterator_impl_t {
    typename mat_seq_t::template iterator_impl_t<IsConst> iter;
    using proxy_t = vector_seq_t::proxy_t<IsConst>;

    using difference_type = std::ptrdiff_t;
    using value_type = void;
    using pointer = proxy_t*;
    using reference = proxy_t;
    using iterator_category = std::bidirectional_iterator_tag;

    auto operator==(iterator_impl_t other) const -> bool {
      return iter == other.iter;
    }
    auto operator!=(iterator_impl_t other) const -> bool {
      return iter != other.iter;
    }
    auto operator++() -> iterator_impl_t& {
      ++iter;
      return *this;
    }
    auto operator--() -> iterator_impl_t& {
      --iter;
      return *this;
    }
    DDP_DEFINE_POSTFIX(iterator_impl_t);
    auto operator*() const -> proxy_t { return {*iter}; }
    auto operator*() -> proxy_t { return {*iter}; }
  };

  using iterator = iterator_impl_t<false>;
  using const_iterator = iterator_impl_t<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() -> iterator { return {m_data.begin()}; }
  auto end() -> iterator { return {m_data.end()}; }
  auto rbegin() -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() -> reverse_iterator { return reverse_iterator{begin()}; }

  auto begin() const -> const_iterator { return {m_data.begin()}; }
  auto end() const -> const_iterator { return {m_data.end()}; }
  auto rbegin() const -> const_reverse_iterator {
    return const_reverse_iterator{end()};
  }
  auto rend() const -> const_reverse_iterator {
    return const_reverse_iterator{begin()};
  }

  auto operator()(gu::isize t) const -> proxy_t<true> {
    // TODO: Conditionally based on random access iter
    auto it = begin();
    for (isize i = 0; i < t; ++i) {
      ++it;
    }
    return *it;
  }

  auto operator()(gu::isize t) -> proxy_t<false> {
    // TODO: Conditionally based on random access iter
    auto it = begin();
    for (isize i = 0; i < t; ++i) {
      ++it;
    }
    return *it;
  }

  auto clone() const -> vector_seq_t { return (m_data.idx); }
};
} // namespace ddp

#endif /* end of include guard: LINEAR_MODEL_SEQ_HPP_QUPNBESD */
