#ifndef INCLUDE_MODEL_HPP_QVJOWSOT
#define INCLUDE_MODEL_HPP_QVJOWSOT

#include "eigen/matrix.hpp"
#include "eigen/zero.hpp"
#include "general_utilities.hpp"

namespace ddp {
template <
    typename ValType,
    typename JacXType,
    typename JacUType,
    typename HessXXType,
    typename HessUXType,
    typename HessUUType>
struct quadratic_model {
  static constexpr bool is_first_order =
      std::is_same<HessXXType, np::zero_t>::value and
      std::is_same<HessUXType, np::zero_t>::value and
      std::is_same<HessUUType, np::zero_t>::value;

  static constexpr bool is_univariate =
      std::is_same<gu::remove_cvref_t<JacUType>, np::zero_t>::value and
      std::is_same<gu::remove_cvref_t<HessUXType>, np::zero_t>::value and
      std::is_same<gu::remove_cvref_t<HessUUType>, np::zero_t>::value;

public:
  ValType m_val;
  JacXType m_jx;
  JacUType m_ju;
  HessXXType m_hxx;
  HessUXType m_hux;
  HessUUType m_huu;

  template <typename InX, typename InU>
  using eval_t = np::mat<
      typename InX::scalar_t,
      gu::remove_cvref_t<JacXType>::rows_t::value,
      InX::cols_t::value>;

  auto out_dim() const noexcept ->
      typename gu::remove_cvref_t<JacXType>::rows_t {
    return m_jx.rows();
  }

  auto x_dim() const noexcept -> typename gu::remove_cvref_t<JacXType>::cols_t {
    return m_jx.cols();
  }

  template <typename JU = JacUType>
  auto u_dim() const noexcept -> typename gu::remove_cvref_t<JU>::cols_t {
    return m_ju.cols();
  }

  template <typename InputX, typename InputU>
  auto operator()(const InputX& in_x, const InputU& in_u) const RETURNS_AUTO(
      m_val + m_jx * in_x + m_ju * in_u +
      ((np::trans(in_x) * m_hxx) * in_x + (np::trans(in_u) * m_huu) * in_u) /
          2.0 +
      (np::trans(in_u) * m_hux) * in_x)

  template <typename InputX, typename InputU, REQUIRES(not is_first_order)>
  auto j_x(const InputX& in_x, const InputU& in_u) const
      RETURNS_AUTO(np::trans(in_x) * m_hxx + np::trans(in_u) * m_hux + m_jx)
  template <typename InputX, typename InputU, REQUIRES(not is_first_order)>
  auto j_u(const InputX& in_x, const InputU& in_u) const
      RETURNS_AUTO(np::trans(in_u) * m_huu + m_hux * in_x + m_ju)

  template <typename InputX, typename InputU, REQUIRES(is_first_order)>
  auto j_x(const InputX&, const InputU&) const -> gu::cref_t<JacXType> {
    return m_jx;
  }
  template <typename InputX, typename InputU, REQUIRES(is_first_order)>
  auto j_u(const InputX&, const InputU&) const -> gu::cref_t<JacUType> {
    return m_ju;
  }

  template <typename InputX, typename InputU>
  auto h_xx(const InputX&, const InputU&) const -> gu::cref_t<HessXXType> {
    return m_hxx;
  }
  template <typename InputX, typename InputU>
  auto h_ux(const InputX&, const InputU&) const -> gu::cref_t<HessUXType> {
    return m_hux;
  }
  template <typename InputX, typename InputU>
  auto h_uu(const InputX&, const InputU&) const -> gu::cref_t<HessUUType> {
    return m_huu;
  }

  template <typename InX, typename InU> struct derivs_1st_t {
    using val_t = eval_t<InX, InU>;
    using jac_x_t = gu::conditional_t<
        is_first_order,
        gu::cref_t<JacXType>,
        decltype(np::clone(m_jx))>;
    using jac_u_t = gu::conditional_t<
        is_first_order,
        gu::cref_t<JacUType>,
        decltype(np::clone(m_ju))>;

    val_t val;
    jac_x_t x;
    jac_u_t u;
  };

  template <typename InX, typename InU> struct derivs_2nd_t {
    using val_t = eval_t<InX, InU>;
    using jac_x_t = gu::conditional_t<
        is_first_order,
        gu::cref_t<JacXType>,
        decltype(np::clone(m_jx))>;
    using jac_u_t = gu::conditional_t<
        is_first_order,
        gu::cref_t<JacUType>,
        decltype(np::clone(m_ju))>;

    val_t val;
    jac_x_t x;
    jac_u_t u;
    gu::cref_t<HessXXType> xx;
    gu::cref_t<HessUXType> ux;
    gu::cref_t<HessUUType> uu;
  };

  template <typename InputX, typename InputU>
  auto derivs_1(const InputX& x, const InputU& u) const
      -> derivs_1st_t<InputX, InputU> {
    return {np::eval(this->operator()(x, u)), j_x(x, u), j_u(x, u)};
  }

  template <typename InputX, typename InputU>
  auto derivs_2(const InputX& x, const InputU& u) const
      -> derivs_2nd_t<InputX, InputU> {
    return {
        np::eval(this->operator()(x, u)),
        np::eval(j_x(x, u)),
        np::eval(j_u(x, u)),
        h_xx(x, u),
        h_ux(x, u),
        h_uu(x, u)};
  }

#define SHORTHAND_IF_UNIVARIATE(UnivarFun, Fun)                                \
  template <typename Input, REQUIRES(is_univariate)>                           \
  auto UnivarFun(const Input& x)                                               \
      const RETURNS_DECLTYPE_AUTO(Fun(x, np::zero_t{}))

  SHORTHAND_IF_UNIVARIATE(operator(), operator())
  SHORTHAND_IF_UNIVARIATE(j, j_x)
  SHORTHAND_IF_UNIVARIATE(h, h_xx)
  SHORTHAND_IF_UNIVARIATE(derivs_1, derivs_1)
  SHORTHAND_IF_UNIVARIATE(derivs_2, derivs_2)

#undef SHORTHAND_IF_UNIVARIATE
}; // namespace ddp

template <typename... Args>
auto make_quadratic_model(Args&&... args) -> quadratic_model<Args...> {
  return {FWD_PACK(args)};
}

template <typename Val, typename Jac, typename Hess>
auto make_univar_quadratic_model(Val&& val, Jac&& jac, Hess&& hess)
    -> quadratic_model<Val, Jac, np::zero_t, Hess, np::zero_t, np::zero_t> {
  using np::zero_v;
  return {FWD(val), FWD(jac), zero_v, FWD(hess), zero_v, zero_v};
}

template <typename Val, typename JacX, typename JacU>
auto make_linear_model(Val&& val, JacX&& j_x, JacU&& j_u)
    -> quadratic_model<Val, JacX, JacU, np::zero_t, np::zero_t, np::zero_t> {
  using np::zero_v;
  return {FWD(val), FWD(j_x), FWD(j_u), zero_v, zero_v, zero_v};
}

} // namespace ddp

#endif /* end of include guard: INCLUDE_MODEL_HPP_KQA91FXN */
