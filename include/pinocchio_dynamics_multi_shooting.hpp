#ifndef INCLUDE_PINOCCHIO_DYNAMICS_MULTI_SHOOTING_HPP_HWIA9AUV
#define INCLUDE_PINOCCHIO_DYNAMICS_MULTI_SHOOTING_HPP_HWIA9AUV

#include <csignal>

#include <cstdio>
#include <iostream>

#include "eigen/matrix.hpp"
#include "eigen/tensor.hpp"
#include "eigen/random.hpp"

#include "general_utilities.hpp"
#include "model.hpp"

#include "boost/optional.hpp"

#include "pinocchio/algorithm/aba-derivatives.hpp"
#include "pinocchio/algorithm/aba.hpp"

namespace ddp {
template <
    typename Model_,
    typename Data_,
    typename Nq_t = gu::dyn,
    typename Nv_t = gu::dyn>
struct pinocchio_dynamics_ms {
  using Model = Model_;
  using Data = Data_;
  using scalar_t = typename Model::scalar_t;

  Model model_;
  mutable Data data_;
  scalar_t dt;
  bool debug;

  using Config_Vector_t = typename Model::ConfigVectorType;
  using Tangent_Vector_t = typename Model::TangentVectorType;

  using isize = gu::isize;
  template <isize N>
  using Fixed_Or_Dyn = gu::conditional_t<N >= 0, gu::fix<N>, gu::dyn>;
  template <isize InDimL, isize InDimR, isize OutDim>
  using Ten = np::tensor<
      scalar_t,
      np::tensor_shape_t<
          Fixed_Or_Dyn<InDimL>,
          Fixed_Or_Dyn<InDimR>,
          Fixed_Or_Dyn<OutDim> >,
      np::tags::detail::on_heap_t>;
  template <isize rows_t, isize cols_t>
  using mat = np::mat<scalar_t, rows_t, cols_t>;
  template <isize Size> using Vec = np::mat<scalar_t, Size, 1>;

public:
  using Nx_Tot_t = gu::add_t<Nq_t, Nv_t>;
  using Nv_Tot_t = gu::add_t<Nv_t, Nv_t>;

  static constexpr auto nq_v = Nq_t::value;
  static constexpr auto nv_v = Nv_t::value;
  static constexpr auto nx_tot_v = Nx_Tot_t::value;
  static constexpr auto nv_tot_v = Nv_Tot_t::value;

  template <REQUIRES(Nq_t::is_runtime)> //
  auto nq() const -> gu::dyn {
    return {model_.nq};
  }
  template <REQUIRES(Nv_t::is_runtime)> //
  auto nv() const -> gu::dyn {
    return {model_.nv};
  }

  template <REQUIRES(not Nq_t::is_runtime)>
  auto nq() const -> gu::fix<Nq_t::value> {
    return {};
  }
  template <REQUIRES(not Nv_t::is_runtime)>
  auto nv() const -> gu::fix<Nv_t::value> {
    return {};
  }

  template <typename InX, typename InU>
  auto old_f(const InX& in_x, const InU& in_u) const -> Vec<nx_tot_v> {
    using gu::fix;
    using uninit = np::uninitialized<scalar_t>;

    const auto pos = np::top_rows(in_x, nq());
    const auto vel = np::bot_rows(in_x, nv());
    pinocchio::aba(model_, data_, pos.eigen(), vel.eigen(), in_u.eigen());

    auto res = uninit::col(nq() + nv());
    np::top_rows(res, nq()) = pos + dt * vel;
    np::bot_rows(res, nv()).eigen() = vel.eigen() + dt * data_.ddq;

    return res;
  }

  template <typename InX, typename InU>
  auto f(const InX& in_x, const InU& in_u) const -> Vec<nx_tot_v> {
    using gu::fix;
    using uninit = np::uninitialized<scalar_t>;

    auto ctrl = np::top_rows(in_u, nv());
    auto q_drift = np::mid_rows(in_u, nv(), nv());
    auto v_drift = np::bot_rows(in_u, nv());

    const auto pos = np::top_rows(in_x, nq());
    const auto vel = np::bot_rows(in_x, nv());
    pinocchio::aba(  //
        model_,      //
        data_,       //
        pos.eigen(), //
        vel.eigen(), //
        ctrl.eigen() //
    );

    auto res = uninit::col(nq() + nv());

    np::top_rows(res, nq()) = pos + dt * vel + q_drift;
    np::bot_rows(res, nv()).eigen() =
        vel.eigen() + dt * data_.ddq + v_drift.eigen();

    return res;
  }

  struct ss_derivs_2nd_t {
    Vec<nx_tot_v> val;
    mat<nv_tot_v, nv_tot_v> x;
    mat<nv_tot_v, nv_v> u;
    Ten<nv_tot_v, nv_tot_v, nv_tot_v> xx;
    Ten<nv_v, nv_tot_v, nv_tot_v> ux;
    Ten<nv_v, nv_v, nv_tot_v> uu;
  };

  struct derivs_2nd_t {
    Vec<nx_tot_v> val;
    mat<nv_tot_v, nv_tot_v> x;
    mat<nv_tot_v, 3 * nv_v> u;
    Ten<nv_tot_v, nv_tot_v, nv_tot_v> xx;
    Ten<3 * nv_v, nv_tot_v, nv_tot_v> ux;
    Ten<3 * nv_v, 3 * nv_v, nv_tot_v> uu;
  };

  template <typename InX, typename InU>
  auto old_derivs_2(InX const& in_x, InU const& in_u) const -> ss_derivs_2nd_t {
    using gu::fix;
    using uninit = np::uninitialized<scalar_t>;

    const auto pos = np::top_rows(in_x, nq());
    const auto vel = np::bot_rows(in_x, nv());

    auto nx = nv() + nv();
    auto nu = nv();

    auto val = uninit::col(nq() + nv());
    auto jx = uninit::mat(nx, nx);
    auto ju = uninit::mat(nx, nu);

    auto hxx = uninit::ten(nx, nx, nx, np::tags::on_heap);
    auto hux = uninit::ten(nu, nx, nx, np::tags::on_heap);
    auto huu = uninit::ten(nu, nu, nx, np::tags::on_heap);

    np::fill_constant(hxx.as_matrix(), 0);
    np::fill_constant(hux.as_matrix(), 0);
    np::fill_constant(huu.as_matrix(), 0);

    np::top_left_block(jx, nv(), nv()).eigen().setIdentity();

    np::top_right_block(jx, nv(), nv()).eigen().setIdentity();
    np::top_right_block(jx, nv(), nv()).eigen() *= dt;

    np::fill_constant(np::top_rows(ju, nv()), 0);

    pinocchio::computeABADerivatives(
        model_,
        data_,
        pos.eigen(),
        vel.eigen(),
        in_u.eigen(),
        np::bot_left_block(jx, nv(), nv()).eigen(),
        np::bot_right_block(jx, nv(), nv()).eigen(),
        np::bot_rows(ju, nv()).eigen());

    np::bot_rows(jx, nv()).eigen() *= dt;

    np::top_rows(val, nq()) = pos + dt * vel;
    np::bot_rows(val, nv()).eigen() = vel.eigen() + dt * data_.ddq;

    // TODO optimize
    {
      auto identity = uninit::mat(nv(), nv());
      identity.eigen().setIdentity();
      np::bot_right_block(jx, nv(), nv()).eigen() += identity.eigen();
    }

    np::bot_rows(ju, nv()).eigen() *= dt;

    using std::move;
    return {move(val), move(jx), move(ju), move(hxx), move(hux), move(huu)};
  }

  template <typename InX, typename InU>
  auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {
    using gu::fix;
    using uninit = np::uninitialized<scalar_t>;

    const auto pos = np::top_rows(in_x, nq());
    const auto vel = np::bot_rows(in_x, nv());

    auto nx = nv() + nv();
    auto nu = nv() + nv() + nv();

    auto val = uninit::col(nq() + nv());
    auto jx = uninit::mat(nx, nx);
    auto ju = uninit::mat(nx, nu);

    auto hxx = uninit::ten(nx, nx, nx, np::tags::on_heap);
    auto hux = uninit::ten(nu, nx, nx, np::tags::on_heap);
    auto huu = uninit::ten(nu, nu, nx, np::tags::on_heap);

    np::fill_constant(hxx.as_matrix(), 0);
    np::fill_constant(hux.as_matrix(), 0);
    np::fill_constant(huu.as_matrix(), 0);

    np::top_left_block(jx, nv(), nv()).eigen().setIdentity();

    np::top_right_block(jx, nv(), nv()).eigen().setIdentity();
    np::top_right_block(jx, nv(), nv()).eigen() *= dt;

    np::fill_constant(np::top_left_block(ju, nv(), nv()), 0);

    auto ctrl = np::top_rows(in_u, nv());
    auto q_drift = np::mid_rows(in_u, nv(), nv());
    auto v_drift = np::bot_rows(in_u, nv());

    pinocchio::computeABADerivatives(
        model_,
        data_,
        pos.eigen(),
        vel.eigen(),
        ctrl.eigen(),
        np::bot_left_block(jx, nv(), nv()).eigen(),
        np::bot_right_block(jx, nv(), nv()).eigen(),
        np::bot_left_block(ju, nv(), nv()).eigen());

    np::top_rows(val, nq()) = pos + dt * vel + q_drift;
    np::bot_rows(val, nv()).eigen() =
        vel.eigen() + dt * data_.ddq + v_drift.eigen();

    np::bot_rows(jx, nv()).eigen() *= dt;

    // TODO optimize
    {
      auto identity = uninit::mat(nv(), nv());
      identity.eigen().setIdentity();
      np::bot_right_block(jx, nv(), nv()).eigen() += identity.eigen();
    }

    np::bot_left_block(ju, nv(), nv()).eigen() *= dt;
    np::right_cols(ju, nx).eigen().setIdentity();

    using std::move;
    return {move(val), move(jx), move(ju), move(hxx), move(hux), move(huu)};
  }

  struct functor_t {
    gu::cref_t<pinocchio_dynamics_ms> parent;
    template <typename InX, typename InU>
    auto operator()(InX const& in_x, InU const& in_u) const -> Vec<nx_tot_v> {
      return parent.f(in_x, in_u);
    }

    template <typename InX, typename InU>
    auto derivs_2(InX const& in_x, InU const& in_u) const -> derivs_2nd_t {
      return parent.derivs_2(in_x, in_u);
    }
  };

  auto operator()(isize) const -> functor_t { return {*this}; }
};
} /* namespace ddp */
#endif /* end of include guard:                                                \
          INCLUDE_PINOCCHIO_DYNAMICS_MULTI_SHOOTING_HPP_HWIA9AUV */
