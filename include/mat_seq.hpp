#ifndef MAT_SEQ_HPP_OFMZNUJB
#define MAT_SEQ_HPP_OFMZNUJB

#include <iterator>
#include <tuple>
#include "eigen/matrix.hpp"
#include "general_utilities.hpp"
#include "indexers.hpp"

namespace ddp {
namespace detail {
template <typename Scalar, typename Indexer> struct mat_seq_t {
  struct storage_config {
    static constexpr auto storage_alloc = np::tags::on_heap;
    static constexpr auto storage_mem_align = np::tags::aligned;
  };
  using storage_t = np::detail::storage_t<Scalar, gu::dyn, storage_config>;
  using scalar_t = Scalar;
  using indexer_t = Indexer;

  using rows_t = typename indexer_t::rows_t;
  using cols_t = typename indexer_t::cols_t;

  indexer_t m_indexer;
  storage_t m_storage;

  using matrix_t =
      np::mat<Scalar, indexer_t::rows_t::value, indexer_t::cols_t::value>;
  using cview_t = np::mat_view<matrix_t const>;

  explicit mat_seq_t(indexer_t&& indexer)
      : m_indexer(std::move(indexer)),
        m_storage(gu::dyn{m_indexer.total_size()}) {}

private:
  mat_seq_t(indexer_t&& indexer, storage_t&& storage)
      : m_indexer(std::move(indexer)), m_storage(std::move(storage)) {}

public:
  auto clone() const -> mat_seq_t {
    return {indexer_t{m_indexer}, m_storage.clone()};
  }

  template <bool IsConst> struct proxy_t {
    using inner_proxy_t = indexer_proxy_t<indexer_t>;
    using data_ptr_t = gu::const_if_t<IsConst, Scalar>*;
    inner_proxy_t m_inner_proxy;
    data_ptr_t m_data;
    auto rows() const -> rows_t { return m_inner_proxy.rows(); }
    auto cols() const -> cols_t { return m_inner_proxy.cols(); }
    auto offset() const -> isize { return m_inner_proxy.m_memory_offset; }

    auto get() const -> np::mat_view<const matrix_t, np::tags::unaligned> {
      return {m_data + offset(), rows(), cols()};
    }
    auto get() //
        -> np::
            mat_view<gu::const_if_t<IsConst, matrix_t>, np::tags::unaligned> {
      return {m_data + offset(), rows(), cols()};
    }

    auto operator*() RETURNS_AUTO(get())
    auto operator*() const RETURNS_AUTO(get())

    auto time_idx() const -> isize { return m_inner_proxy.time_idx(); }
    auto as_const() const -> proxy_t<true> { return {m_inner_proxy, m_data}; }
  };

  template <bool IsConst> struct iterator_impl_t {
    using indexer_iterator_t = typename indexer_t::iterator;
    using data_ptr_t = gu::const_if_t<IsConst, Scalar>*;
    using proxy_t = mat_seq_t::proxy_t<IsConst>;

    using difference_type = std::ptrdiff_t;
    using value_type = void;
    using pointer = proxy_t*;
    using reference = proxy_t;
    using iterator_category = std::bidirectional_iterator_tag;

    indexer_iterator_t m_iter;
    data_ptr_t m_data;
    auto operator==(iterator_impl_t other) const -> bool {
      return m_iter == other.m_iter;
    }
    auto operator!=(iterator_impl_t other) const -> bool {
      return not(*this == other);
    }
    auto operator++() -> iterator_impl_t& {
      ++m_iter;
      return *this;
    }
    auto operator--() -> iterator_impl_t& {
      --m_iter;
      return *this;
    }
    DDP_DEFINE_POSTFIX(iterator_impl_t);

    auto operator*() const -> proxy_t { return {*m_iter, m_data}; }
    auto operator*() -> proxy_t { return {*m_iter, m_data}; }
  };

  using iterator = iterator_impl_t<false>;
  using const_iterator = iterator_impl_t<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  auto begin() -> iterator { return {m_indexer.begin(), m_storage.data()}; }
  auto end() -> iterator { return {m_indexer.end(), m_storage.data()}; }
  auto rbegin() -> reverse_iterator { return reverse_iterator{end()}; }
  auto rend() -> reverse_iterator { return reverse_iterator{begin()}; }

  auto begin() const -> const_iterator {
    return {m_indexer.begin(), m_storage.data()};
  }
  auto end() const -> const_iterator {
    return {m_indexer.end(), m_storage.data()};
  }
  auto rbegin() const -> const_reverse_iterator {
    return const_reverse_iterator{end()};
  }
  auto rend() const -> const_reverse_iterator {
    return const_reverse_iterator{begin()};
  }
};
} // namespace detail
template <typename Scalar, typename Indexer>
auto mat_seq_t(Indexer&& m_indexer) -> detail::mat_seq_t<Scalar, Indexer> {
  return detail::mat_seq_t<Scalar, Indexer>{FWD(m_indexer)};
}
} // namespace ddp

#endif /* end of include guard: MAT_SEQ_HPP_OFMZNUJB */
