#include "Eigen/Dense"
#include "pinocchio/algorithm/aba-derivatives.hpp"
#include "pinocchio/algorithm/aba.hpp"
#include "boost/multiprecision/cpp_bin_float.hpp"

template <int N>
using big_float = boost::multiprecision::number<
    boost::multiprecision::backends::cpp_bin_float<N> >;

#define EXTERN(Type)                                                           \
  template <typename T>                                                        \
  using vec_cref_base = Eigen::MatrixBase<                                     \
      Eigen::Ref<const Eigen::Matrix<T, Eigen::Dynamic, 1> > > const&;         \
                                                                               \
  template <typename T>                                                        \
  using mat_ref_base = Eigen::MatrixBase<                                      \
      Eigen::Ref<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> > > const&;  \
                                                                               \
  template void pinocchio::computeABADerivatives(                              \
      ModelTpl<Type> const&,                                                   \
      DataTpl<Type>&,                                                          \
      vec_cref_base<Type>,                                                     \
      vec_cref_base<Type>,                                                     \
      vec_cref_base<Type>,                                                     \
      mat_ref_base<Type>,                                                      \
      mat_ref_base<Type>,                                                      \
      mat_ref_base<Type>);                                                     \
                                                                               \
  template const typename pinocchio::DataTpl<Type>::TangentVectorType&         \
  pinocchio::aba(                                                              \
      ModelTpl<Type> const&,                                                   \
      DataTpl<Type>&,                                                          \
      vec_cref_base<Type>,                                                     \
      vec_cref_base<Type>,                                                     \
      vec_cref_base<Type>);                                                    \
                                                                               \
  template <typename T>                                                        \
  using mat_cref =                                                             \
      Eigen::Ref<const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >;     \
                                                                               \
  template const Eigen::LLT<Eigen::MatrixBase<mat_cref<Type> >::PlainObject>   \
  Eigen::MatrixBase<mat_cref<Type> >::llt() const;

// EXTERN(float)
EXTERN(double)
// EXTERN(long double)
EXTERN(big_float<200>)
// EXTERN(big_float<500>)
