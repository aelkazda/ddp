cmake_minimum_required(VERSION 3.16)
project(ddp CXX)

include(cmake/standard_project_settings.cmake)

# Link this 'library' to set the c++ standard / compile-time options requested
add_library(project_options INTERFACE)
target_compile_features(project_options INTERFACE cxx_std_11)
set(CMAKE_CXX_STANDARD 11)

# Link this 'library' to use the warnings specified in compiler_warnings.cmake
add_library(project_warnings INTERFACE)

# standard compiler warnings
include(cmake/compiler_warnings.cmake)
set_project_warnings(project_warnings)

# sanitizer options if supported by compiler
include(cmake/sanitizers.cmake)
enable_sanitizers(project_options)

# enable doxygen
include(cmake/doxygen.cmake)
enable_doxygen()

# allow for static analysis options
include(cmake/static_analyzers.cmake)

option(ENABLE_PCH "Enable Precompiled Headers" OFF)
if(ENABLE_PCH)
  target_precompile_headers(
    project_options
    INTERFACE
    <vector>
    <string>
    <map>
    <utility>
  )
endif()

# Conan dependencies
set(CONAN_OPTIONS)
set(CONAN_REQUIRES
    # Boost Software License 1.0
    boost/1.73.0
    range-v3/0.10.0
    catch2/2.12.1
    #
    # Apache License 2.0
    benchmark/1.5.0
    #
    # MIT License
    gsl-lite/0.36.0
    fmt/6.2.1
    spdlog/1.6.0
    backward-cpp/1.5
    #
    # Mozilla Public License 2.0
    eigen/3.3.7
    #
    # New (Revised) BSD License
    blaze/3.7
    # LGPL > 3
    mpfr/4.0.2
)
find_package(PkgConfig REQUIRED)
pkg_check_modules(
  URDFDOM
  REQUIRED
  IMPORTED_TARGET
  urdfdom
)
pkg_check_modules(
  PINOCCHIO
  REQUIRED
  IMPORTED_TARGET
  pinocchio
)
target_compile_definitions(
  project_options INTERFACE SPDLOG_FMT_EXTERNAL gsl_CONFIG_DEFAULTS_VERSION=1
                            EIGEN_MAX_STATIC_ALIGN_BYTES=0
)

include(cmake/conan.cmake)
run_conan()

target_link_libraries(
  project_options
  INTERFACE project_warnings
            PkgConfig::PINOCCHIO
            PkgConfig::URDFDOM
            CONAN_PKG::boost
            CONAN_PKG::range-v3
            CONAN_PKG::catch2
            CONAN_PKG::benchmark
            CONAN_PKG::gsl-lite
            CONAN_PKG::fmt
            CONAN_PKG::spdlog
            CONAN_PKG::backward-cpp
            CONAN_PKG::eigen
            CONAN_PKG::blaze
            CONAN_PKG::mpfr
)

add_library(cddp src/extern.cpp)
target_include_directories(cddp PUBLIC include)
target_link_libraries(cddp project_options)

add_executable(pinocchio_single test/pinocchio_single.cpp)
target_link_libraries(pinocchio_single cddp)

add_executable(pinocchio_terminal_single test/pinocchio_terminal_single.cpp)
target_link_libraries(pinocchio_terminal_single cddp)

add_executable(pinocchio_multi test/pinocchio_multi.cpp)
target_link_libraries(pinocchio_multi cddp)
